WITH W_DOC AS (
    SELECT *
    FROM (SELECT DOCUMENT_NO
        ,TEMPLATE_ID ||'\' || EXPORT_FILE_PATH AS PATH
     FROM DH_T_POLICY_EDOC_DET ORDER BY INSERT_DATE DESC)
    WHERE 1=1
        AND DOCUMENT_NO=:doc_no
        AND ROWNUM=1
)

SELECT *
FROM W_DOC
WHERE 1=1