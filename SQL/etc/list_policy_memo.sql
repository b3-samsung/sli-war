with DH_V_UW_PENDING_MEMO as
(
SELECT AA."POLICY_CODE",AA."POLICY_ID",
            AA."SUB_DATE",
            AA."COMMENCEMENT_DATE",
            AA."ISSUE_DATE",
            AA."DOCUMENT_NO",
            AA."DOCU_STATUS",
            AA."TEMPLATE_NAME",
            AA."DOCU_UPDATE_TIME",
            AA."PHYSICAL_ITEM"
       FROM (SELECT A.POLICY_CODE, A.POLICY_ID,
                    TO_CHAR (A.SUBMISSION_DATE, 'YYYY/MM/DD') "SUB_DATE",
                    TO_CHAR (A.VALIDATE_DATE, 'YYYY/MM/DD') "COMMENCEMENT_DATE",
                    TO_CHAR (A.ISSUE_DATE, 'YYYY/MM/DD') "ISSUE_DATE",
                    B.DOCUMENT_NO,
                    (SELECT STATUS_NAME
                       FROM T_DOCUMENT_STATUS
                      WHERE STATUS_ID = C.STATUS)
                       "DOCU_STATUS",
                    D.TEMPLATE_NAME,
                    TO_CHAR (C.UPDATE_TIME, 'YYYY/MM/DD') "DOCU_UPDATE_TIME",
                    B.PHYSICAL_ITEM,
                    ROW_NUMBER ()
                    OVER (
                       PARTITION BY A.POLICY_CODE,
                                    TO_CHAR (C.UPDATE_TIME, 'YYYYMMDD'),
                                    B.PHYSICAL_ITEM
                       ORDER BY C.DOCUMENT_NO DESC)
                       "RN"
               FROM mv_CONTRACT_MASTER A,
                    T_PHYSICAL_EXAM_ITEM B,
                    T_DOCUMENT C,
                    T_TEMPLATE D
              WHERE     A.POLICY_ID = B.POLICY_ID
                    AND B.DOCUMENT_NO = C.DOCUMENT_NO
                    AND C.TEMPLATE_ID = D.TEMPLATE_ID
                    AND A.POLICY_CODE =:policy_code
						  -- AND A.POLICY_ID = 193142 -- :policy_id
                    AND TO_CHAR (C.UPDATE_TIME, 'YYYY/MM/DD') >=
                           TO_CHAR (SYSDATE - 90, 'YYYY/MM/DD')) AA
      WHERE AA.RN = 1
) 

select distinct policy_code
, policy_id
, document_no 
, template_name
, PHYSICAL_ITEM as memo
, docu_status as memo_status
from DH_V_UW_PENDING_MEMO 
where document_no in (select max(document_no) 
	from DH_V_UW_PENDING_MEMO group by policy_code)
-- agent_id=:agent_id