select distinct memo.policy_code
-- , policy_id
	, memo.document_no 
	, template_name
	, PHYSICAL_ITEM as memo
	, docu_status as memo_status
	, DOCU_UPDATE_TIME as memo_status_date
	, tcm.service_agent
	, agent.agent_code
, det.TEMPLATE_ID || '/' || det.EXPORT_FILE_PATH as document_path
from QV_UW_PENDING_MEMO memo
	inner join mv_contract_master tcm on tcm.policy_code=memo.policy_code
	inner join mv_agent agent on tcm.service_agent=agent.agent_id
inner join dh_t_policy_edoc_det det ON det.document_no=memo.document_no
where memo.document_no in (select max(document_no) from QV_UW_PENDING_MEMO group by policy_code)
	and agent.agent_code=:agent_code
order by
	memo_status_date desc