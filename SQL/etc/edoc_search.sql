SELECT 
	EDOC.DOCUMENT_NO as KEY
	, EDOC.DOCUMENT_DATE as DOC_DATE
	, EDOC.DOCUMENT_NO as DOC_NO
	, TPL.TEMPLATE_NAME_TH as DOC_TYPE
	, TPL.CATEGORY_NAME_TH as CAT_NAME
	, EDOC.POLICY_CODE as POLICY_CODE
	, ins.first_name as PH_FNAME
	, ins.last_name as PH_LNAME
	, ag.first_name ||' '|| ag.last_name as AGENT_NAME
	, mv_agent.agent_code
FROM 
	DH_T_POLICY_EDOC EDOC
	INNER JOIN DH_T_TEMPLATE TPL 
		ON TPL.TEMPLATE_ID=EDOC.TEMPLATE_ID AND TPL.edoc_for_agt='Y'
	INNER JOIN MV_CONTRACT_MASTER TCM ON TCM.policy_id=EDOC.policy_id
	INNER JOIN mv_insured_list til ON tcm.policy_id = til.policy_id
	-- INNER JOIN mv_benefit_insured tbi ON tcm.policy_id = tbi.policy_id AND tbi.insured_id = til.list_id AND tbi.item_id = tcp.item_id
	INNER JOIN mv_agent ON mv_agent.agent_id=tcm.service_agent
	INNER JOIN mv_customer ins ON til.party_id = ins.customer_id
	LEFT JOIN (SELECT customer_id, first_name, last_name FROM MV_CUSTOMER) AG ON TCM.service_agent=AG.customer_id
	-- LEFT JOIN (SELECT customer_id, first_name, last_name FROM MV_CUSTOMER) PH ON TCM.policy_id=PH.customer_id
WHERE 1=1
	AND EDOC.DEL_INDI IS NULL
	AND EDOC.ARCHIVE_INDI IS NULL