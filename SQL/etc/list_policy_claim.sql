WITH w_case_status as (
SELECT status_id, 
    (CASE 
        WHEN status_id in (1,2,3,4) THEN 'บันทึกการเรียกร้องสินไหมแล้ว'
        WHEN status_id in (5,6,14,15,16,18) THEN 'อยู่ระหว่างการพิจารณา'
        WHEN status_id in (7,8,13) THEN 'อยู่ระหว่างการพิจารณา'
        WHEN status_id in (10) THEN 'การพิจาณาสินไหมสิ้นสุดแล้ว'
        WHEN status_id in (11,12,17) THEN 'ยกเลิกการบันทึกสินไหม'
        ELSE status_name || '[' || status_id || ']'
     END) as status_name
FROM t_case_status
)

select
    -- policy_id
    -- , ccase.case_id 
    case_no
    , w_case_status.status_name as status
    , notification_date
    , trunc(approve_time) as approve_date
    -- , ccase.agent_id
    , agent.agent_code
    , cust.first_name || ' ' || cust.last_name as agent_name
from 
    t_claim_case ccase
    inner join mv_agent agent on ccase.agent_id = agent.agent_id
    inner join mv_customer cust on cust.customer_id = agent.agent_id
    inner join t_claim_product cprod on cprod.case_id = ccase.case_id and is_accepted = 'Y'
    inner join (select policy_code,policy_id as tcm_policy_id from mv_contract_master) tcm on tcm.tcm_policy_id = policy_id
	 left join w_case_status on ccase.case_status = w_case_status.status_id
where 
    tcm.policy_code = :policy_code
order by 
    policy_id, case_no desc