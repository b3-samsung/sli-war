with w_product as (
SELECT
    tcm.policy_id
     ,tcm.service_agent
    ,tcm.policy_code
    ,coalesce(org_life.internal_id, tpl.internal_id) as product_code
    ,coalesce(org_life.product_name, tpl.product_name) as product_name
	 ,(CASE 
            WHEN tcp.liability_state = 1 THEN
                CASE
                    WHEN tce.prem_status is null THEN
                        'มีผลบังคับ'
                    WHEN tce.prem_status = 11 THEN
                        'มีผลบังคับ/ขยายระยะเวลาอัตโนมัติ'
                    WHEN tce.prem_status = 8 THEN
                        'มีผลบังคับ/ขยายระยะเวลา'
                    WHEN tce.prem_status = 6 THEN
                        'มีผลบังคับ/มูลค่าใช้เงินสำเร็จอัตโนมัติ'
                    WHEN tce.prem_status = 3 THEN
                        'มีผลบังคับ/มูลค่าใช้เงินสำเร็จ'
                    WHEN apl.sum_apl > 0 THEN
                        'มีผลบังคับ/กู้อัตโนมัติชำระเบี้ย'
                    WHEN tce.prem_status = 1 OR tce.prem_status = 2 or tce.prem_status = 4 THEN
                        'มีผลบังคับ'
                    ELSE
                        'มีผลบังคับ (' || t_prem_status.status_name || ')'
                END
            WHEN tcp.liability_state = 2 THEN
                'ขาดอายุ'
            WHEN tcp.liability_state = 3 THEN
                CASE
                    WHEN tcp.end_cause = 51 THEN
                        'สิ้นผลบังคับ/ขอยกเลิกกรมธรรม์ (Freelook)'
                    WHEN tcp.end_cause = 31 THEN
                        'สิ้นผลบังคับ/เวนคืนกรมธรรม์'
                    WHEN tcp.end_cause = 12 THEN
                        'สิ้นสุดความคุ้มครอง'
                    WHEN tcp.end_cause = 34 THEN
                        'ยกเลิกกรมธรรม์'
                    WHEN tcp.end_cause = 21 THEN
                        'สิ้นผลบังคับ'
                    WHEN tcp.end_cause = 35 THEN
                        'ยกเลิกกรมธรรม์'
                    WHEN tcp.end_cause = 22 OR tcp.end_cause = 23 THEN
                        'สิ้นผลบังคับ'
                    ELSE
                        'สิ้นผลบังคับ (' || t_end_cause.cause_name || ')'
                END
            END) as prem_status_name
    ,t_liability_status.status_name || ' / ' || coalesce(t_prem_status.status_name,t_end_cause.cause_name) as policy_status
    ,tcp.master_id
    ,tcp.amount
    ,tcp.total_prem_af
	 ,apl.sum_apl
    , tcp.validate_date as effective_date
    , tcp.expiry_date as coverage_end_date
    , tcp.paidup_date
	,tce.due_date as next_due_date
FROM
	MV_CONTRACT_MASTER tcm
	INNER JOIN MV_CONTRACT_PRODUCT tcp on tcm.policy_id=tcp.policy_id
	INNER JOIN t_product_life tpl on tcp.product_id = tpl.product_id
	INNER JOIN MV_CONTRACT_EXTEND tce on tcm.policy_id=tce.policy_id AND tce.item_id=tcp.item_id
	LEFT JOIN t_product_life org_life on tcp.origin_product_id is not null and tcp.origin_product_id=org_life.product_id
	LEFT JOIN t_prem_status on tce.prem_status = t_prem_status.status_id
	LEFT JOIN t_liability_status on tcp.liability_state = t_liability_status.status_id
	LEFT JOIN t_end_cause on tcp.end_cause = t_end_cause.cause_id
	LEFT JOIN (SELECT ACC.POLICY_ID,MAS.POLICY_CODE, SUM (CASE WHEN ACC.ACCOUNT_TYPE IN (53) THEN ACC.CAPITAL_BALANCE + ACC.INTEREST_BALANCE ELSE 0 END) AS SUM_APL
		FROM MV_POLICY_ACCOUNT ACC, MV_CONTRACT_MASTER MAS
		WHERE ACC.POLICY_ID = MAS.POLICY_ID AND MAS.SUSPEND <> 'Y'
		GROUP BY ACC.POLICY_ID, MAS.POLICY_CODE
		having SUM (CASE WHEN ACC.ACCOUNT_TYPE IN (53) THEN ACC.CAPITAL_BALANCE + ACC.INTEREST_BALANCE ELSE 0 END) > 0) apl on tcm.POLICY_ID=apl.POLICY_ID
ORDER BY
    tcm.policy_id
)

select *
    -- policy_id
    -- , product_code
    -- , product_name
    -- , count(product_code)
from w_product
where 
    -- policy_id=222 -- PB
    service_agent='26223536'
    AND master_id is null
-- group by
    -- policy_id,product_code,product_name
-- having count(product_code) > 1
