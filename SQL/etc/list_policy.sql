with w_agent_policy as (
	select
		-- tcm.service_agent as agent_id
		-- tcm.item_id
		tcm.policy_id as policy_id
		, tcm.policy_code as policy_code
		, agent.agent_id
		, agent.agent_code
		, cus.customer_id as customer_id
	from
		mv_contract_master tcm
		left join mv_agent agent on tcm.service_agent = agent.agent_id
		-- inner join mv_contract_product tcp on tcm.policy_id = tcp.policy_id
		inner join mv_insured_list til on tcm.policy_id = til.policy_id
		inner join mv_customer cus on cus.customer_id = til.party_id
	order by
		agent_id, policy_id
)
, w_policy as (
	select
		tcm.policy_id
		, tcm.policy_code
		, tcm.suspend as policy_frozen
		-- , tcm.liability_state
		, case
			when tcm.liability_state=0 then
				(select status_desc from t_proposal_status psts where psts.proposal_status = pros.proposal_status)
			else 
				(select status_name from t_liability_status where status_id = tcm.liability_state)				
			end policy_status
		, (select cause_name from t_end_cause where cause_id = tcm.end_cause) as end_cause
		, case
			when (select sum(prem.total_prem_af) from mv_contract_product prem, mv_contract_extend prem_sts where prem.policy_id = tcm.policy_id and prem.liability_state = 1 and prem.item_id = prem_sts.item_id and prem.policy_id = prem_sts.policy_id and prem_sts.prem_status = 1) is null then (select sum(prem.total_prem_af) from mv_contract_product prem where prem.policy_id = tcm.policy_id )
			else (select sum(prem.total_prem_af) from mv_contract_product prem, mv_contract_extend prem_sts where prem.policy_id = tcm.policy_id and prem.liability_state = 1 and prem.item_id = prem_sts.item_id and prem.policy_id = prem_sts.policy_id and prem_sts.prem_status = 1)
			end policy_premium
		, life.product_name
		-- , service_agent_code
		-- , service_agent_name
		-- , service_agent_last_name
		-- , service_agent_home_tel
		-- , service_agent_office_tel
		-- , service_agent_mobile
	from mv_contract_master tcm
		inner join mv_contract_product tcp on tcm.policy_id=tcp.policy_id and tcp.master_id is null
		inner join t_product_life life on tcp.product_id = life.product_id and life.ins_type = 1
		inner join mv_contract_proposal pros on tcm.policy_id = pros.policy_id
)

select
	wap.*
	-- ,wp.*
from
	w_agent_policy wap
	-- left join w_policy wp on wap.policy_id=wp.policy_id
where
	wap.agent_id=:agent_id
	-- and rownum<10