SELECT DISTINCT tcm.policy_code,chq.cheque_no,agent.agent_code,chq.fee_amount--,chq.*
FROM (mv_contract_master tcm
    LEFT JOIN mv_cash cash ON tcm.policy_id=cash.policy_id)
    LEFT JOIN mv_cheque chq ON cash.cheque_id=chq.cheque_id
    LEFT JOIN mv_agent agent ON tcm.service_agent=agent.agent_id
WHERE
    tcm.policy_code=:policy_code AND LENGTH(chq.cheque_no)=11
--012160006854 082160000777