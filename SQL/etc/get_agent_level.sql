select 
	-- a.agent_code,
	-- a.agent_grade_order, 
	(case when a.agent_cate in ('D', 'E')
		then
			decode(a.agent_grade_order, 1, 'AG', 2, 'AL', 3, 'AVP', 4, 'GM')
		when a.agent_cate = 'L'
		then
			decode(a.agent_grade_order, 1, 'FC', 2, 'FM', 3, 'CM', 4, 'BM')
		end) agent_level
from 
	mv_agent a
where
	agent_code=:agent_code