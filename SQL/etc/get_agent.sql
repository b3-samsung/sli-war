with channel_h as
( 
select c_level.agent_id
	, (select agent_code from mv_agent where agent_id = c_level.al) as al_agent_code
	, (select first_name ||' '|| last_name from mv_customer where customer_id = c_level.al) as al_name
	, (select agent_code from mv_agent where agent_id = c_level.avp) as avp_agent_code
	, (select first_name ||' '|| last_name from mv_customer where customer_id = c_level.avp) as avp_name
	, (select agent_code from mv_agent where agent_id = c_level.gm) as gm_agent_code 
	, (select first_name ||' '|| last_name from mv_customer where customer_id = c_level.gm) as gm_name
	, (SELECT CHANNEL_CODE ||' '|| CHANNEL_NAME FROM mv_CHANNEL_ORG WHERE CHANNEL_ID = GET_CHANNEL_HIERARCHY(c_level.CHANNEL_ORG_ID, 3)) Team
	, (SELECT CHANNEL_CODE ||' '|| CHANNEL_NAME FROM mv_CHANNEL_ORG WHERE CHANNEL_ID = GET_CHANNEL_HIERARCHY(c_level.CHANNEL_ORG_ID, 2)) Sale_office
	, (SELECT CHANNEL_CODE ||' '|| CHANNEL_NAME FROM mv_CHANNEL_ORG WHERE CHANNEL_ID = GET_CHANNEL_HIERARCHY(c_level.CHANNEL_ORG_ID, 1)) Branch
from (select agent_id
	, channel_org_id 
	, GET_CHANNEL_AGENT_HIERARCHY (agent.agent_id, 3) as AL
	, GET_CHANNEL_AGENT_HIERARCHY (agent.agent_id, 2) as AVP
	, GET_CHANNEL_AGENT_HIERARCHY (agent.agent_id, 1) as GM
	from mv_agent agent) c_level 
),
agent_h as
(
select agent_id
	, (select agent_code from mv_agent where agent_id = p_level.level_1) as level1_agent_code
	, (select first_name ||' '|| last_name from mv_customer where customer_id = p_level.level_1 )as level1_agent_name 
	, (select agent_code from t_agent where agent_id = p_level.level_2) as level2_agent_code
	, (select first_name ||' '|| last_name from mv_customer where customer_id = p_level.level_2 )as level2_agent_name 
	, (select agent_code from t_agent where agent_id = p_level.level_3) as level3_agent_code
	, (select first_name ||' '|| last_name from mv_customer where customer_id = p_level.level_3 )as level3_agent_name 
	, (select agent_code from t_agent where agent_id = p_level.level_4) as level4_agent_code
	, (select first_name ||' '|| last_name from mv_customer where customer_id = p_level.level_4 )as level4_agent_name 
	, (select agent_code from t_agent where agent_id = p_level.level_5) as level5_agent_code
	, (select first_name ||' '|| last_name from mv_customer where customer_id = p_level.level_5 )as level5_agent_name  
from 
	(select agent_id
		, GET_AGENT_HIERARCHY (AGENT_ID, 1) as LEVEL_1
		, GET_AGENT_HIERARCHY (AGENT_ID, 2) as LEVEL_2
		, GET_AGENT_HIERARCHY (AGENT_ID, 3) as LEVEL_3
		, GET_AGENT_HIERARCHY (AGENT_ID, 4) as LEVEL_4
		, GET_AGENT_HIERARCHY (AGENT_ID, 5) as LEVEL_5
	FROM MV_AGENT
		WHERE AGENT_CATE IN ('L','F','D','E')) p_level
)

select agent.agent_id
	, agent.agent_code
	, title.title_desc as title
	, cust.first_name ||' '|| cust.last_name as agent_name
	, (select agent_status_name from t_agent_status where agent_status = agent.agent_status) as agent_status
	, (select status_desc from t_agent_sub_status where status_id = agent.agent_substatus) as agent_sub_status
	, agent.status_date
	, (select cate_code from t_agent_cate where agent_cate = agent.agent_cate) as agent_category_code
	, (select cate_name from t_agent_cate where agent_cate = agent.agent_cate) as agent_category_name
	, (select grade_name from t_agent_grade where agent_grade = agent.agent_grade) as agent_grade
	, (select job_nature from t_job_nature where job_nature_id = agent.job_nature_id) as job_nature
	, (select position_desc from t_agent_position where position_id = agent.position) as position_type
	, agent.appoinment_date
	, agent.enter_company_date
	, case when to_char(agent.leave_company_date, 'YYYYMMDD') <> '19991231' then agent.leave_company_date end leave_company_date
	, agent.position_date
	, agent.effective_date
	, case when guar.agent_id is not null then guar.agent_code end guarantor_code
	, case when g_cust.customer_id is not null then g_cust.first_name ||' '|| g_cust.last_name end guarantor_name
	, lics.license_number
	, lics.expire_date as license_expire_date
	, cust.email
	, cust.mobile
	, cust.office_tel
	, cust.home_tel
	, (select min(bank_account) from t_bank_account a where a.party_id = agent.agent_id and account_status = 1 and approval_status = 1 and bank_code = '065'
	and account_type = 2) as bank_account 
	, loc.abbr_name as location_code
	, loc.company_name as location_name
	, ch_h.al_agent_code
	, ch_h.al_name
	, ch_h.avp_agent_code
	, ch_h.avp_name
	, ch_h.gm_agent_code
	, ch_h.gm_name
	, ch_h.team
	, ch_h.sale_office
	, ch_h.branch
	, ag_h.level1_agent_code
	, ag_h.level1_agent_name
	, ag_h.level2_agent_code
	, ag_h.level2_agent_name
	, ag_h.level3_agent_code
	, ag_h.level3_agent_name
	, ag_h.level4_agent_code
	, ag_h.level4_agent_name
	, ag_h.level5_agent_code
	, ag_h.level5_agent_name

from mv_agent agent
	inner join mv_channel_org chan on chan.channel_id = agent.channel_org_id
	inner join mv_customer cust on agent.agent_id = cust.customer_id 
	left join t_title title on title.title_code = cust.honor_title
	left join mv_agent guar on guar.agent_id = agent.guarantor_id
	left join mv_customer g_cust on g_cust.customer_id = guar.agent_id
	left join mv_test_info lics on lics.agent_id = agent.agent_id
	left join mv_company_organ loc on loc.organ_id = chan.org_id
	left join channel_h ch_h on ch_h.agent_id = agent.agent_id
	left join agent_h ag_h on ag_h.agent_id = agent.agent_id
where agent.agent_code=:agent_code -- 9001