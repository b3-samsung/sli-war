WITH Billing AS
  (SELECT policy_id,
             due_time,
             SUM (fee_amount) AS BILLING_PREMIUM
    FROM
      (SELECT a.POLICY_ID,
                 a.fee_amount,
                 a.due_time,
                 a.change_id,
                 a.fee_status,
                 a.offset_status,
                 a.fee_type
        FROM mv_prem_arap a,
                                mv_arap_offset b,
          (SELECT aa.policy_id,
                     bb.renewal_type,
                     CASE WHEN aa.initial_vali_date IS NULL THEN aa.validate_date WHEN aa.initial_vali_date IS NOT NULL
            AND TO_CHAR (aa.initial_vali_date, 'DD') = TO_CHAR (aa.validate_date, 'DD') THEN aa.initial_vali_date ELSE TO_DATE ( TO_CHAR (aa.initial_vali_date, 'YYYY') || '-' || TO_CHAR (aa.validate_date, 'MM-DD'), 'YYYY-MM-DD') END effective_date
            FROM mv_contract_master aa,
                                            mv_contract_product bb
            WHERE aa.policy_id = bb.policy_id
              AND bb.master_id IS NULL) c
        WHERE a.list_id = b.offset_list_id(+)
          AND a.policy_id = c.policy_id(+) ) aa
    WHERE aa.fee_type = '41'
      AND aa.offset_status = '0'
      AND aa.fee_status = 0 /*
                            AND aa.fee_status NOT IN (-1,
                                                              -2,
                                                              3,
                                                              8,
                                                              11,
                                                              20) */
      AND aa.change_id IN
         (SELECT MAX (sub.change_id)
          FROM
             (SELECT a.POLICY_ID,
                        a.CHANGE_ID,
                        a.due_time,
                        a.fee_status,
                        a.offset_status,
                        a.fee_type
              FROM mv_prem_arap a,
                                      mv_arap_offset b,
                 (SELECT aa.policy_id,
                            bb.renewal_type,
                            CASE WHEN aa.initial_vali_date IS NULL THEN aa.validate_date WHEN aa.initial_vali_date IS NOT NULL
                  AND TO_CHAR ( aa.initial_vali_date, 'DD') = TO_CHAR ( aa.validate_date, 'DD') THEN aa.initial_vali_date ELSE TO_DATE ( TO_CHAR ( aa.initial_vali_date, 'YYYY') || '-' || TO_CHAR ( aa.validate_date, 'MM-DD'), 'YYYY-MM-DD') END effective_date
                  FROM mv_contract_master aa,
                                                  mv_contract_product bb
                  WHERE aa.policy_id = bb.policy_id
                     AND bb.master_id IS NULL) c
              WHERE a.list_id = b.offset_list_id(+)
                 AND a.policy_id = c.policy_id(+) ) sub
          WHERE sub.policy_id = aa.policy_id
             AND sub.due_time = aa.due_time
             AND sub.fee_type = '41'
             AND sub.offset_status = '0'
             AND sub.fee_status = 0 /*
                                                                    AND sub.fee_status NOT IN (-1,
                                                                                                        -2,
                                                                                                        3,
                                                                                                        8,
                                                                                                        11,
                                                                                                        20) */
          GROUP BY sub.policy_id,
                      sub.due_time)
    GROUP BY aa.policy_id,
                aa.due_time)
SELECT fee_id,
         policy_id,
         policy_year,
         policy_period,
         due_time,
         collection_date,
         apply_amount,
         status,
         collection_method,
         CASE
              WHEN cheque_no IS NULL THEN
                        (SELECT replace(acct.bank_account,substr(acct.bank_account,length(acct.bank_account)-3,4),'XXXX') AS cheque_no
                         FROM t_bank_account acct,
                                                    mv_cash c
                         WHERE acct.account_id = c.account_id
                            AND c.fee_id = all_tran.fee_id)
              ELSE cheque_no
         END cheque_no,
         CASE
              WHEN bank_code IS NULL THEN
                        (SELECT substr(acct.bank_code,1,3)
                         FROM t_bank_account acct,
                                                    mv_cash ch
                         WHERE acct.account_id = ch.account_id
                            AND ch.fee_id = all_tran.fee_id)
              ELSE bank_code
         END bank_code,
         CASE
              WHEN bank_name IS NULL THEN
                        (SELECT bo.bank_name
                         FROM t_bank_account acct,
                                                    mv_cash ch,
                                                              t_bank_org bo
                         WHERE acct.account_id = ch.account_id
                            AND ch.fee_id = all_tran.fee_id
                            AND substr(acct.bank_code,1,3)= bo.bank_code)
              ELSE bank_name
         END bank_name,
         nvl(refund_amount,0) AS refund_amount
FROM
  (SELECT DISTINCT aa.fee_id ,
                         aa.policy_id ,
                         PKG_LS_PUB_UTILS.F_POLICY_DURATION_YEAR (aa.DUE_TIME,EFFECTIVE_DATE)+ 1 AS POLICY_YEAR ,
                         (CASE WHEN PKG_LS_PUB_UTILS.F_POLICY_DURATION_MONTH ( aa.DUE_TIME, EFFECTIVE_DATE) > 0
                          AND (TO_CHAR (EFFECTIVE_DATE, 'DD') - TO_CHAR (aa.DUE_TIME, 'DD')= ABS(1)) THEN PKG_RPT_TSLI_COMMONS.GET_INSTALMENT_NO ( RENEWAL_TYPE, PKG_LS_PUB_UTILS.F_POLICY_DURATION_MONTH ( last_day(aa.DUE_TIME), last_day(EFFECTIVE_DATE))) WHEN PKG_LS_PUB_UTILS.F_POLICY_DURATION_MONTH ( aa.DUE_TIME, EFFECTIVE_DATE) > 0 THEN PKG_RPT_TSLI_COMMONS.GET_INSTALMENT_NO ( RENEWAL_TYPE, PKG_LS_PUB_UTILS.F_POLICY_DURATION_MONTH (aa.DUE_TIME, EFFECTIVE_DATE)) WHEN PKG_LS_PUB_UTILS.F_POLICY_DURATION_MONTH ( aa.DUE_TIME, EFFECTIVE_DATE) = 0
                          AND aa.DUE_TIME < EFFECTIVE_DATE THEN 1 ELSE 0 END) AS POLICY_PERIOD ,
                         aa.due_time --    , aa.policy_chg_id

                                      ,
                                      CASE
                                            WHEN ca.fee_id IS NOT NULL THEN aa.collection_date
                                            ELSE aa.collection_date_min
                                      END collection_date ,
                                      aa.paid_amount AS apply_amount ,
                                      aa.status,
                                      CASE
                                            WHEN ca.fee_id IS NOT NULL THEN
                                                     (SELECT pm.mode_name
                                                      FROM t_pay_mode pm
                                                      WHERE pm.mode_id=ca.pay_mode)
                                            ELSE
                                                     (SELECT pm.mode_name
                                                      FROM t_pay_mode pm
                                                      WHERE pm.mode_id=ca_min.pay_mode)
                                      END collection_method ,
                                      CASE
                                            WHEN ca.pay_mode IN (30,
                                                                        3)
                                                  AND ca.cheque_id IS NOT NULL
                                                  AND ca.account_id = ca.internal_account THEN
                                                     (SELECT replace(ch.cheque_no,substr(ch.cheque_no,length(ch.cheque_no)-3,4),'XXXX')
                                                      FROM t_cheque ch
                                                      WHERE ch.cheque_id = ca.cheque_id)
                                            WHEN ca.cheque_id IS NULL
                                                  AND ca.account_id IS NOT NULL
                                                  AND ca.account_id <> ca.internal_account THEN replace(acct.bank_account,substr(acct.bank_account,length(acct.bank_account)-3,4),'XXXX')
                                            WHEN ca.fee_id IS NOT NULL THEN
                                                     (SELECT ch.cheque_no
                                                      FROM t_cheque ch
                                                      WHERE ch.cheque_id = ca.cheque_id)
                                            WHEN ca_min.pay_mode IN (30,
                                                                             3)
                                                  AND ca_min.cheque_id IS NOT NULL
                                                  AND ca_min.account_id = ca_min.internal_account THEN
                                                     (SELECT replace(ch.cheque_no,substr(ch.cheque_no,length(ch.cheque_no)-3,4),'XXXX')
                                                      FROM t_cheque ch
                                                      WHERE ch.cheque_id = ca_min.cheque_id)
                                            WHEN ca_min.cheque_id IS NULL
                                                  AND ca_min.account_id IS NOT NULL
                                                  AND ca_min.account_id <> ca.internal_account THEN replace(acct.bank_account,substr(acct_min.bank_account,length(acct_min.bank_account)-3,4),'XXXX')
                                            WHEN ca_min.fee_id IS NOT NULL THEN
                                                     (SELECT ch.cheque_no
                                                      FROM t_cheque ch
                                                      WHERE ch.cheque_id = ca_min.cheque_id)
                                      END cheque_no ,
                                      CASE
                                            WHEN ca.bank_code IS NULL
                                                  AND ca.account_id IS NOT NULL
                                                  AND ca.account_id <> ca.internal_account THEN substr(acct.bank_code,1,3)
                                            WHEN ca.fee_id IS NOT NULL THEN ca.bank_code
                                            WHEN ca_min.bank_code IS NULL
                                                  AND ca_min.account_id IS NOT NULL
                                                  AND ca_min.account_id <> ca_min.internal_account THEN substr(acct_min.bank_code,1,3)
                                            WHEN ca_min.fee_id IS NOT NULL THEN ca_min.bank_code
                                      END bank_code ,
                                      CASE
                                            WHEN ca.bank_code IS NULL
                                                  AND ca.account_id IS NOT NULL
                                                  AND ca.account_id <> ca.internal_account THEN
                                                     (SELECT bo.bank_name
                                                      FROM t_bank_org bo
                                                      WHERE bo.bank_code=substr(acct.bank_code,1,3))
                                            WHEN ca.fee_id IS NOT NULL THEN
                                                     (SELECT bo.bank_name
                                                      FROM t_bank_org bo
                                                      WHERE bo.bank_code=ca.bank_code)
                                            WHEN ca_min.bank_code IS NULL
                                                  AND ca_min.account_id IS NOT NULL
                                                  AND ca_min.account_id <> ca_min.internal_account THEN
                                                     (SELECT bo.bank_name
                                                      FROM t_bank_org bo
                                                      WHERE bo.bank_code=substr(acct.bank_code,1,3))
                                            WHEN ca_min.fee_id IS NOT NULL THEN
                                                     (SELECT bo.bank_name
                                                      FROM t_bank_org bo
                                                      WHERE bo.bank_code=ca_min.bank_code)
                                      END bank_name ,
                                      refund_amount
    FROM
      (SELECT policy_id,
                 due_time,
                 status,
                 collection_date,
                 fee_id,
                 collection_date_min,
                 fee_id_min,
                 sum(fee_amount) AS paid_amount
        FROM
          (SELECT DISTINCT c.policy_id,
                                 pa.due_time,
                                 --pa.policy_chg_id,


              (SELECT status_name
                FROM t_fee_status
                WHERE status_id = pa.fee_status) AS status,
                                 pa.fee_amount,
                                 max(trunc(c.check_enter_time)) AS collection_date,
                                 max(c.fee_id) AS fee_id,
                                 min(trunc(c.check_enter_time)) AS collection_date_min,
                                 min(c.fee_id) AS fee_id_min
            FROM mv_cash c,
                  mv_prem_arap pa,
                  mv_arap_offset ao_a,
                  mv_arap_offset ao_c
            WHERE c.fee_id=ao_c.fee_id
              AND ao_c.arap_table=1
              AND ao_c.offset_id=ao_a.offset_id
              AND ao_a.fee_id=pa.list_id
              AND ao_a.arap_table=4
              AND c.fee_status = 1
              AND pa.offset_status = 1 --and pa.fee_type in (41,42,53)
AND pa.fee_type = 41
              AND pa.fee_amount > 0
              AND c.fee_type = 11
              AND pa.fee_status = 1 --and pa.policy_id = 755

            GROUP BY c.policy_id,
                        pa.due_time,
                        pa.fee_status,
                        pa.fee_amount) paid_amt
        GROUP BY policy_id,
                    due_time,
                    status,
                    collection_date,
                    fee_id,
                    collection_date_min,
                    fee_id_min) aa
    LEFT JOIN mv_cash ca ON ca.fee_id = aa.fee_id
    AND ca.fee_amount = aa.paid_amount
    LEFT JOIN mv_cash ca_min ON ca_min.fee_id = aa.fee_id_min --and (ca_min.fee_amount = aa.paid_amount)
LEFT JOIN t_bank_account acct ON acct.account_id = ca.account_id
    AND ca.account_id <> ca.internal_account
    LEFT JOIN t_bank_account acct_min ON acct.account_id = ca_min.account_id
    AND ca_min.account_id <> ca_min.internal_account
    LEFT JOIN
      (SELECT tcm.POLICY_ID,
                 tcp.RENEWAL_TYPE,
                 CASE WHEN tcm.INITIAL_VALI_DATE IS NULL THEN tcm.VALIDATE_DATE WHEN tcm.INITIAL_VALI_DATE IS NOT NULL
        AND TO_CHAR (tcm.INITIAL_VALI_DATE, 'DD') = TO_CHAR (tcm.VALIDATE_DATE, 'DD') THEN tcm.INITIAL_VALI_DATE ELSE TO_DATE ( TO_CHAR (tcm.INITIAL_VALI_DATE, 'YYYY') || '-' || TO_CHAR (tcm.VALIDATE_DATE, 'MM-DD'), 'YYYY-MM-DD') END EFFECTIVE_DATE
        FROM mv_CONTRACT_MASTER tcm,
              mv_CONTRACT_PRODUCT tcp
        WHERE tcm.POLICY_ID = tcp.POLICY_ID
          AND tcp.MASTER_ID IS NULL) C ON aa.POLICY_ID = C.POLICY_ID
    LEFT JOIN
      (SELECT policy_id,
                 due_time,
                 sum(fee_amount) AS refund_amount
        FROM mv_prem_arap
        WHERE fee_amount > 0
          AND fee_type = 42
          AND fee_status = 1
          AND offset_status = 1 
          and policy_id = :policy_id

        GROUP BY policy_id,
                    due_time) refund ON refund.policy_id = aa.policy_id
    AND refund.due_time = aa.due_time
    UNION SELECT NULL AS fee_id ,
                     billing.policy_id ,
                     PKG_LS_PUB_UTILS.F_POLICY_DURATION_YEAR (billing.DUE_TIME,EFFECTIVE_DATE)+ 1 AS POLICY_YEAR ,
                     (CASE WHEN PKG_LS_PUB_UTILS.F_POLICY_DURATION_MONTH ( billing.DUE_TIME, EFFECTIVE_DATE) > 0
                      AND (TO_CHAR (EFFECTIVE_DATE, 'DD') - TO_CHAR (billing.DUE_TIME, 'DD') = ABS(1)) THEN PKG_RPT_TSLI_COMMONS.GET_INSTALMENT_NO ( RENEWAL_TYPE, PKG_LS_PUB_UTILS.F_POLICY_DURATION_MONTH ( last_day(billing.DUE_TIME), last_day(EFFECTIVE_DATE))) WHEN PKG_LS_PUB_UTILS.F_POLICY_DURATION_MONTH ( billing.DUE_TIME, EFFECTIVE_DATE) > 0 THEN PKG_RPT_TSLI_COMMONS.GET_INSTALMENT_NO ( RENEWAL_TYPE, PKG_LS_PUB_UTILS.F_POLICY_DURATION_MONTH (billing.DUE_TIME, EFFECTIVE_DATE)) WHEN PKG_LS_PUB_UTILS.F_POLICY_DURATION_MONTH ( billing.DUE_TIME, EFFECTIVE_DATE) = 0
                      AND billing.DUE_TIME < EFFECTIVE_DATE THEN 1 ELSE 0 END) AS POLICY_PERIOD ,
                     billing.due_time,
                     NULL AS collection_date ,
                     billing.billing_premium AS apply_amount ,
                     'Waiting for Processing' AS status ,
                     ' ' AS collection_method ,
                     ' ' AS cheque_no ,
                     ' ' AS bank_code ,
                     ' ' AS bank_name ,
                     0 AS refund_amount
    FROM billing
         LEFT JOIN
            (SELECT tcm.POLICY_ID,
                          tcp.RENEWAL_TYPE,
                          CASE WHEN tcm.INITIAL_VALI_DATE IS NULL THEN tcm.VALIDATE_DATE WHEN tcm.INITIAL_VALI_DATE IS NOT NULL
              AND TO_CHAR (tcm.INITIAL_VALI_DATE, 'DD') = TO_CHAR (tcm.VALIDATE_DATE, 'DD') THEN tcm.INITIAL_VALI_DATE ELSE TO_DATE ( TO_CHAR (tcm.INITIAL_VALI_DATE, 'YYYY') || '-' || TO_CHAR (tcm.VALIDATE_DATE, 'MM-DD'), 'YYYY-MM-DD') END EFFECTIVE_DATE
              FROM mv_CONTRACT_MASTER tcm,
                      mv_CONTRACT_PRODUCT tcp
              WHERE tcm.POLICY_ID = tcp.POLICY_ID
                 AND tcp.MASTER_ID IS NULL) C ON billing.POLICY_ID = C.POLICY_ID)all_tran
 where
    all_tran.policy_id = :policy_id