-- mv_contract_master tcm, mv_insured_list til, mv_customer ins, tcm.policy_id = til.policy_id and til.party_id = customer_id
-- select agent > select customer > select policy > show policy detail

with w_agent_policy as (
	select
		tcm.service_agent as agent_id
		, tcm.policy_id as policy_id
		, cus.customer_id as customer_id
	from
		mv_contract_master tcm
		-- inner join mv_contract_product tcp on tcm.policy_id = tcp.policy_id
		inner join mv_insured_list til on tcm.policy_id = til.policy_id
		inner join mv_customer cus on cus.customer_id = til.party_id
	order by
		agent_id, policy_id, customer_id
)
, w_policy as (
	select
		tcm.policy_id
		, tcm.policy_code
		, tcm.suspend as policy_frozen
		-- , tcm.liability_state
		, case
			when tcm.liability_state=0 then
				(select status_desc from t_proposal_status psts where psts.proposal_status = pros.proposal_status)
			else 
				(select status_name from t_liability_status where status_id = tcm.liability_state)				
			end policy_status
		, (select cause_name from t_end_cause where cause_id = tcm.end_cause) as end_cause
		, case
			when (select sum(prem.total_prem_af) from mv_contract_product prem, mv_contract_extend prem_sts where prem.policy_id = tcm.policy_id and prem.liability_state = 1 and prem.item_id = prem_sts.item_id and prem.policy_id = prem_sts.policy_id and prem_sts.prem_status = 1) is null then (select sum(prem.total_prem_af) from mv_contract_product prem where prem.policy_id = tcm.policy_id )
			else (select sum(prem.total_prem_af) from mv_contract_product prem, mv_contract_extend prem_sts where prem.policy_id = tcm.policy_id and prem.liability_state = 1 and prem.item_id = prem_sts.item_id and prem.policy_id = prem_sts.policy_id and prem_sts.prem_status = 1)
			end policy_premium
		, life.product_name
		-- , service_agent_code
		-- , service_agent_name
		-- , service_agent_last_name
		-- , service_agent_home_tel
		-- , service_agent_office_tel
		-- , service_agent_mobile
	from mv_contract_master tcm
		inner join mv_contract_product tcp on tcm.policy_id=tcp.policy_id and tcp.master_id is null
		inner join t_product_life life on tcp.product_id = life.product_id and life.ins_type = 1
		inner join mv_contract_proposal pros on tcm.policy_id = pros.policy_id
)
, w_customer as (
	select
		ins.customer_id as customer_id
		, ins.first_name || ' ' || ins.last_name as insured_name
		, replace(ins.certi_code,substr(ins.certi_code,length(ins.certi_code)-3,4),'XXXX') as insured_pid
		, ins.gender as insured_gender
		, ins.birthday as insured_birthday
		, ins.home_tel as insured_home_tel
		, ins.office_tel as insured_office_tel
		, ins.mobile as insured_mobile
	from mv_customer ins
)
, w_agent_level as (
	select
		agent_id
		, channel_org_id
		, channel.channel_code
		, channel.channel_name
		, GET_CHANNEL_AGENT_HIERARCHY (agent.agent_id, 1) as GM
		, GET_CHANNEL_AGENT_HIERARCHY (agent.agent_id, 2) as AVP
		, GET_CHANNEL_AGENT_HIERARCHY (agent.agent_id, 3) as AL
	from mv_agent agent
		inner join mv_channel_org channel on channel.channel_id=agent.channel_org_id
)
, w_agent as (
	select
		agent.agent_id as wa_agent_id
		, agent.agent_code as wa_agent_code
		, cus.first_name || ' ' || cus.last_name as wa_agent_name
	from
		mv_agent agent
		inner join mv_customer cus on cus.customer_id = agent.agent_id
)
, w_channel_h as
(
select c_level.agent_id
	, (select agent_id from mv_agent where agent_id = c_level.al) as al_agent_id
	, (select agent_code from mv_agent where agent_id = c_level.al) as al_agent_code
	, (select first_name ||' '|| last_name from mv_customer where customer_id = c_level.al) as al_name
	, (select agent_id from mv_agent where agent_id = c_level.avp) as avp_agent_id
	, (select agent_code from mv_agent where agent_id = c_level.avp) as avp_agent_code
	, (select first_name ||' '|| last_name from mv_customer where customer_id = c_level.avp) as avp_name
	, (select agent_id from mv_agent where agent_id = c_level.gm) as gm_agent_id
	, (select agent_code from mv_agent where agent_id = c_level.gm) as gm_agent_code
	, (select first_name ||' '|| last_name from mv_customer where customer_id = c_level.gm) as gm_name
	, (SELECT CHANNEL_CODE ||' '|| CHANNEL_NAME FROM mv_CHANNEL_ORG WHERE CHANNEL_ID = GET_CHANNEL_HIERARCHY(c_level.CHANNEL_ORG_ID, 3)) Team
	, (SELECT CHANNEL_CODE ||' '|| CHANNEL_NAME FROM mv_CHANNEL_ORG WHERE CHANNEL_ID = GET_CHANNEL_HIERARCHY(c_level.CHANNEL_ORG_ID, 2)) Sale_office
	, (SELECT CHANNEL_CODE ||' '|| CHANNEL_NAME FROM mv_CHANNEL_ORG WHERE CHANNEL_ID = GET_CHANNEL_HIERARCHY(c_level.CHANNEL_ORG_ID, 1)) Branch
from (select agent_id
	, channel_org_id
	, GET_CHANNEL_AGENT_HIERARCHY (agent.agent_id, 1) as GM
	, GET_CHANNEL_AGENT_HIERARCHY (agent.agent_id, 2) as AVP
	, GET_CHANNEL_AGENT_HIERARCHY (agent.agent_id, 3) as AL
	from mv_agent agent) c_level
)
, w_customer as
(
	select *
	from mv_customer
)
, w_agent_h as
(
select agent_id
	, (select agent_code from mv_agent where agent_id = p_level.level_1) as level1_agent_code
	, (select first_name ||' '|| last_name from mv_customer where customer_id = p_level.level_1 )as level1_agent_name
	, (select agent_code from t_agent where agent_id = p_level.level_2) as level2_agent_code
	, (select first_name ||' '|| last_name from mv_customer where customer_id = p_level.level_2 )as level2_agent_name
	, (select agent_code from t_agent where agent_id = p_level.level_3) as level3_agent_code
	, (select first_name ||' '|| last_name from mv_customer where customer_id = p_level.level_3 )as level3_agent_name
	, (select agent_code from t_agent where agent_id = p_level.level_4) as level4_agent_code
	, (select first_name ||' '|| last_name from mv_customer where customer_id = p_level.level_4 )as level4_agent_name
	, (select agent_code from t_agent where agent_id = p_level.level_5) as level5_agent_code
	, (select first_name ||' '|| last_name from mv_customer where customer_id = p_level.level_5 )as level5_agent_name
from
(select agent_id
, GET_AGENT_HIERARCHY (AGENT_ID, 1) as LEVEL_1
, GET_AGENT_HIERARCHY (AGENT_ID, 2) as LEVEL_2
, GET_AGENT_HIERARCHY (AGENT_ID, 3) as LEVEL_3
, GET_AGENT_HIERARCHY (AGENT_ID, 4) as LEVEL_4
, GET_AGENT_HIERARCHY (AGENT_ID, 5) as LEVEL_5

FROM MV_AGENT
WHERE AGENT_CATE IN ('L','F','D','E')) p_level
)

select ins.*
from w_customer ins
where
	ins.customer_id=:customer_id
