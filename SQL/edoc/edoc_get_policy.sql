SELECT 
	pol.*
	,tpl.TEMPLATE_NAME_TH 
	,doc.EXPORT_FILE_PATH as path
FROM DH_T_POLICY_EDOC pol 
	LEFT JOIN DH_T_TEMPLATE tpl 
		ON tpl.TEMPLATE_ID=pol.TEMPLATE_ID 
	LEFT JOIN DH_T_POLICY_EDOC_DET doc
		ON doc.DOCUMENT_NO=pol.DOCUMENT_NO
WHERE pol.certi_code=:pid
ORDER BY pol.DOCUMENT_DATE DESC