-- list_policy_product
--Product list
--coverage_period = 3, charge_period = 3, AGE
with w_age as (
select
    distinct a.policy_id
    , a.party_id
    , b.entry_age
    , case when e_joiner.product_id is not null then 'JointLife' else 'Others' end Product_type,case when e_joiner.product_id is not null and (relation_to_ph IN ('6', '7') OR entry_age < 15) then 'Joiner' else 'Insured' end Type
from mv_insured_list a
    inner join mv_benefit_insured b on b.insured_id = a.list_id and b.policy_id = a.policy_id
    inner join mv_contract_product c on c.policy_id = a.policy_id and c.master_id is null and b.item_id = c.item_id
    left join t_product_life e_joiner on e_joiner.product_id = c.product_id and c.product_id = e_joiner.product_id and e_joiner.ins_type = 1 and e_joiner.ally = '1')
-- end with w_age
select
    -- distinct 
	tcp.policy_id
    , tcp.item_id
    -- tcp.item_id
    , tcp.master_id
    , case when tcp.origin_product_id is not null then (select internal_id from t_product_life life where LIFE.PRODUCT_ID = tcp.origin_product_id) else tpl.internal_id end product_code
    , case when tcp.origin_product_id is not null then (select product_name from t_product_life life where LIFE.PRODUCT_ID = tcp.origin_product_id) else tpl.product_name end product_name
    , tcp.amount as sum_assured
--    , case when prem_sts.prem_status=1 then tcp.next_total_prem_af else 0 end total_premium
    , tcp.next_total_prem_af as total_premium
    , case when tcp.coverage_period = 3 then 'A' else '' end
        || tcp.coverage_year as coverage_year
    , case when tcp.charge_period = 3 then 'A' else '' end || tcp.charge_year as charge_year
    , case when tcp.coverage_period = 3 then tcp.coverage_year - age.entry_age else tcp.coverage_year end coverage_in_year
    , case when tcp.charge_period = 3 then tcp.charge_year - age.entry_age else tcp.charge_year end charge_in_year
    , tcp.validate_date as effective_date
    , tcp.expiry_date as coverage_end_date
    , tcp.paidup_date
    , tls.status_name as benefit_status
    , tcp.liability_state_date as benefit_status_date
    , case when tce.prem_status is null then tcp.item_id || '-' || tcp.policy_id
        else (select status_name from t_prem_status tps where tps.status_id = tce.prem_status) end as premium_status
    , (CASE 
--            WHEN tcm.suspend='Y' AND tcm.suspend_cause<>'99' THEN 'อยู่ระหว่างดำเนินการ กรุณาติดต่อทางบริษัท'
            WHEN tcp.liability_state=3 THEN
                CASE WHEN tcp.end_cause = 51 THEN 'สิ้นผลบังคับ/ขอยกเลิกกรมธรรม์ (Freelook)'
                    WHEN tcp.end_cause = 31 THEN 'สิ้นผลบังคับ/เวนคืนกรมธรรม์'
                    WHEN tcp.end_cause = 12 THEN 'สิ้นสุดความคุ้มครอง'
                    WHEN tcp.end_cause = 34 THEN 'ยกเลิกกรมธรรม์'
                    WHEN tcp.end_cause = 21 THEN 'สิ้นผลบังคับ'
                    WHEN tcp.end_cause = 35 THEN 'ยกเลิกกรมธรรม์'
                    WHEN tcp.end_cause = 22 OR tcp.end_cause = 23 THEN 'สิ้นผลบังคับ'
                    ELSE 'สิ้นผลบังคับ (' || tcp.end_cause || ')'
                END
            WHEN tcp.liability_state=2 THEN 'ขาดอายุ'
            WHEN tcp.liability_state=1 THEN
                CASE WHEN tce.prem_status is null THEN 'มีผลบังคับ'
                    WHEN tce.prem_status = 11 THEN 'มีผลบังคับ/ขยายระยะเวลาอัตโนมัติ'
                    WHEN tce.prem_status = 8 THEN 'มีผลบังคับ/ขยายระยะเวลา'
                    WHEN tce.prem_status = 6 THEN 'มีผลบังคับ/มูลค่าใช้เงินสำเร็จอัตโนมัติ'
                    WHEN tce.prem_status = 3 THEN 'มีผลบังคับ/มูลค่าใช้เงินสำเร็จ'
                     WHEN apl.sum_apl > 0 THEN 'มีผลบังคับ/กู้อัตโนมัติชำระเบี้ย'
                    WHEN tce.prem_status = 1 THEN 'มีผลบังคับ'
                    WHEN tce.prem_status = 2 or tce.prem_status = 4 THEN 'มีผลบังคับ (' || tce.prem_status || ')'
                    ELSE 'มีผลบังคับ (' || tce.prem_status || ')'
                END
            WHEN tcp.liability_state=0 THEN
                    CASE 
						WHEN pros.proposal_status=32 THEN 'อยู่ระหว่างการพิจารณา'
						WHEN pros.proposal_status=80 THEN 'อยู่ระหว่างการพิจารณา (Accepted)'
						WHEN pros.proposal_status=81 THEN 'อยู่ระหว่างการพิจารณา (Conditionally Accepted)'
						ELSE ' อยู่ระหว่างการพิจารณา (' || psts.status_desc || ')'
                    END
            ELSE 'ไม่ระบุ'
        END) as PREM_STATUS_NAME
    , tce.due_date as next_due_date
from
    mv_contract_master tcm 
    INNER JOIN mv_contract_product tcp ON tcm.policy_id=tcp.policy_id
    LEFT JOIN mv_contract_extend prem_sts ON prem_sts.item_id = tcp.item_id
    left join mv_contract_extend tce on tce.item_id = tcp.item_id and tce.policy_id = tcp.policy_id
    -- inner join (select policy_code,policy_id from mv_contract_master) tcm on tcp.policy_id = tcm.policy_id
    inner join t_product_life tpl on tcp.product_id = tpl.product_id
    inner join t_liability_status tls on tls.status_id = tcp.liability_state
    -- LEFT JOIN mv_agent agent ON tcm.service_agent = agent.agent_id
    LEFT JOIN mv_contract_proposal pros ON pros.policy_id = tcm.policy_id
    -- LEFT JOIN t_prem_status sts ON sts.status_id = tce.prem_status
    LEFT JOIN t_proposal_status psts ON psts.proposal_status = pros.proposal_status    
    inner join (select * from w_age where product_type = 'Others' or 
		(product_type = 'JointLife' and Type = 'Joiner')) age 
        on age.policy_id = tcp.policy_id
    -- left join t_prem_status on tce.prem_status = t_prem_status.status_id
    -- left join t_end_cause on t_end_cause.cause_id = tcp.end_cause
     left join (select acc.policy_id,mas.policy_code,
        sum (case when acc.account_type in (53) then acc.capital_balance + acc.interest_balance else 0 end) as sum_apl
        from mv_policy_account acc, mv_contract_master mas
        where acc.policy_id = mas.policy_id and mas.suspend <> 'y'
        group by acc.policy_id, mas.policy_code
        having sum (case when acc.account_type in (53) then acc.capital_balance + acc.interest_balance else 0 end) > 0) apl on tcm.policy_id=apl.policy_id
where
    tcm.policy_code = :policy_code
order by
    tcp.policy_id
    , tcp.item_id