WITH addr_list AS
  (SELECT tpa.party_id,
          tpa.contact_type,
          addr.*
   FROM mv_address addr,
                   mv_party_addr tpa
   WHERE addr.address_id = tpa.address_id
     AND tpa.party_addr_id IN
       (SELECT max(party_addr_id) AS party_addr_id
        FROM mv_party_addr list
        WHERE tpa.contact_type = list.contact_type
          AND tpa.party_id = list.party_id
        GROUP BY party_id,
                 contact_type)),
     agent_level AS
  ( SELECT tcm.policy_id,
           agent.agent_code AS service_agent_code,
           cagent.first_name AS service_agent_name,
           cagent.last_name AS service_agent_last_name,
           cagent.home_tel AS service_agent_home_tel,
           cagent.office_tel AS service_agent_office_tel,
           cagent.mobile AS service_agent_mobile,

     (SELECT agent_code
      FROM mv_agent
      WHERE agent_id = c_level.al) AS AL_serviceagent_code,

     (SELECT first_name ||' '|| last_name
      FROM mv_customer
      WHERE customer_id = c_level.al) AS AL_serviceAgent_name,

     (SELECT agent_code
      FROM mv_agent
      WHERE agent_id = c_level.avp) AS avp_serviceagent_code,

     (SELECT first_name ||' '|| last_name
      FROM mv_customer
      WHERE customer_id = c_level.avp) AS AVP_serviceAgent_name,

     (SELECT agent_code
      FROM mv_agent
      WHERE agent_id = c_level.gm) AS gm_serviceagent_code,

     (SELECT first_name ||' '|| last_name
      FROM mv_customer
      WHERE customer_id = c_level.gm) AS gm_serviceagent_name,
           iss_agent.agent_code AS issue_agent_code,
           iss_cagent.first_name AS issue_agent_name,
           iss_cagent.last_name AS issue_agent_last_name,
           iss_cagent.home_tel AS issue_agent_home_tel,
           iss_cagent.office_tel AS issue_agent_office_tel,
           iss_cagent.mobile AS issue_agent_mobile,

     (SELECT agent_code
      FROM mv_agent
      WHERE agent_id = c_level_i.al) AS AL_issueagent_code,

     (SELECT first_name ||' '|| last_name
      FROM mv_customer
      WHERE customer_id = c_level_i.al) AS AL_issueAgent_name,

     (SELECT agent_code
      FROM mv_agent
      WHERE agent_id = c_level_i.avp) AS avp_issueagent_code,

     (SELECT first_name ||' '|| last_name
      FROM mv_customer
      WHERE customer_id = c_level_i.avp) AS AVP_issueAgent_name,

     (SELECT agent_code
      FROM mv_agent
      WHERE agent_id = c_level_i.gm) AS gm_issueagent_code,

     (SELECT first_name ||' '|| last_name
      FROM mv_customer
      WHERE customer_id = c_level_i.gm) AS gm_issueagent_name
   FROM mv_contract_master tcm
   INNER JOIN mv_agent agent ON agent.agent_id = tcm.service_agent
   INNER JOIN mv_customer cagent ON cagent.customer_id = agent.agent_id
   LEFT JOIN mv_contract_product tcp ON tcm.policy_id = tcp.policy_id
   AND tcp.master_id IS NULL
   LEFT JOIN mv_agent iss_agent ON iss_agent.agent_id = tcp.issue_agent
   LEFT JOIN mv_customer iss_cagent ON iss_cagent.customer_id = iss_agent.agent_id
   LEFT JOIN
     (SELECT policy_id,
             GET_CHANNEL_AGENT_HIERARCHY (a.service_agent, 3) AS AL,
             GET_CHANNEL_AGENT_HIERARCHY (a.service_agent, 2) AS AVP,
             GET_CHANNEL_AGENT_HIERARCHY (a.service_agent, 1) AS GM
      FROM mv_contract_master a) c_level ON c_level.policy_id = tcm.policy_id
   LEFT JOIN
     (SELECT policy_id,
             GET_CHANNEL_AGENT_HIERARCHY (a.issue_agent, 3) AS AL,
             GET_CHANNEL_AGENT_HIERARCHY (a.issue_agent, 2) AS AVP,
             GET_CHANNEL_AGENT_HIERARCHY (a.issue_agent, 1) AS GM
      FROM mv_contract_product a
      WHERE a.master_id IS NULL) c_level_i ON c_level_i.policy_id = tcm.policy_id)
SELECT tcm.policy_id,
       tcm.policy_code,
       tcm.suspend AS frozen,
       CASE
           WHEN tcm.liability_state = 0 THEN
                  (SELECT status_desc
                   FROM t_proposal_status psts
                   WHERE psts.proposal_status = pros.proposal_status)
           ELSE
                  (SELECT status_name
                   FROM t_liability_status
                   WHERE status_id = tcm.liability_state)
       END policy_status,

  (SELECT cause_name
   FROM t_end_cause
   WHERE cause_id = tcm.end_cause) AS end_cause,
       title_ins.title_desc || ' ' || ins.first_name||' '|| ins.last_name AS insured_name,
		 
       ins.first_name AS insured_fname,
       ins.last_name AS insured_lname,
       replace(ins.certi_code,substr(ins.certi_code,length(ins.certi_code)-3,4),'XXXX') AS insured_id,
       tbi.entry_age AS insured_entry_age,
       ins.gender AS insured_gender,
       ins.birthday AS insured_birthday,
		 
		 life.product_name AS product_name,
		 
		 tcm.liability_state_date as policy_status_date,
       -- joiner.joiner_name,
       -- joiner.joiner_id,
       -- joiner.joiner_entry_age,
       -- joiner.joiner_birthday,
       -- addr_ins.POST_CODE AS ins_addrcontact_NO,
       -- addr_ins.ADDRESS_7 AS ins_addrcontact_MOOBAN_build,
       -- addr_ins.MOO AS ins_addrcontact_MOO,
       -- addr_ins.ADDRESS_6 AS ins_addrcontact_SOI,
       -- addr_ins.ADDRESS_1 AS ins_addrcontact_ROAD,
       -- addr_ins.ADDRESS_2 AS ins_addrcontact_SUB,
       -- addr_ins.ADDRESS_3 AS ins_addrcontact_DISTRICT,
       -- addr_ins.ADDRESS_4 AS ins_addrcontact_PROVINCE,
       -- addr_ins.ZIP_CODE AS ins_addrcontact_ZIPCODE,
       -- addr_home.POST_CODE AS ins_addrhome_NO,
       -- addr_home.ADDRESS_7 AS ins_addrhome_MOOBAN_build,
       -- addr_home.MOO AS ins_addrhome_MOO,
       -- addr_home.ADDRESS_6 AS ins_addrhome_SOI,
       -- addr_home.ADDRESS_1 AS ins_addrhome_ROAD,
       -- addr_home.ADDRESS_2 AS ins_addrhome_SUB,
       -- addr_home.ADDRESS_3 AS ins_addrhome_DISTRICT,
       -- addr_home.ADDRESS_4 AS ins_addrhome_PROVINCE,
       -- addr_home.ZIP_CODE AS ins_addrhome_ZIPCODE,
       -- addr_office.POST_CODE AS ins_addroffc_NO,
       -- addr_office.ADDRESS_7 AS ins_addroffc_MOOBAN_build,
       -- addr_office.MOO AS ins_addroffc_MOO,
       -- addr_office.ADDRESS_6 AS ins_addroffc_SOI,
       -- addr_office.ADDRESS_1 AS ins_addroffc_ROAD,
       -- addr_office.ADDRESS_2 AS ins_addroffc_SUB,
       -- addr_office.ADDRESS_3 AS ins_addroffc_DISTRICT,
       -- addr_office.ADDRESS_4 AS ins_addroffc_PROVINCE,
       -- addr_office.ZIP_CODE AS ins_addroffc_ZIPCODE,
       -- addr_current.POST_CODE AS ins_addrcurr_NO,
       -- addr_current.ADDRESS_7 AS ins_addrcurr_MOOBAN_build,
       -- addr_current.MOO AS ins_addrcurr_MOO,
       -- addr_current.ADDRESS_6 AS ins_addrcurr_SOI,
       -- addr_current.ADDRESS_1 AS ins_addrcurr_ROAD,
       -- addr_current.ADDRESS_2 AS ins_addrcurr_SUB,
       -- addr_current.ADDRESS_3 AS ins_addrcurr_DISTRICT,
       -- addr_current.ADDRESS_4 AS ins_addrcurr_PROVINCE,
       -- addr_current.ZIP_CODE AS ins_addrcurr_ZIPCODE,
       ins.home_tel AS insured_home_tel,
       ins.office_tel AS insured_office_tel,
       ins.mobile AS insured_mobile,
       CASE
           WHEN cpay.customer_id IS NOT NULL THEN title_pay.title_desc || ' ' || cpay.first_name||' '|| cpay.last_name
           ELSE com_pay.company_name
       END payer_name,
       -- addr_pay.POST_CODE AS billing_addr_NO,
       -- addr_pay.ADDRESS_7 AS billing_addr_MOOBAN_build,
       -- addr_pay.MOO AS billing_addr_MOO,
       -- addr_pay.ADDRESS_6 AS billing_addr_SOI,
       -- addr_pay.ADDRESS_1 AS billing_addr_ROAD,
       -- addr_pay.ADDRESS_2 AS billing_addr_SUB,
       -- addr_pay.ADDRESS_3 AS billing_addr_DISTRICT,
       -- addr_pay.ADDRESS_4 AS billing_addr_PROVINCE,
       -- addr_pay.ZIP_CODE AS billing_addr_ZIPCODE,
       -- cpay.home_tel AS insured_home_tel,
       -- cpay.office_tel AS insured_office_tel,
       -- cpay.mobile AS insured_mobile,
       agent_level.service_agent_code,
       agent_level.service_agent_name || ' ' || agent_level.service_agent_last_name as service_agent_name,
       agent_level.service_agent_home_tel,
       agent_level.service_agent_office_tel,
       agent_level.service_agent_mobile,
       agent_level.AVP_serviceAgent_code,
       agent_level.AVP_serviceAgent_name,
       agent_level.GM_serviceAgent_code,
       agent_level.GM_serviceAgent_name,
       agent_level.AL_serviceagent_code,
       agent_level.AL_serviceAgent_name ,
       agent_level.issue_agent_code,
       agent_level.issue_agent_name,
       agent_level.issue_agent_last_name,
       agent_level.issue_agent_home_tel,
       agent_level.issue_agent_office_tel,
       agent_level.issue_agent_mobile,
       agent_level.AVP_issueAgent_code,
       agent_level.AVP_issueAgent_name,
       agent_level.GM_issueAgent_code,
       agent_level.GM_issueAgent_name,
       agent_level.AL_issueagent_code,
       agent_level.AL_issueAgent_name ,

  (SELECT mode_name
   FROM t_pay_mode
   WHERE tcp.pay_next = mode_id) AS payment_method,

  (SELECT charge_name
   FROM t_charge_mode
   WHERE tcp.renewal_type = charge_type) AS frequency_payment,
       CASE
           WHEN
                  (SELECT sum(prem.total_prem_af)
                   FROM mv_contract_product prem,
                                            mv_contract_extend prem_sts
                   WHERE prem.policy_id = tcm.policy_id
                     AND prem.liability_state = 1
                     AND prem.item_id = prem_sts.item_id
                     AND prem.policy_id = prem_sts.policy_id
                     AND prem_sts.prem_status = 1) IS NULL THEN
                  (SELECT sum(prem.total_prem_af)
                   FROM mv_contract_product prem
                   WHERE prem.policy_id = tcm.policy_id)
           ELSE
                  (SELECT sum(prem.total_prem_af)
                   FROM mv_contract_product prem,
                                            mv_contract_extend prem_sts
                   WHERE prem.policy_id = tcm.policy_id
                     AND prem.liability_state = 1
                     AND prem.item_id = prem_sts.item_id
                     AND prem.policy_id = prem_sts.policy_id
                     AND prem_sts.prem_status = 1)
       END total_premium
FROM mv_contract_master tcm
INNER JOIN mv_contract_product tcp ON tcp.policy_id = tcm.policy_id
AND tcp.master_id IS NULL
INNER JOIN mv_contract_proposal pros ON pros.policy_id = tcm.policy_id
INNER JOIN mv_insured_list til ON tcm.policy_id = til.policy_id
INNER JOIN mv_benefit_insured tbi ON tcm.policy_id = tbi.policy_id
AND tbi.insured_id = til.list_id
AND tbi.item_id = tcp.item_id
INNER JOIN mv_customer ins ON til.party_id = ins.customer_id
LEFT JOIN t_title title_ins ON title_ins.title_code = ins.honor_title
INNER JOIN t_product_life life ON tcp.product_id = life.product_id
AND life.ins_type = 1
INNER JOIN mv_address addr_ins ON addr_ins.address_id = til.address_id
-- LEFT JOIN addr_list addr_home ON til.party_id = addr_home.party_id AND addr_home.contact_type = 1
-- LEFT JOIN addr_list addr_office ON til.party_id = addr_office.party_id AND addr_office.contact_type = 2
-- LEFT JOIN addr_list addr_current ON til.party_id = addr_current.party_id AND addr_current.contact_type = 3
INNER JOIN mv_payer payer ON payer.policy_id = tcm.policy_id
LEFT JOIN mv_customer cpay ON payer.party_id = cpay.customer_id
LEFT JOIN t_title title_pay ON title_pay.title_code = cpay.honor_title
LEFT JOIN mv_company_customer com_pay ON payer.party_id = com_pay.company_id
-- LEFT JOIN mv_address addr_pay ON addr_pay.address_id = payer.address_id
LEFT JOIN agent_level ON agent_level.policy_id = tcm.policy_id
-- LEFT JOIN
  -- (SELECT tcm.policy_id,
          -- title_ins.title_desc || ' ' || ins.first_name||' '|| ins.last_name AS joiner_name,
          -- replace(ins.certi_code,substr(ins.certi_code,length(ins.certi_code)-3,4),'XXXX') AS joiner_id,
          -- entry_age AS joiner_entry_age,
          -- birthday AS joiner_birthday
   -- FROM mv_contract_master tcm
   -- INNER JOIN mv_contract_product tcp ON tcp.policy_id = tcm.policy_id
   -- AND tcp.master_id IS NULL
   -- INNER JOIN mv_insured_list til ON tcm.policy_id = til.policy_id
   -- INNER JOIN mv_benefit_insured tbi ON tcm.policy_id = tbi.policy_id
   -- AND tbi.insured_id = til.list_id
   -- AND tbi.item_id = tcp.item_id
   -- INNER JOIN mv_customer ins ON til.party_id = ins.customer_id
   -- LEFT JOIN t_title title_ins ON title_ins.title_code = ins.honor_title
   -- INNER JOIN t_product_life life ON tcp.product_id = life.product_id
   -- AND life.ins_type = 1
   -- WHERE ally = 1
     -- AND (til.relation_to_ph IN ('6','7')
          -- OR tbi.entry_age < 15)) joiner ON joiner.policy_id = tcm.policy_id
WHERE NOT (ally = 1
           AND (til.relation_to_ph IN ('6',
                                       '7')
                OR tbi.entry_age < 15)) AND agent_level.service_agent_code=:agent_code