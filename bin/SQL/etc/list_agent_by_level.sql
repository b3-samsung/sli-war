WITH w_producer as (
SELECT 
    PRODUCER_ID,
    PRODUCER_POSITION,
    UPLINE_ID,
    UPLINE_POSITION
FROM (SELECT DISTINCT
     PRODUCER_ID,
     PRODUCER_POSITION,
     UPLINE_ID,
     UPLINE_POSITION,
     ROW_NUMBER ()
     OVER (PARTITION BY PRODUCER_ID
           ORDER BY UPDATE_TIME DESC)
        "RN"
FROM MV_PRODUCER_HIERARCHY
WHERE 1=1
     AND HIERARCHY_TYPE = '1'
     AND ACTIVE_INDI = 'Y'
     AND CHANGE_TYPE NOT IN ('10')
     AND TO_CHAR (END_DATE, 'YYYYMMDD') ='99991231') A
WHERE A.RN=1
)
, w_agent as (
select 
     agent_id
     , agent_code
     -- , agent_status || '-' || agent_substatus as status
    -- , upline_id, producer_id, producer_position
    , level as agent_level
    , connect_by_root upline_id as top
    , upline_id as parent
    -- , connect_by_root upline_id || sys_connect_by_path(producer_id,',') as path
from w_producer tph
    inner join mv_agent agent on agent.agent_id=tph.producer_id
connect by upline_id=prior producer_id
start with upline_id=:agent_id
order by level)
, w_policy_count as (
	select count(policy_id) as policy_count, service_agent as agent_id
	from mv_contract_master 
	group by service_agent)

select agent.*
    -- , cus.first_name, cus.last_name
    , cus.first_name || ' ' || cus.last_name as agent_name
	 , COALESCE(policy_count,0) as policy_count
from w_agent agent 
    inner join mv_customer cus on cus.customer_id=agent.agent_id
	 left join w_policy_count cnt on cnt.agent_id=agent.agent_id 
order by agent_level, agent.agent_code
