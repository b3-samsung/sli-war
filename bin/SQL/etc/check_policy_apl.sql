	,(CASE 
		WHEN tcp.liability_state = 1 THEN
			CASE
				WHEN tce.prem_status is null THEN
					'มีผลบังคับ'
				WHEN tce.prem_status = 11 THEN
					'มีผลบังคับ/ขยายระยะเวลาอัตโนมัติ'
				WHEN tce.prem_status = 8 THEN
					'มีผลบังคับ/ขยายระยะเวลา'
				WHEN tce.prem_status = 6 THEN
					'มีผลบังคับ/มูลค่าใช้เงินสำเร็จอัตโนมัติ'
				WHEN tce.prem_status = 3 THEN
					'มีผลบังคับ/มูลค่าใช้เงินสำเร็จ'
				WHEN tce.prem_status = 1 OR tce.prem_status = 2 or tce.prem_status = 4 THEN
					'มีผลบังคับ'
				ELSE
					'มีผลบังคับ (' || t_prem_status.status_name || ')'
			END
		WHEN tcp.liability_state = 2 THEN
			'ขาดอายุ'
		WHEN tcp.liability_state = 3 THEN
			CASE
				WHEN tcp.end_cause = 51 THEN
					'สิ้นผลบังคับ/ขอยกเลิกกรมธรรม์ (Freelook)'
				WHEN tcp.end_cause = 31 THEN
					'สิ้นผลบังคับ/เวนคืนกรมธรรม์'
				WHEN tcp.end_cause = 12 THEN
					'สิ้นสุดความคุ้มครอง'
				WHEN tcp.end_cause = 34 THEN
					'ยกเลิกกรมธรรม์'
				WHEN tcp.end_cause = 21 THEN
					'สิ้นผลบังคับ'
				WHEN tcp.end_cause = 35 THEN
					'ยกเลิกกรมธรรม์'
				WHEN tcp.end_cause = 22 OR tcp.end_cause = 23 THEN
					'สิ้นผลบังคับ'
				ELSE
					'สิ้นผลบังคับ (' || t_end_cause.cause_name || ')'
			END
		END) as prem_status_name

SELECT 
	ACC.POLICY_ID
	,MAS.POLICY_CODE
	,SUM (CASE WHEN ACC.ACCOUNT_TYPE IN (53) THEN ACC.CAPITAL_BALANCE + ACC.INTEREST_BALANCE ELSE 0 END) AS SUM_APL
FROM 
	MV_POLICY_ACCOUNT ACC
	, MV_CONTRACT_MASTER MAS
WHERE 
	ACC.POLICY_ID = MAS.POLICY_ID
	AND MAS.SUSPEND <> 'Y'
GROUP 
	BY ACC.POLICY_ID, MAS.POLICY_CODE
HAVING 
	SUM (CASE WHEN ACC.ACCOUNT_TYPE IN (53) THEN ACC.CAPITAL_BALANCE + ACC.INTEREST_BALANCE ELSE 0 END) > 0
	
