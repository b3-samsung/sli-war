WITH w_producer as (
SELECT 
    PRODUCER_ID,
    PRODUCER_POSITION,
    UPLINE_ID,
    UPLINE_POSITION
FROM (SELECT DISTINCT
     PRODUCER_ID,
     PRODUCER_POSITION,
     UPLINE_ID,
     UPLINE_POSITION,
     ROW_NUMBER ()
     OVER (PARTITION BY PRODUCER_ID
           ORDER BY UPDATE_TIME DESC)
        "RN"
FROM MV_PRODUCER_HIERARCHY
WHERE HIERARCHY_TYPE = '1'
     AND ACTIVE_INDI = 'Y'
     AND CHANGE_TYPE NOT IN ('10')
     AND TO_CHAR (END_DATE, 'YYYYMMDD') ='99991231') A
WHERE A.RN=1
)
, w_agent as (
select agent.*
	-- agent_id
    -- , agent_code
    -- , agent_status || '-' || agent_substatus as status
--   , cus.first_name || ' ' || cus.last_name
    -- , upline_id, producer_id, producer_position
   -- , level as agent_level
   -- , connect_by_root upline_id as top
   -- , connect_by_root upline_id || sys_connect_by_path(producer_id,',') as path
--   , tph.* 
from w_producer tph
    inner join mv_agent agent on agent.agent_id=tph.producer_id --and agent.agent_status=1 --and agent.agent_substatus IN (1,2)
--    inner join mv_customer cus on cus.customer_id=agent.agent_id
connect by upline_id=prior producer_id
start with upline_id in (select agent_id from mv_agent where agent_code=:login_agent_code)
order by level),
-- w_charge_mode
w_charge_mode AS
(SELECT charge_type
	, (CASE
        WHEN charge_type=1 THEN 'ราย 12  เดือน'
        WHEN charge_type=2 THEN 'ราย 6 เดือน'
        WHEN charge_type=3 THEN 'ราย 3 เดือน'
        WHEN charge_type=4 THEN 'รายเดือน'
        WHEN charge_type=5 THEN 'ชำระครั้งเดียว'
        ELSE '(' || charge_type || ':' || charge_name || ')'
       END) as charge_name
FROM t_charge_mode),
-- w_pay_mode
w_pay_mode as (
SELECT mode_id
    , (CASE
        WHEN mode_id=1 THEN 'เงินสด'
        WHEN mode_id=3 THEN 'หักบัญชีเงินฝากอัตโนมัติ'
        WHEN mode_id=30 THEN 'หักบัตรเครดิตต่อเนื่อง'
        ELSE '(' || mode_id || ':' || mode_name || ')'
       END) as mode_name
FROM t_pay_mode),
-- with w_policy_status
	w_policy_status as (SELECT 
    tcm.policy_id as policy_id
--    , tce.prem_status, tcm.liability_state, pros.proposal_status as tmp_id
     , (CASE 
--            WHEN tcm.suspend='Y' AND tcm.suspend_cause<>'99' THEN 'อยู่ระหว่างดำเนินการ กรุณาติดต่อทางบริษัท'
            WHEN tcm.liability_state=3 THEN
                CASE WHEN tcp.end_cause = 51 THEN 'สิ้นผลบังคับ/ขอยกเลิกกรมธรรม์ (Freelook)'
                    WHEN tcp.end_cause = 31 THEN 'สิ้นผลบังคับ/เวนคืนกรมธรรม์'
                    WHEN tcp.end_cause = 12 THEN 'สิ้นสุดความคุ้มครอง'
                    WHEN tcp.end_cause IS NULL THEN 'สิ้นผลบังคับ'
                    WHEN tcp.end_cause = 21 THEN 'สิ้นผลบังคับ'
                    WHEN tcp.end_cause = 22 OR tcp.end_cause = 23 THEN 'สิ้นผลบังคับ'
                    WHEN tcp.end_cause = 34 THEN 'ยกเลิกกรมธรรม์'
                    WHEN tcp.end_cause = 35 THEN 'ยกเลิกกรมธรรม์'
                    ELSE ' สิ้นผลบังคับ (' || end_cause.cause_name || ')'
                END
            WHEN tcm.liability_state=2 THEN 'ขาดอายุ'
            WHEN tcm.liability_state=1 THEN
                CASE WHEN tce.prem_status is null THEN 'มีผลบังคับ'
                    WHEN tce.prem_status = 11 THEN 'มีผลบังคับ/ขยายระยะเวลาอัตโนมัติ'
                    WHEN tce.prem_status = 8 THEN 'มีผลบังคับ/ขยายระยะเวลา'
                    WHEN tce.prem_status = 6 THEN 'มีผลบังคับ/มูลค่าใช้เงินสำเร็จอัตโนมัติ'
                    WHEN tce.prem_status = 3 THEN 'มีผลบังคับ/มูลค่าใช้เงินสำเร็จ'
                     WHEN apl.sum_apl > 0 THEN 'มีผลบังคับ/กู้อัตโนมัติชำระเบี้ย'
                    WHEN tce.prem_status = 1 OR tce.prem_status = 2 or tce.prem_status = 4 THEN 'มีผลบังคับ'
                    ELSE ' มีผลบังคับ (' || prem_sts.status_name || ')'
                END
            WHEN tcm.liability_state=0 THEN
					CASE 
						WHEN pros.proposal_status=32 THEN 'อยู่ระหว่างการพิจารณา'
						WHEN pros.proposal_status=80 THEN 'อยู่ระหว่างการพิจารณา (Accepted)'
						WHEN pros.proposal_status=81 THEN 'อยู่ระหว่างการพิจารณา (Conditionally Accepted)'
						ELSE ' อยู่ระหว่างการพิจารณา (' || pro_sts.status_desc || ')'
					END
            ELSE 'ไม่ระบุ'
        END) as status_name
FROM mv_contract_master tcm 
    INNER JOIN mv_contract_product tcp ON tcm.policy_id=tcp.policy_id AND tcp.master_id IS NULL
    LEFT JOIN mv_agent agent ON tcm.service_agent = agent.agent_id
    LEFT JOIN mv_contract_extend tce ON tce.item_id = tcp.item_id AND tce.policy_id = tcp.policy_id
    LEFT JOIN mv_contract_proposal pros ON pros.policy_id = tcm.policy_id
    LEFT JOIN t_proposal_status pro_sts ON pro_sts.proposal_status = pros.proposal_status
	LEFT JOIN t_prem_status prem_sts ON prem_sts.status_id = tce.prem_status
	LEFT JOIN t_end_cause end_cause ON end_cause.cause_id = tcp.end_cause
	 LEFT JOIN (SELECT ACC.POLICY_ID,MAS.POLICY_CODE,
        SUM (CASE WHEN ACC.ACCOUNT_TYPE IN (53) THEN ACC.CAPITAL_BALANCE + ACC.INTEREST_BALANCE ELSE 0 END) AS SUM_APL
        FROM MV_POLICY_ACCOUNT ACC, MV_CONTRACT_MASTER MAS
        WHERE ACC.POLICY_ID = MAS.POLICY_ID AND MAS.SUSPEND <> 'Y'
        GROUP BY ACC.POLICY_ID, MAS.POLICY_CODE
        having SUM (CASE WHEN ACC.ACCOUNT_TYPE IN (53) THEN ACC.CAPITAL_BALANCE + ACC.INTEREST_BALANCE ELSE 0 END) > 0) apl on tcm.POLICY_ID=apl.POLICY_ID
),
	addr_list AS
  (SELECT tpa.party_id,
          tpa.contact_type,
          addr.*
   FROM mv_address addr,
                   mv_party_addr tpa
   WHERE addr.address_id = tpa.address_id
     AND tpa.party_addr_id IN
       (SELECT max(party_addr_id) AS party_addr_id
        FROM mv_party_addr list
        WHERE tpa.contact_type = list.contact_type
          AND tpa.party_id = list.party_id
        GROUP BY party_id,
                 contact_type)),
     agent_level AS
  ( SELECT tcm.policy_id,
           agent.agent_code AS service_agent_code,
           cagent.first_name AS service_agent_name,
           cagent.last_name AS service_agent_last_name,
           cagent.home_tel AS service_agent_home_tel,
           cagent.office_tel AS service_agent_office_tel,
           cagent.mobile AS service_agent_mobile,

     (SELECT agent_code
      FROM mv_agent
      WHERE agent_id = c_level.al) AS AL_serviceagent_code,

     (SELECT first_name ||' '|| last_name
      FROM mv_customer
      WHERE customer_id = c_level.al) AS AL_serviceAgent_name,

     (SELECT agent_code
      FROM mv_agent
      WHERE agent_id = c_level.avp) AS avp_serviceagent_code,

     (SELECT first_name ||' '|| last_name
      FROM mv_customer
      WHERE customer_id = c_level.avp) AS AVP_serviceAgent_name,

     (SELECT agent_code
      FROM mv_agent
      WHERE agent_id = c_level.gm) AS gm_serviceagent_code,

     (SELECT first_name ||' '|| last_name
      FROM mv_customer
      WHERE customer_id = c_level.gm) AS gm_serviceagent_name,
           iss_agent.agent_code AS issue_agent_code,
           iss_cagent.first_name AS issue_agent_name,
           iss_cagent.last_name AS issue_agent_last_name,
           iss_cagent.home_tel AS issue_agent_home_tel,
           iss_cagent.office_tel AS issue_agent_office_tel,
           iss_cagent.mobile AS issue_agent_mobile,

     (SELECT agent_code
      FROM mv_agent
      WHERE agent_id = c_level_i.al) AS AL_issueagent_code,

     (SELECT first_name ||' '|| last_name
      FROM mv_customer
      WHERE customer_id = c_level_i.al) AS AL_issueAgent_name,

     (SELECT agent_code
      FROM mv_agent
      WHERE agent_id = c_level_i.avp) AS avp_issueagent_code,

     (SELECT first_name ||' '|| last_name
      FROM mv_customer
      WHERE customer_id = c_level_i.avp) AS AVP_issueAgent_name,

     (SELECT agent_code
      FROM mv_agent
      WHERE agent_id = c_level_i.gm) AS gm_issueagent_code,

     (SELECT first_name ||' '|| last_name
      FROM mv_customer
      WHERE customer_id = c_level_i.gm) AS gm_issueagent_name
   FROM mv_contract_master tcm
   INNER JOIN w_agent agent ON agent.agent_id = tcm.service_agent
   INNER JOIN mv_customer cagent ON cagent.customer_id = agent.agent_id
   LEFT JOIN mv_contract_product tcp ON tcm.policy_id = tcp.policy_id
   AND tcp.master_id IS NULL
   LEFT JOIN mv_agent iss_agent ON iss_agent.agent_id = tcp.issue_agent
   LEFT JOIN mv_customer iss_cagent ON iss_cagent.customer_id = iss_agent.agent_id
   LEFT JOIN
     (SELECT policy_id,
             GET_CHANNEL_AGENT_HIERARCHY (a.service_agent, 3) AS AL,
             GET_CHANNEL_AGENT_HIERARCHY (a.service_agent, 2) AS AVP,
             GET_CHANNEL_AGENT_HIERARCHY (a.service_agent, 1) AS GM
      FROM mv_contract_master a) c_level ON c_level.policy_id = tcm.policy_id
   LEFT JOIN
     (SELECT policy_id,
             GET_CHANNEL_AGENT_HIERARCHY (a.issue_agent, 3) AS AL,
             GET_CHANNEL_AGENT_HIERARCHY (a.issue_agent, 2) AS AVP,
             GET_CHANNEL_AGENT_HIERARCHY (a.issue_agent, 1) AS GM
      FROM mv_contract_product a
      WHERE a.master_id IS NULL) c_level_i ON c_level_i.policy_id = tcm.policy_id)

SELECT tcm.policy_id,
	tcm.validate_date,
       tcm.policy_code,
       tcm.TAX_CERITIFICATE_INDI,
       tcm.suspend AS frozen,
		 w_policy_status.status_name as policy_status,

  (SELECT cause_name
   FROM t_end_cause
   WHERE cause_id = tcm.end_cause) AS end_cause,
       title_ins.title_desc || ' ' || ins.first_name||' '|| ins.last_name AS insured_name,
       replace(ins.certi_code,substr(ins.certi_code,length(ins.certi_code)-3,4),'XXXX') AS insured_id,
		tbi.entry_age AS insured_entry_age,
		ins.gender AS insured_gender,
		ins.birthday AS insured_birthday,
		joiner.joiner_name,
		joiner.joiner_id,
		joiner.joiner_entry_age,
		joiner.joiner_birthday,
		addr_ins.POST_CODE AS ins_addrcontact_NO,
		addr_ins.ADDRESS_7 AS ins_addrcontact_MOOBAN_build,
		addr_ins.MOO AS ins_addrcontact_MOO,
		addr_ins.ADDRESS_6 AS ins_addrcontact_SOI,
		addr_ins.ADDRESS_1 AS ins_addrcontact_ROAD,
		addr_ins.ADDRESS_2 AS ins_addrcontact_SUB,
		addr_ins.ADDRESS_3 AS ins_addrcontact_DISTRICT,
		addr_ins.ADDRESS_4 AS ins_addrcontact_PROVINCE,
		addr_ins.ZIP_CODE AS ins_addrcontact_ZIPCODE,
		ins.email as email,
		life.product_name as product_name,
		tcp.amount as sum_insured,
		tcm.liability_state_date as policy_status_date,
		tcm.validate_date as validate_date,
		tcm.expiry_date as expiry_date,
       ins.home_tel AS insured_home_tel,
       ins.office_tel AS insured_office_tel,
       ins.mobile AS insured_mobile,
       CASE
           WHEN cpay.customer_id IS NOT NULL THEN title_pay.title_desc || ' ' || cpay.first_name||' '|| cpay.last_name
           ELSE com_pay.company_name
       END payer_name,
		 GET_MV_ADDR(addr_pay.address_id) as payer_addr,
       addr_pay.POST_CODE AS billing_addr_NO,
       addr_pay.ADDRESS_7 AS billing_addr_MOOBAN_build,
       addr_pay.MOO AS billing_addr_MOO,
       addr_pay.ADDRESS_6 AS billing_addr_SOI,
       addr_pay.ADDRESS_1 AS billing_addr_ROAD,
       addr_pay.ADDRESS_2 AS billing_addr_SUB,
       addr_pay.ADDRESS_3 AS billing_addr_DISTRICT,
       addr_pay.ADDRESS_4 AS billing_addr_PROVINCE,
       addr_pay.ZIP_CODE AS billing_addr_ZIPCODE,
       cpay.home_tel AS insured_home_tel,
       cpay.office_tel AS insured_office_tel,
       cpay.mobile AS insured_mobile,
		 cpay.email as payer_email,
       agent_level.service_agent_code,
       agent_level.service_agent_name,
       -- agent_level.service_agent_last_name,
       agent_level.service_agent_home_tel,
       agent_level.service_agent_office_tel,
       agent_level.service_agent_mobile,
       agent_level.AVP_serviceAgent_code,
       agent_level.AVP_serviceAgent_name,
       agent_level.GM_serviceAgent_code,
       agent_level.GM_serviceAgent_name,
       agent_level.AL_serviceagent_code,
       agent_level.AL_serviceAgent_name ,
       agent_level.issue_agent_code,
       agent_level.issue_agent_name,
       agent_level.issue_agent_last_name,
       agent_level.issue_agent_home_tel,
       agent_level.issue_agent_office_tel,
       agent_level.issue_agent_mobile,
       agent_level.AVP_issueAgent_code,
       agent_level.AVP_issueAgent_name,
       agent_level.GM_issueAgent_code,
       agent_level.GM_issueAgent_name,
       agent_level.AL_issueagent_code,
       agent_level.AL_issueAgent_name ,

  (SELECT mode_name
   FROM w_pay_mode
   WHERE tcp.pay_next = mode_id) AS payment_method,

  (SELECT charge_name
   FROM w_charge_mode
   WHERE tcp.renewal_type = charge_type) AS frequency_payment,
       CASE
           WHEN
                  (SELECT sum(prem.next_total_prem_af)
                   FROM mv_contract_product prem,
                                            mv_contract_extend prem_sts
                   WHERE prem.policy_id = tcm.policy_id
                     AND prem.liability_state = 1
                     AND prem.item_id = prem_sts.item_id
                     AND prem.policy_id = prem_sts.policy_id
                     AND prem_sts.prem_status = 1) IS NULL THEN
                  (SELECT sum(prem.next_total_prem_af)
                   FROM mv_contract_product prem
                   WHERE prem.policy_id = tcm.policy_id)
           ELSE
                  (SELECT sum(prem.next_total_prem_af)
                   FROM mv_contract_product prem,
                                            mv_contract_extend prem_sts
                   WHERE prem.policy_id = tcm.policy_id
                     AND prem.liability_state = 1
                     AND prem.item_id = prem_sts.item_id
                     AND prem.policy_id = prem_sts.policy_id
                     AND prem_sts.prem_status IN (1,2))
       END total_premium
FROM mv_contract_master tcm
INNER JOIN mv_contract_product tcp ON tcp.policy_id = tcm.policy_id AND tcp.master_id IS NULL
INNER JOIN mv_contract_proposal pros ON pros.policy_id = tcm.policy_id
INNER JOIN mv_insured_list til ON tcm.policy_id = til.policy_id
INNER JOIN mv_benefit_insured tbi ON tcm.policy_id = tbi.policy_id AND tbi.insured_id = til.list_id AND tbi.item_id = tcp.item_id
INNER JOIN mv_customer ins ON til.party_id = ins.customer_id
LEFT JOIN t_title title_ins ON title_ins.title_code = ins.honor_title
INNER JOIN t_product_life life ON tcp.product_id = life.product_id
AND life.ins_type = 1
INNER JOIN mv_address addr_ins ON addr_ins.address_id = til.address_id
-- LEFT JOIN addr_list addr_home ON til.party_id = addr_home.party_id AND addr_home.contact_type = 1
-- LEFT JOIN addr_list addr_office ON til.party_id = addr_office.party_id AND addr_office.contact_type = 2
-- LEFT JOIN addr_list addr_current ON til.party_id = addr_current.party_id AND addr_current.contact_type = 3
INNER JOIN mv_payer payer ON payer.policy_id = tcm.policy_id
LEFT JOIN mv_customer cpay ON payer.party_id = cpay.customer_id
LEFT JOIN t_title title_pay ON title_pay.title_code = cpay.honor_title
LEFT JOIN mv_company_customer com_pay ON payer.party_id = com_pay.company_id
LEFT JOIN mv_address addr_pay ON addr_pay.address_id = payer.address_id
LEFT JOIN agent_level ON agent_level.policy_id = tcm.policy_id
LEFT JOIN w_policy_status ON tcm.policy_id=w_policy_status.policy_id
LEFT JOIN
  (SELECT tcm.policy_id,
          title_ins.title_desc || ' ' || ins.first_name||' '|| ins.last_name AS joiner_name,
          replace(ins.certi_code,substr(ins.certi_code,length(ins.certi_code)-3,4),'XXXX') AS joiner_id,
          entry_age AS joiner_entry_age,
          birthday AS joiner_birthday
   FROM mv_contract_master tcm
   INNER JOIN mv_contract_product tcp ON tcp.policy_id = tcm.policy_id
   AND tcp.master_id IS NULL
   INNER JOIN mv_insured_list til ON tcm.policy_id = til.policy_id
   INNER JOIN mv_benefit_insured tbi ON tcm.policy_id = tbi.policy_id
   AND tbi.insured_id = til.list_id
   AND tbi.item_id = tcp.item_id
   INNER JOIN mv_customer ins ON til.party_id = ins.customer_id
   LEFT JOIN t_title title_ins ON title_ins.title_code = ins.honor_title
   INNER JOIN t_product_life life ON tcp.product_id = life.product_id
   AND life.ins_type = 1
   WHERE ally = 1
     AND (til.relation_to_ph IN ('6','7')
          OR tbi.entry_age < 15)) joiner ON joiner.policy_id = tcm.policy_id
WHERE NOT (ally = 1
           AND (til.relation_to_ph IN ('6',
                                       '7')
                AND tbi.entry_age < 15)) AND tcm.policy_code=:policy_code