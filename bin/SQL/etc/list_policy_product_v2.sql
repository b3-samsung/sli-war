with w_product_list as (
select distinct 
    tcm.policy_id
    , tcm.policy_code
    , tcp.product_id
    , tpl.product_name
from mv_contract_master tcm
    inner join mv_contract_product tcp on tcp.policy_id = tcm.policy_id
    inner join t_product_life tpl on tcp.product_id = tpl.product_id
order by product_id desc),
w_premium as (
select tcm.policy_id
    ,tcp.product_id
    ,sum(tcp.amount) as c_sum_assured
    ,sum(tcp.total_prem_af) as c_total_premium
from mv_contract_master tcm
    inner join mv_contract_product tcp on tcp.policy_id = tcm.policy_id
group by 
    tcm.policy_id, tcp.product_id
order by tcp.product_id desc
)

select
    wpl.*
    , prm.c_sum_assured
    , prm.c_total_premium
from w_product_list wpl
    inner join w_premium prm on prm.policy_id=wpl.policy_id and prm.product_id=wpl.product_id 
where 
    policy_code = :policy_code
    -- policy_code = '012130009823'
