WITH
w_agent_ids as (
SELECT agent_id
FROM 
    mv_agent agent
    INNER JOIN (
        SELECT CHANNEL_ID
        FROM mv_CHANNEL_ORG 
        START WITH LEADER_ID=:login_agent_id
        CONNECT BY PRIOR CHANNEL_ID=PARENT_ID) ch ON channel_org_id=channel_id
UNION
SELECT agent_id
FROM
	mv_agent agent
WHERE
	agent_id=:login_agent_id
),
w_policy_status as (
SELECT 
    tcm.policy_id as policy_id
     , (CASE 
--            WHEN tcm.suspend='Y' AND tcm.suspend_cause<>'99' THEN 'อยู่ระหว่างดำเนินการ กรุณาติดต่อทางบริษัท'
            WHEN tcm.liability_state=3 THEN
                CASE WHEN tcp.end_cause = 51 THEN 'สิ้นผลบังคับ/ขอยกเลิกกรมธรรม์ (Freelook)'
                    WHEN tcp.end_cause = 31 THEN 'สิ้นผลบังคับ/เวนคืนกรมธรรม์'
                    WHEN tcp.end_cause = 12 THEN 'สิ้นสุดความคุ้มครอง'
                    WHEN tcp.end_cause IS NULL THEN 'สิ้นผลบังคับ'
                    WHEN tcp.end_cause = 21 THEN 'สิ้นผลบังคับ'
                    WHEN tcp.end_cause = 22 OR tcp.end_cause = 23 THEN 'สิ้นผลบังคับ'
                    WHEN tcp.end_cause = 34 THEN 'ยกเลิกกรมธรรม์'
                    WHEN tcp.end_cause = 35 THEN 'ยกเลิกกรมธรรม์'
                    ELSE ' สิ้นผลบังคับ (' || end_cause.cause_name || ')'
                END
            WHEN tcm.liability_state=2 THEN 'ขาดอายุ'
            WHEN tcm.liability_state=1 THEN
                CASE WHEN tce.prem_status is null THEN 'มีผลบังคับ'
                    WHEN tce.prem_status = 11 THEN 'มีผลบังคับ/ขยายระยะเวลาอัตโนมัติ'
                    WHEN tce.prem_status = 8 THEN 'มีผลบังคับ/ขยายระยะเวลา'
                    WHEN tce.prem_status = 6 THEN 'มีผลบังคับ/มูลค่าใช้เงินสำเร็จอัตโนมัติ'
                    WHEN tce.prem_status = 3 THEN 'มีผลบังคับ/มูลค่าใช้เงินสำเร็จ'
                     WHEN apl.sum_apl > 0 THEN 'มีผลบังคับ/กู้อัตโนมัติชำระเบี้ย'
                    WHEN tce.prem_status = 1 OR tce.prem_status = 2 or tce.prem_status = 4 THEN 'มีผลบังคับ'
                    ELSE ' มีผลบังคับ (' || prem_sts.status_name || ')'
                END
            WHEN tcm.liability_state=0 THEN
					CASE 
						WHEN pros.proposal_status=32 THEN 'อยู่ระหว่างการพิจารณา'
						WHEN pros.proposal_status=80 THEN 'อยู่ระหว่างการพิจารณา (Accepted)'
						WHEN pros.proposal_status=81 THEN 'อยู่ระหว่างการพิจารณา (Conditionally Accepted)'
						ELSE ' อยู่ระหว่างการพิจารณา (' || pro_sts.status_desc || ')'
					END
            ELSE 'ไม่ระบุ'
        END) as status_name
FROM mv_contract_master tcm 
    INNER JOIN mv_contract_product tcp ON tcm.policy_id=tcp.policy_id AND tcp.master_id IS NULL
    LEFT JOIN mv_agent agent ON tcm.service_agent = agent.agent_id --and agent.agent_id=:agent_id
    LEFT JOIN mv_contract_extend tce ON tce.item_id = tcp.item_id AND tce.policy_id = tcp.policy_id
    LEFT JOIN mv_contract_proposal pros ON pros.policy_id = tcm.policy_id
    LEFT JOIN t_proposal_status pro_sts ON pro_sts.proposal_status = pros.proposal_status
	LEFT JOIN t_prem_status prem_sts ON prem_sts.status_id = tce.prem_status
	LEFT JOIN t_end_cause end_cause ON end_cause.cause_id = tcp.end_cause
    LEFT JOIN (SELECT ACC.POLICY_ID,MAS.POLICY_CODE,
        SUM (CASE WHEN ACC.ACCOUNT_TYPE IN (53) THEN ACC.CAPITAL_BALANCE + ACC.INTEREST_BALANCE ELSE 0 END) AS SUM_APL
        FROM MV_POLICY_ACCOUNT ACC, MV_CONTRACT_MASTER MAS
        WHERE ACC.POLICY_ID = MAS.POLICY_ID AND MAS.SUSPEND <> 'Y'
        GROUP BY ACC.POLICY_ID, MAS.POLICY_CODE
        having SUM (CASE WHEN ACC.ACCOUNT_TYPE IN (53) THEN ACC.CAPITAL_BALANCE + ACC.INTEREST_BALANCE ELSE 0 END) > 0) apl on tcm.POLICY_ID=apl.POLICY_ID
)
, w_policy_list as (
    SELECT tcm.policy_id,
         tcm.policy_code,
		 tcm.validate_date,
         
         life.product_name AS product_name,
       title_ins.title_desc || ' ' || ins.first_name||' '|| ins.last_name AS insured_name
       , ins.certi_code as pid
       , ins.first_name AS insured_fname
       , ins.last_name AS insured_lname
        ,w_policy_status.status_name as policy_status,
         TO_CHAR(tcm.validate_date,'DD/MM/YYYY','NLS_CALENDAR=''THAI BUDDHA') as policy_status_date,
	tcm.service_agent,
         agent.agent_code as service_agent_code,
       cagent.first_name || ' ' || cagent.last_name || ' ' || agent.agent_code as service_agent_name
--    , w_policy_status.status_name as prem_status_name
    , tcm.liability_state as prem_status_name
FROM mv_contract_master tcm
    INNER JOIN mv_contract_product tcp ON tcp.policy_id = tcm.policy_id AND tcp.master_id IS NULL
    INNER JOIN t_product_life life ON tcp.product_id = life.product_id AND life.ins_type = 1
    INNER JOIN w_policy_status ON tcm.policy_id = w_policy_status.policy_id

    INNER JOIN mv_agent agent ON tcm.service_agent=agent.agent_id
    INNER JOIN mv_customer cagent ON cagent.customer_id = agent.party_id

    INNER JOIN mv_contract_proposal pros ON pros.policy_id = tcm.policy_id
    INNER JOIN mv_insured_list til ON tcm.policy_id = til.policy_id
    INNER JOIN mv_benefit_insured tbi ON tcm.policy_id = tbi.policy_id AND tbi.insured_id = til.list_id AND tbi.item_id = tcp.item_id
    INNER JOIN mv_customer ins ON til.party_id = ins.customer_id
    LEFT JOIN t_title title_ins ON title_ins.title_code = ins.honor_title
    LEFT JOIN mv_contract_extend tce ON tce.item_id = tcp.item_id AND tce.policy_id = tcp.policy_id
    LEFT JOIN t_end_cause ON tcp.end_cause = t_end_cause.cause_id
    LEFT JOIN t_prem_status ON tce.prem_status = t_prem_status.status_id

WHERE 
    NOT (ally = 1 AND (til.relation_to_ph IN ('6','7') AND tbi.entry_age < 15))
--	and tcm.service_agent in (select agent_id from w_agent_ids)
)

select * 
from w_policy_list 