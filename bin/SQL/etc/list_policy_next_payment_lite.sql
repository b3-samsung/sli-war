--SB
select aa.*, tcm.policy_code from (
SELECT policy_id,
       policy_year,
       policy_period,
       due_time,
       finish_date,
       paid_amount,
       payment_method,
       status,
       TYPE,
       bank_name,
       bank_account
FROM
  (SELECT x.*,
          ROW_NUMBER() OVER (PARTITION BY policy_id, due_time
                             ORDER BY fee_id DESC) "RN"
   FROM
     (SELECT p_sb.fee_id,
             p_sb.policy_id ,
             PKG_LS_PUB_UTILS.F_POLICY_DURATION_YEAR (sb.pay_due_date,EFFECTIVE_DATE)+ 1 AS POLICY_YEAR ,
             (CASE WHEN PKG_LS_PUB_UTILS.F_POLICY_DURATION_MONTH ( sb.pay_due_date, EFFECTIVE_DATE) > 0
              AND (TO_CHAR (EFFECTIVE_DATE, 'DD') > TO_CHAR (sb.pay_due_date, 'DD')) THEN PKG_RPT_TSLI_COMMONS.GET_INSTALMENT_NO ( RENEWAL_TYPE, PKG_LS_PUB_UTILS.F_POLICY_DURATION_MONTH ( sb.pay_due_date, LAST_DAY (EFFECTIVE_DATE))) WHEN PKG_LS_PUB_UTILS.F_POLICY_DURATION_MONTH ( sb.pay_due_date, EFFECTIVE_DATE) > 0
              AND (TO_CHAR (EFFECTIVE_DATE, 'DD') <= TO_CHAR (sb.pay_due_date, 'DD')) THEN PKG_RPT_TSLI_COMMONS.GET_INSTALMENT_NO ( RENEWAL_TYPE, PKG_LS_PUB_UTILS.F_POLICY_DURATION_MONTH (sb.pay_due_date, EFFECTIVE_DATE)) WHEN PKG_LS_PUB_UTILS.F_POLICY_DURATION_MONTH ( sb.pay_due_date, EFFECTIVE_DATE) = 0
              AND sb.pay_due_date < EFFECTIVE_DATE THEN 1 ELSE 0 END) AS POLICY_PERIOD,
             CASE WHEN to_char(p_sb.c_due_time, 'YYYY') = to_char(sb.pay_due_date, 'YYYY')
      AND p_sb.c_due_time <> sb.pay_due_date THEN sb.pay_due_date ELSE p_sb.due_time END due_time,
                                                                                         p_sb.finish_date,
                                                                                         p_sb.fee_amount AS paid_amount,
                                                                                         p_sb.payment_method,
                                                                                         p_sb.status,
                                                                                         'SB' AS TYPE,
                                                                                         CASE WHEN p_sb.pay_mode = 3
      AND p_sb.bank_code IS NULL THEN
        (SELECT DISTINCT bo.bank_name
         FROM t_pay_plan_payee tpp,
              t_bank_account tba,
              t_bank_org bo
         WHERE tpp.policy_id = p_sb.policy_id
           AND tpp.payee_id = p_sb.payee_id
           AND tpp.plan_id = sb.plan_id
           AND tpp.pay_mode = p_sb.pay_mode
           AND tpp.payee_account_id = tba.account_id
           AND substr(tba.bank_code,1,3)= bo.bank_code) WHEN p_sb.pay_mode = 3
      AND p_sb.bank_code IS NOT NULL THEN
        (SELECT DISTINCT bo.bank_name
         FROM t_bank_org bo
         WHERE bo.bank_code = substr(p_sb.bank_code,1,3)) WHEN p_sb.pay_mode <> 3 THEN
        (SELECT DISTINCT bo.bank_name
         FROM t_bank_account tba,
              t_bank_org bo
         WHERE tba.account_id = p_sb.internal_account
           AND substr(tba.bank_code,1,3)= bo.bank_code) END bank_name,
                                                            CASE WHEN p_sb.pay_mode = 3
      AND p_sb.account_id IS NULL THEN
        (SELECT DISTINCT replace(tba.bank_account,substr(tba.bank_account,length(tba.bank_account)-3,4),'XXXX')
         FROM t_pay_plan_payee tpp,
              t_bank_account tba
         WHERE tpp.policy_id = p_sb.policy_id
           AND tpp.payee_id = p_sb.payee_id
           AND tpp.plan_id = sb.plan_id
           AND tpp.pay_mode = p_sb.pay_mode
           AND tpp.payee_account_id = tba.account_id) WHEN p_sb.pay_mode = 3
      AND p_sb.account_id IS NOT NULL THEN
        (SELECT replace(tba.bank_account,substr(tba.bank_account,length(tba.bank_account)-3,4),'XXXX')
         FROM t_bank_account tba
         WHERE tba.account_id = p_sb.account_id) END bank_account
      FROM
        (SELECT DISTINCT c.fee_id,
                         pa.policy_id,
                         pa.liab_id,
                         pa.item_id,
                         c.due_time AS c_due_time,
                         pa.due_time,
                         trunc(c.finish_time) AS finish_date,
                         c.fee_amount,
                         c.pay_mode,
                         c.payee_id,
                         c.internal_account,
                         c.account_id,
                         c.bank_code,
                         c.prem_purpose,
                         CASE WHEN c.pay_mode = 3 THEN 'Direct Credit' ELSE
           (SELECT pm.mode_name
            FROM t_pay_mode pm
            WHERE pm.mode_id=c.pay_mode) END payment_method,
                                             'Payment confirm' AS status
         FROM mv_cash c,
              mv_prem_arap pa,
              mv_arap_offset ao_a,
              mv_arap_offset ao_c
         WHERE c.fee_id=ao_c.fee_id
           AND ao_c.arap_table=1
           AND ao_c.offset_id=ao_a.offset_id
           AND ao_a.fee_id=pa.list_id
           AND ao_a.arap_table=4
           AND c.fee_status = '1'
           AND pa.offset_status = '1'
           AND c.check_enter_time IS NOT NULL
           AND c.fee_type = 32
           AND c.withdraw_type IN ('21',
                                   '19',
                                   '26',
                                   '27')) p_sb,
        (SELECT DISTINCT tpd.policy_id,
                         tpd.item_id,
                         tpd.liab_id,
                         tpd.pay_due_date,
                         tpd.plan_id,
                         tpd.fee_amount
         FROM t_pay_due tpd
         WHERE fee_status = 1
           AND fee_amount > 0
           AND liab_id = 301 ) sb,
        (SELECT tcm.POLICY_ID,
                tcp.RENEWAL_TYPE,
                CASE WHEN tcm.INITIAL_VALI_DATE IS NULL THEN tcm.VALIDATE_DATE WHEN tcm.INITIAL_VALI_DATE IS NOT NULL
         AND TO_CHAR (tcm.INITIAL_VALI_DATE, 'DD') = TO_CHAR (tcm.VALIDATE_DATE, 'DD') THEN tcm.INITIAL_VALI_DATE ELSE TO_DATE ( TO_CHAR (tcm.INITIAL_VALI_DATE, 'YYYY') || '-' || TO_CHAR (tcm.VALIDATE_DATE, 'MM-DD'), 'YYYY-MM-DD') END EFFECTIVE_DATE
         FROM mv_CONTRACT_MASTER tcm,
              mv_CONTRACT_PRODUCT tcp
         WHERE tcm.POLICY_ID = tcp.POLICY_ID
           AND tcp.MASTER_ID IS NULL) C
      WHERE p_sb.policy_id = sb.policy_id
        AND ((p_sb.fee_amount <= sb.fee_amount
              AND p_sb.c_due_time = sb.pay_due_date)
             OR (p_sb.fee_amount = sb.fee_amount
                 AND to_char(p_sb.c_due_time, 'YYYY') = to_char(sb.pay_due_date, 'YYYY')))
        AND p_sb.item_id = sb.item_id
        AND p_sb.POLICY_ID = C.POLICY_ID
      ORDER BY p_sb.fee_id DESC) x)
WHERE RN = 1
UNION --Annuity

SELECT policy_id,
       policy_year,
       policy_period,
       due_time,
       finish_date,
       paid_amount,
       payment_method,
       status,
       TYPE,
       bank_name,
       bank_account
FROM
  (SELECT x.*,
          ROW_NUMBER() OVER (PARTITION BY policy_id, due_time
                             ORDER BY fee_id DESC) "RN"
   FROM
     (SELECT p_ab.fee_id,
             p_ab.policy_id ,
             PKG_LS_PUB_UTILS.F_POLICY_DURATION_YEAR (ab.pay_due_date,EFFECTIVE_DATE)+ 1 AS POLICY_YEAR ,
             (CASE WHEN PKG_LS_PUB_UTILS.F_POLICY_DURATION_MONTH ( ab.pay_due_date, EFFECTIVE_DATE) > 0
              AND (TO_CHAR (EFFECTIVE_DATE, 'DD') > TO_CHAR (ab.pay_due_date, 'DD')) THEN PKG_RPT_TSLI_COMMONS.GET_INSTALMENT_NO ( RENEWAL_TYPE, PKG_LS_PUB_UTILS.F_POLICY_DURATION_MONTH ( ab.pay_due_date, LAST_DAY (EFFECTIVE_DATE))) WHEN PKG_LS_PUB_UTILS.F_POLICY_DURATION_MONTH ( ab.pay_due_date, EFFECTIVE_DATE) > 0
              AND (TO_CHAR (EFFECTIVE_DATE, 'DD') <= TO_CHAR (ab.pay_due_date, 'DD')) THEN PKG_RPT_TSLI_COMMONS.GET_INSTALMENT_NO ( RENEWAL_TYPE, PKG_LS_PUB_UTILS.F_POLICY_DURATION_MONTH (ab.pay_due_date, EFFECTIVE_DATE)) WHEN PKG_LS_PUB_UTILS.F_POLICY_DURATION_MONTH ( ab.pay_due_date, EFFECTIVE_DATE) = 0
              AND ab.pay_due_date < EFFECTIVE_DATE THEN 1 ELSE 0 END) AS POLICY_PERIOD,
             CASE WHEN to_char(p_ab.c_due_time, 'YYYY') = to_char(ab.pay_due_date, 'YYYY')
      AND p_ab.c_due_time <> ab.pay_due_date THEN ab.pay_due_date ELSE p_ab.due_time END due_time,
                                                                                         p_ab.finish_date,
                                                                                         p_ab.fee_amount AS paid_amount,
                                                                                         p_ab.payment_method,
                                                                                         p_ab.status,
                                                                                         'Annuity' AS TYPE,
                                                                                         CASE WHEN p_ab.pay_mode = 3
      AND p_ab.bank_code IS NULL THEN
        (SELECT DISTINCT bo.bank_name
         FROM t_pay_plan_payee tpp,
              t_bank_account tba,
              t_bank_org bo
         WHERE tpp.policy_id = p_ab.policy_id
           AND tpp.payee_id = p_ab.payee_id
           AND tpp.plan_id = ab.plan_id
           AND tpp.pay_mode = p_ab.pay_mode
           AND tpp.payee_account_id = tba.account_id
           AND substr(tba.bank_code,1,3)= bo.bank_code) WHEN p_ab.pay_mode = 3
      AND p_ab.bank_code IS NOT NULL THEN
        (SELECT DISTINCT bo.bank_name
         FROM t_bank_org bo
         WHERE bo.bank_code = substr(p_ab.bank_code,1,3)) WHEN p_ab.pay_mode <> 3 THEN
        (SELECT DISTINCT bo.bank_name
         FROM t_bank_account tba,
              t_bank_org bo
         WHERE tba.account_id = p_ab.internal_account
           AND substr(tba.bank_code,1,3)= bo.bank_code) END bank_name,
                                                            CASE WHEN p_ab.pay_mode = 3
      AND p_ab.account_id IS NULL THEN
        (SELECT DISTINCT replace(tba.bank_account,substr(tba.bank_account,length(tba.bank_account)-3,4),'XXXX')
         FROM t_pay_plan_payee tpp,
              t_bank_account tba
         WHERE tpp.policy_id = p_ab.policy_id
           AND tpp.payee_id = p_ab.payee_id
           AND tpp.plan_id = ab.plan_id
           AND tpp.pay_mode = p_ab.pay_mode
           AND tpp.payee_account_id = tba.account_id) WHEN p_ab.pay_mode = 3
      AND p_ab.account_id IS NOT NULL THEN
        (SELECT replace(tba.bank_account,substr(tba.bank_account,length(tba.bank_account)-3,4),'XXXX')
         FROM t_bank_account tba
         WHERE tba.account_id = p_ab.account_id) END bank_account
      FROM
        (SELECT DISTINCT c.fee_id,
                         pa.policy_id,
                         pa.liab_id,
                         pa.item_id,
                         c.due_time AS c_due_time,
                         pa.due_time,
                         trunc(c.finish_time) AS finish_date,
                         c.fee_amount,
                         c.pay_mode,
                         c.payee_id,
                         c.internal_account,
                         c.account_id,
                         c.bank_code,
                         c.prem_purpose,
                         CASE WHEN c.pay_mode = 3 THEN 'Direct Credit' ELSE
           (SELECT pm.mode_name
            FROM t_pay_mode pm
            WHERE pm.mode_id=c.pay_mode) END payment_method,
                                             'Payment confirm' AS status
         FROM mv_cash c,
              mv_prem_arap pa,
              mv_arap_offset ao_a,
              mv_arap_offset ao_c
         WHERE c.fee_id=ao_c.fee_id
           AND ao_c.arap_table=1
           AND ao_c.offset_id=ao_a.offset_id
           AND ao_a.fee_id=pa.list_id
           AND ao_a.arap_table=4
           AND c.fee_status = '1'
           AND pa.offset_status = '1'
           AND c.check_enter_time IS NOT NULL
           AND c.fee_type = 32
           AND c.withdraw_type IN ('21',
                                   '19',
                                   '26',
                                   '27')) p_ab,
        (SELECT DISTINCT tpd.policy_id,
                         tpd.item_id,
                         tpd.liab_id,
                         tpd.pay_due_date,
                         tpd.plan_id,
                         tpd.fee_amount
         FROM t_pay_due tpd
         WHERE fee_status = 1
           AND fee_amount > 0
           AND liab_id = 302) ab,
        (SELECT tcm.POLICY_ID,
                tcp.RENEWAL_TYPE,
                CASE WHEN tcm.INITIAL_VALI_DATE IS NULL THEN tcm.VALIDATE_DATE WHEN tcm.INITIAL_VALI_DATE IS NOT NULL
         AND TO_CHAR (tcm.INITIAL_VALI_DATE, 'DD') = TO_CHAR (tcm.VALIDATE_DATE, 'DD') THEN tcm.INITIAL_VALI_DATE ELSE TO_DATE ( TO_CHAR (tcm.INITIAL_VALI_DATE, 'YYYY') || '-' || TO_CHAR (tcm.VALIDATE_DATE, 'MM-DD'), 'YYYY-MM-DD') END EFFECTIVE_DATE
         FROM mv_CONTRACT_MASTER tcm,
              mv_CONTRACT_PRODUCT tcp
         WHERE tcm.POLICY_ID = tcp.POLICY_ID
           AND tcp.MASTER_ID IS NULL) C
      WHERE p_ab.policy_id = ab.policy_id
        AND ((p_ab.fee_amount <= ab.fee_amount
              AND p_ab.c_due_time = ab.pay_due_date)
             OR (p_ab.fee_amount = ab.fee_amount
                 AND to_char(p_ab.c_due_time, 'YYYY') = to_char(ab.pay_due_date, 'YYYY')))
        AND p_ab.item_id = ab.item_id
        AND p_ab.POLICY_ID = C.POLICY_ID
      ORDER BY p_ab.fee_id DESC) x)
WHERE RN = 1
UNION --MB

SELECT policy_id,
       policy_year,
       policy_period,
       due_time,
       finish_date,
       paid_amount,
       payment_method,
       status,
       TYPE,
       bank_name,
       bank_account
FROM
  (SELECT x.*,
          ROW_NUMBER() OVER (PARTITION BY policy_id, due_time
                             ORDER BY fee_id DESC) "RN"
   FROM
     (SELECT p_mb.fee_id,
             p_mb.policy_id ,
             PKG_LS_PUB_UTILS.F_POLICY_DURATION_YEAR (mb.pay_due_date,EFFECTIVE_DATE)+ 1 AS POLICY_YEAR ,
             (CASE WHEN PKG_LS_PUB_UTILS.F_POLICY_DURATION_MONTH ( mb.pay_due_date, EFFECTIVE_DATE) > 0
              AND (TO_CHAR (EFFECTIVE_DATE, 'DD') > TO_CHAR (mb.pay_due_date, 'DD')) THEN PKG_RPT_TSLI_COMMONS.GET_INSTALMENT_NO ( RENEWAL_TYPE, PKG_LS_PUB_UTILS.F_POLICY_DURATION_MONTH ( mb.pay_due_date, LAST_DAY (EFFECTIVE_DATE))) WHEN PKG_LS_PUB_UTILS.F_POLICY_DURATION_MONTH ( mb.pay_due_date, EFFECTIVE_DATE) > 0
              AND (TO_CHAR (EFFECTIVE_DATE, 'DD') <= TO_CHAR (mb.pay_due_date, 'DD')) THEN PKG_RPT_TSLI_COMMONS.GET_INSTALMENT_NO ( RENEWAL_TYPE, PKG_LS_PUB_UTILS.F_POLICY_DURATION_MONTH (mb.pay_due_date, EFFECTIVE_DATE)) WHEN PKG_LS_PUB_UTILS.F_POLICY_DURATION_MONTH ( mb.pay_due_date, EFFECTIVE_DATE) = 0
              AND mb.pay_due_date < EFFECTIVE_DATE THEN 1 ELSE 0 END) AS POLICY_PERIOD,
             CASE WHEN to_char(p_mb.c_due_time, 'YYYY') = to_char(mb.pay_due_date, 'YYYY')
      AND p_mb.c_due_time <> mb.pay_due_date THEN mb.pay_due_date ELSE p_mb.due_time END due_time,
                                                                                         p_mb.finish_date,
                                                                                         p_mb.fee_amount AS paid_amount,
                                                                                         p_mb.payment_method,
                                                                                         p_mb.status,
                                                                                         'MB' AS TYPE,
                                                                                         CASE WHEN p_mb.pay_mode = 3
      AND p_mb.bank_code IS NULL THEN
        (SELECT DISTINCT bo.bank_name
         FROM t_pay_plan_payee tpp,
              t_bank_account tba,
              t_bank_org bo
         WHERE tpp.policy_id = p_mb.policy_id
           AND tpp.payee_id = p_mb.payee_id
           AND tpp.plan_id = mb.plan_id
           AND tpp.pay_mode = p_mb.pay_mode
           AND tpp.payee_account_id = tba.account_id
           AND substr(tba.bank_code,1,3)= bo.bank_code) WHEN p_mb.pay_mode = 3
      AND p_mb.bank_code IS NOT NULL THEN
        (SELECT DISTINCT bo.bank_name
         FROM t_bank_org bo
         WHERE bo.bank_code = substr(p_mb.bank_code,1,3)) WHEN p_mb.pay_mode <> 3 THEN
        (SELECT DISTINCT bo.bank_name
         FROM t_bank_account tba,
              t_bank_org bo
         WHERE tba.account_id = p_mb.internal_account
           AND substr(tba.bank_code,1,3)= bo.bank_code) END bank_name,
                                                            CASE WHEN p_mb.pay_mode = 3
      AND p_mb.account_id IS NULL THEN
        (SELECT DISTINCT replace(tba.bank_account,substr(tba.bank_account,length(tba.bank_account)-3,4),'XXXX')
         FROM t_pay_plan_payee tpp,
              t_bank_account tba
         WHERE tpp.policy_id = p_mb.policy_id
           AND tpp.payee_id = p_mb.payee_id
           AND tpp.plan_id = mb.plan_id
           AND tpp.pay_mode = p_mb.pay_mode
           AND tpp.payee_account_id = tba.account_id) WHEN p_mb.pay_mode = 3
      AND p_mb.account_id IS NOT NULL THEN
        (SELECT replace(tba.bank_account,substr(tba.bank_account,length(tba.bank_account)-3,4),'XXXX')
         FROM t_bank_account tba
         WHERE tba.account_id = p_mb.account_id) END bank_account
      FROM
        (SELECT DISTINCT c.fee_id,
                         pa.policy_id,
                         pa.liab_id,
                         pa.item_id,
                         c.due_time AS c_due_time,
                         pa.due_time,
                         trunc(c.finish_time) AS finish_date,
                         c.fee_amount,
                         c.pay_mode,
                         c.payee_id,
                         c.internal_account,
                         c.account_id,
                         c.bank_code,
                         c.prem_purpose,
                         CASE WHEN c.pay_mode = 3 THEN 'Direct Credit' ELSE
           (SELECT pm.mode_name
            FROM t_pay_mode pm
            WHERE pm.mode_id=c.pay_mode) END payment_method,
                                             'Payment confirm' AS status
         FROM mv_cash c,
              mv_prem_arap pa,
              mv_arap_offset ao_a,
              mv_arap_offset ao_c
         WHERE c.fee_id=ao_c.fee_id
           AND ao_c.arap_table=1
           AND ao_c.offset_id=ao_a.offset_id
           AND ao_a.fee_id=pa.list_id
           AND ao_a.arap_table=4
           AND c.fee_status = '1'
           AND pa.offset_status = '1'
           AND c.check_enter_time IS NOT NULL
           AND c.fee_type = 32
           AND c.withdraw_type IN ('21',
                                   '19',
                                   '26',
                                   '27')) p_mb,
        (SELECT DISTINCT tpd.policy_id,
                         tpd.item_id,
                         tpd.liab_id,
                         tpd.pay_due_date,
                         tpd.plan_id,
                         tpd.fee_amount
         FROM t_pay_due tpd
         WHERE fee_status = 1
           AND fee_amount > 0
           AND liab_id = 303 ) mb,
        (SELECT tcm.POLICY_ID,
                tcp.RENEWAL_TYPE,
                CASE WHEN tcm.INITIAL_VALI_DATE IS NULL THEN tcm.VALIDATE_DATE WHEN tcm.INITIAL_VALI_DATE IS NOT NULL
         AND TO_CHAR (tcm.INITIAL_VALI_DATE, 'DD') = TO_CHAR (tcm.VALIDATE_DATE, 'DD') THEN tcm.INITIAL_VALI_DATE ELSE TO_DATE ( TO_CHAR (tcm.INITIAL_VALI_DATE, 'YYYY') || '-' || TO_CHAR (tcm.VALIDATE_DATE, 'MM-DD'), 'YYYY-MM-DD') END EFFECTIVE_DATE
         FROM mv_CONTRACT_MASTER tcm,
              mv_CONTRACT_PRODUCT tcp
         WHERE tcm.POLICY_ID = tcp.POLICY_ID
           AND tcp.MASTER_ID IS NULL) C
      WHERE p_mb.policy_id = mb.policy_id
        AND p_mb.c_due_time >= mb.pay_due_date
        AND p_mb.liab_id = mb.liab_id
        AND p_mb.item_id = mb.item_id
        AND p_mb.POLICY_ID = C.POLICY_ID
      ORDER BY p_mb.fee_id DESC) x)
WHERE RN = 1 --CB

UNION
SELECT policy_id,
       policy_year,
       policy_period,
       due_time,
       finish_date,
       paid_amount,
       payment_method,
       status,
       TYPE,
       bank_name,
       bank_account
FROM
  (SELECT x.*,
          ROW_NUMBER() OVER (PARTITION BY policy_id, due_time
                             ORDER BY fee_id DESC) "RN"
   FROM
     (SELECT p_cb.fee_id,
             p_cb.policy_id ,
             PKG_LS_PUB_UTILS.F_POLICY_DURATION_YEAR (cb.allocate_date,EFFECTIVE_DATE)+ 1 AS POLICY_YEAR ,
             (CASE WHEN PKG_LS_PUB_UTILS.F_POLICY_DURATION_MONTH ( cb.allocate_date, EFFECTIVE_DATE) > 0
              AND (TO_CHAR (EFFECTIVE_DATE, 'DD') > TO_CHAR (cb.allocate_date, 'DD')) THEN PKG_RPT_TSLI_COMMONS.GET_INSTALMENT_NO ( RENEWAL_TYPE, PKG_LS_PUB_UTILS.F_POLICY_DURATION_MONTH ( cb.allocate_date, LAST_DAY (EFFECTIVE_DATE))) WHEN PKG_LS_PUB_UTILS.F_POLICY_DURATION_MONTH ( cb.allocate_date, EFFECTIVE_DATE) > 0
              AND (TO_CHAR (EFFECTIVE_DATE, 'DD') <= TO_CHAR (cb.allocate_date, 'DD')) THEN PKG_RPT_TSLI_COMMONS.GET_INSTALMENT_NO ( RENEWAL_TYPE, PKG_LS_PUB_UTILS.F_POLICY_DURATION_MONTH (cb.allocate_date, EFFECTIVE_DATE)) WHEN PKG_LS_PUB_UTILS.F_POLICY_DURATION_MONTH ( cb.allocate_date, EFFECTIVE_DATE) = 0
              AND cb.allocate_date < EFFECTIVE_DATE THEN 1 ELSE 0 END) AS POLICY_PERIOD,
             CASE WHEN to_char(p_cb.c_due_time, 'YYYY') = to_char(cb.allocate_date, 'YYYY')
      AND p_cb.c_due_time <> cb.allocate_date THEN cb.allocate_date ELSE p_cb.due_time END due_time,
                                                                                           p_cb.finish_date,
                                                                                           p_cb.fee_amount AS paid_amount,
                                                                                           p_cb.payment_method,
                                                                                           p_cb.status,
                                                                                           'CB' AS TYPE,
                                                                                           CASE WHEN p_cb.pay_mode = 3
      AND p_cb.bank_code IS NULL THEN
        (SELECT DISTINCT bo.bank_name
         FROM t_pay_plan_payee tpp,
              t_bank_account tba,
              t_bank_org bo
         WHERE tpp.policy_id = p_cb.policy_id
           AND tpp.payee_id = p_cb.payee_id
           AND tpp.plan_id = cb.plan_id
           AND tpp.pay_mode = p_cb.pay_mode
           AND tpp.payee_account_id = tba.account_id
           AND substr(tba.bank_code,1,3)= bo.bank_code) WHEN p_cb.pay_mode = 3
      AND p_cb.bank_code IS NOT NULL THEN
        (SELECT DISTINCT bo.bank_name
         FROM t_bank_org bo
         WHERE bo.bank_code = substr(p_cb.bank_code,1,3)) WHEN p_cb.pay_mode <> 3 THEN
        (SELECT DISTINCT bo.bank_name
         FROM t_bank_account tba,
              t_bank_org bo
         WHERE tba.account_id = p_cb.internal_account
           AND substr(tba.bank_code,1,3)= bo.bank_code) END bank_name,
                                                            CASE WHEN p_cb.pay_mode = 3
      AND p_cb.account_id IS NULL THEN
        (SELECT DISTINCT replace(tba.bank_account,substr(tba.bank_account,length(tba.bank_account)-3,4),'XXXX')
         FROM t_pay_plan_payee tpp,
              t_bank_account tba
         WHERE tpp.policy_id = p_cb.policy_id
           AND tpp.payee_id = p_cb.payee_id
           AND tpp.plan_id = cb.plan_id
           AND tpp.pay_mode = p_cb.pay_mode
           AND tpp.payee_account_id = tba.account_id) WHEN p_cb.pay_mode = 3
      AND p_cb.account_id IS NOT NULL THEN
        (SELECT replace(tba.bank_account,substr(tba.bank_account,length(tba.bank_account)-3,4),'XXXX')
         FROM t_bank_account tba
         WHERE tba.account_id = p_cb.account_id) END bank_account
      FROM
        (SELECT DISTINCT c.fee_id,
                         pa.policy_id,
                         pa.liab_id,
                         pa.item_id,
                         c.due_time AS c_due_time,
                         pa.due_time,
                         trunc(c.finish_time) AS finish_date,
                         c.fee_amount,
                         c.pay_mode,
                         c.payee_id,
                         c.internal_account,
                         c.account_id,
                         c.bank_code,
                         c.prem_purpose,
                         pa.policy_chg_id,
                         pa.change_id,
                         CASE WHEN c.pay_mode = 3 THEN 'Direct Credit' ELSE
           (SELECT pm.mode_name
            FROM t_pay_mode pm
            WHERE pm.mode_id=c.pay_mode) END payment_method,
                                             'Payment confirm' AS status
         FROM mv_cash c,
              mv_prem_arap pa,
              mv_arap_offset ao_a,
              mv_arap_offset ao_c
         WHERE c.fee_id=ao_c.fee_id
           AND ao_c.arap_table=1
           AND ao_c.offset_id=ao_a.offset_id
           AND ao_a.fee_id=pa.list_id
           AND ao_a.arap_table=4
           AND c.fee_status = '1'
           AND pa.offset_status = '1'
           AND c.check_enter_time IS NOT NULL
           AND c.fee_type = 32
           AND c.withdraw_type IN ('21','19','26','27')) p_cb,
        (SELECT a.policy_id,
                a.item_id,
                a.change_id,
                a.policy_chg_id,
                a.allocate_date,
                b.plan_id,
                b.liab_id
         FROM t_bonus_allocate a,
              t_pay_plan b
         WHERE a.policy_id = b.policy_id
           AND a.allocate_type = 14
           AND liab_id IS NULL
           AND a.item_id = b.item_id
           AND fee_amount > 0) cb,
        (SELECT tcm.POLICY_ID,
                tcp.RENEWAL_TYPE,
                CASE WHEN tcm.INITIAL_VALI_DATE IS NULL THEN tcm.VALIDATE_DATE WHEN tcm.INITIAL_VALI_DATE IS NOT NULL
         AND TO_CHAR (tcm.INITIAL_VALI_DATE, 'DD') = TO_CHAR (tcm.VALIDATE_DATE, 'DD') THEN tcm.INITIAL_VALI_DATE ELSE TO_DATE ( TO_CHAR (tcm.INITIAL_VALI_DATE, 'YYYY') || '-' || TO_CHAR (tcm.VALIDATE_DATE, 'MM-DD'), 'YYYY-MM-DD') END EFFECTIVE_DATE
         FROM mv_CONTRACT_MASTER tcm,
              mv_CONTRACT_PRODUCT tcp
         WHERE tcm.POLICY_ID = tcp.POLICY_ID
           AND tcp.MASTER_ID IS NULL) C
      WHERE p_cb.policy_id = cb.policy_id
        AND p_cb.c_due_time = cb.allocate_date
        AND p_cb.item_id = cb.item_id
        AND p_cb.change_id = cb.change_id
        AND p_cb.policy_chg_id = cb.policy_chg_id
        AND p_cb.POLICY_ID = C.POLICY_ID
      ORDER BY p_cb.fee_id DESC) x)
WHERE RN = 1 -- Next due of pay plan

UNION
SELECT a.*
FROM
  (SELECT tpp.policy_id,
          PKG_LS_PUB_UTILS.F_POLICY_DURATION_YEAR (tpp.pay_due_date,EFFECTIVE_DATE)+ 1 AS POLICY_YEAR ,
          (CASE WHEN PKG_LS_PUB_UTILS.F_POLICY_DURATION_MONTH ( tpp.pay_due_date, EFFECTIVE_DATE) > 0
           AND (TO_CHAR (EFFECTIVE_DATE, 'DD') > TO_CHAR (tpp.pay_due_date, 'DD')) THEN PKG_RPT_TSLI_COMMONS.GET_INSTALMENT_NO ( RENEWAL_TYPE, PKG_LS_PUB_UTILS.F_POLICY_DURATION_MONTH ( tpp.pay_due_date, LAST_DAY (EFFECTIVE_DATE))) WHEN PKG_LS_PUB_UTILS.F_POLICY_DURATION_MONTH ( tpp.pay_due_date, EFFECTIVE_DATE) > 0
           AND (TO_CHAR (EFFECTIVE_DATE, 'DD') <= TO_CHAR (tpp.pay_due_date, 'DD')) THEN PKG_RPT_TSLI_COMMONS.GET_INSTALMENT_NO ( RENEWAL_TYPE, PKG_LS_PUB_UTILS.F_POLICY_DURATION_MONTH (tpp.pay_due_date, EFFECTIVE_DATE)) WHEN PKG_LS_PUB_UTILS.F_POLICY_DURATION_MONTH ( tpp.pay_due_date, EFFECTIVE_DATE) = 0
           AND tpp.pay_due_date < EFFECTIVE_DATE THEN 1 ELSE 0 END) AS POLICY_PERIOD,
          tpp.pay_due_date AS due_time,
          NULL AS finish_date,
          NULL AS paid_amount,
          NULL AS payment_method,
          'Next due payment' AS status,
          CASE
              WHEN tpp.pay_plan_type = 1 THEN 'CB'
              WHEN tpp.pay_plan_type = 2 THEN 'Annuity'
              WHEN tpp.pay_plan_type = 3 THEN 'SB'
              ELSE 'MB'
          END TYPE,
              NULL AS bank_name,
              NULL AS bank_account
   FROM t_pay_plan tpp --left join t_bonus_allocate tba on tpp.policy_id = tba.policy_id and tpp.item_id = tba.item_id and tba.allocate_type = 14
LEFT JOIN
     (SELECT tcm.POLICY_ID,
             tcp.RENEWAL_TYPE,
             CASE WHEN tcm.INITIAL_VALI_DATE IS NULL THEN tcm.VALIDATE_DATE WHEN tcm.INITIAL_VALI_DATE IS NOT NULL
      AND TO_CHAR (tcm.INITIAL_VALI_DATE, 'DD') = TO_CHAR (tcm.VALIDATE_DATE, 'DD') THEN tcm.INITIAL_VALI_DATE ELSE TO_DATE ( TO_CHAR (tcm.INITIAL_VALI_DATE, 'YYYY') || '-' || TO_CHAR (tcm.VALIDATE_DATE, 'MM-DD'), 'YYYY-MM-DD') END EFFECTIVE_DATE
      FROM mv_CONTRACT_MASTER tcm,
           mv_CONTRACT_PRODUCT tcp
      WHERE tcm.POLICY_ID = tcp.POLICY_ID
        AND tcp.MASTER_ID IS NULL) C ON c.policy_id = tpp.policy_id
   WHERE tpp.pay_status = 1
     AND tpp.pay_due_date IS NOT NULL)a,
	t_contract_master b
WHERE due_time >= sysdate
  AND a.policy_id = b.policy_id
  AND b.liability_state = 1) aa
    inner join mv_contract_master tcm on aa.policy_id=tcm.policy_id
where tcm.policy_code = :policy_code -- 2791 012130099999
	and due_time > sysdate and rownum <= 1
order by due_time desc
--order by due_time
