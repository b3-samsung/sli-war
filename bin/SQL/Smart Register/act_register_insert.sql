INSERT INTO DH_T_ACT_REGIS (
	register_id,register_type,
	activity_id,
	id,title,name,surname,gender,age,dob,
	address1,address2,address3,address4,address5,
	zip,mobile,email,line,
	recommender_code,
	del_indi,
	attendance_result,exam_result,
	picture_local,idcard_local,
	insert_time, update_time,
	insert_by, update_by
)
VALUES(
	:register_id,:register_type,
	:activity_id,
	:id,title,:name,:surname,:gender,:age,:dob,
	:address1,:address2,:address3,:address4,:address5,
	:zip,:mobile,:email,:line,
	:recommender_code,:del_indi,
	:attendance_result,:exam_result,
	:picture_local,:idcard_local,
	:insert_time,:update_time,
	:insert_by,:update_by
)