INSERT INTO DH_T_AG_ACTIVITY (
	activity_id,activity_type,activity_name,activity_date, 
	location,province,numofdays,start_time,end_time,
	activity_regis_from,activity_regis_to,
	max_capa,room_capa,del_indi,
	insert_time,update_time,
	insert_by,update_by)
VALUES(
	:activity_id,:activity_type,:activity_name,:activity_date, 
	:location,province,:numofdays,:start_time,:end_time,
	:activity_regis_from,:activity_regis_to,
	:max_capa,:room_capa,:del_indi,
	:insert_time,:update_time,
	:insert_by,:update_by)