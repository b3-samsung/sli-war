package com.b3.web.agent.api.service;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.function.Predicate;
import java.util.stream.Collectors;

import org.hibernate.transform.Transformers;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import com.b3.web.agent.api.util.ObjectUtil;

@Service
public class SearchDocumentService extends AbstractApiService {

	private final 	int PAGE_SIZE = 25;
	
	private Predicate<Map<String, Object>> searchByKeywords(
			final String agentFilter, final String policyCode, final String docCategory, final String fName, final String lName, final String pid
	) {
		return new Predicate<Map<String, Object>>() {
			public boolean test(Map<String, Object> t) {
				return stringContrain(t.get("policyCode"),policyCode)
						&& stringContrain(t.get("categoryId"),docCategory)
						&& stringContrain(t.get("agentCode"),agentFilter)
						&& stringContrain(t.get("phFname"),fName)
						&& stringContrain(t.get("phLname"),lName)
						&& stringContrain(t.get("certiCode"),pid);
			}
		};
	}
	private  boolean stringContrain( Object str, String test ) {
		if(!StringUtils.hasLength(test) || "null".equals(test)|| "*".equals(test)) return true;
		if( str == null  ) str = "";
		return str.toString().contains(test) ;	
	}

//	public Object searchDocuments(String agentCode, String policyCode, String fName, String lName, Integer docCategory, Integer curPage) {
	public Object searchDocuments(String agentCode, String agentFilter, String policyCode, String fName, String lName, Integer docCategory, Integer curPage) {
			
		String sql = sqlTemplate.getResource("search_document.sql");
//		String sqlPageable = sqlTemplate.getResource("search_document_pageable.sql");
	
		
		int firstResult = curPage * PAGE_SIZE;
		int lastIndex = firstResult+PAGE_SIZE;
		
//		org.hibernate.query.Query query = this.apiDatasource.createNativeQuery(sql)
//				.unwrap(org.hibernate.query.Query.class)
//				.setResultTransformer(Transformers.ALIAS_TO_ENTITY_MAP)
//				.setParameter("agentCode", agentCode)
//				.setParameter("policyCode", policyCode)
//				.setParameter("fName", fName)
//				.setParameter("lName", lName)
//				.setParameter("docCategory", docCategory);
//		List<Map<String, Object>> data = ObjectUtil.columnToCamelCase(query.getResultList());
//		
//		org.hibernate.query.Query queryPageable = this.apiDatasource.createNativeQuery(sqlPageable)
//				.unwrap(org.hibernate.query.Query.class)
//				.setResultTransformer(Transformers.ALIAS_TO_ENTITY_MAP)
//				.setParameter("pageSize", perPage)
//				.setParameter("agentCode", agentCode)
//				.setParameter("policyCode", policyCode)
//				.setParameter("fName", fName)
//				.setParameter("lName", lName)
//				.setParameter("docCategory", docCategory);
//		Map<String, Object> pageable = ObjectUtil.columnToCamelCase(queryPageable.getSingleResult());
		
		org.hibernate.query.Query query = this.apiDatasource.createNativeQuery(sql)
				.unwrap(org.hibernate.query.Query.class)
				.setResultTransformer(Transformers.ALIAS_TO_ENTITY_MAP)
				.setParameter("policyCode", policyCode)
				.setParameter("fName", fName)
				.setParameter("lName", lName)
				.setParameter("docCategory", docCategory);
		
		System.out.println("agentFilter : " + agentFilter);
		if( StringUtils.hasLength(agentFilter) && !"null".equals(agentFilter) && !"*".equals(agentFilter)){
			System.out.println("agentFilter " + agentFilter);
			query.setParameter("agentCode", agentFilter);
		} else {
			System.out.println("agentCode " + agentCode);
			query.setParameter("agentCode", agentCode);
		}
		
		List<Map<String, Object>> data = ObjectUtil.columnToCamelCase(query.getResultList());
		System.out.println("policyCode " + policyCode);
		System.out.println("docCategory " + docCategory);
		

//		if (StringUtils.hasLength(policyCode) 
//				|| StringUtils.hasLength(agentFilter) 
//				|| StringUtils.hasLength(String.valueOf(docCategory))
//				|| StringUtils.hasLength(fName) 
//				|| StringUtils.hasLength(lName)
//				|| StringUtils.hasLength(pid)) {
//			data =  data.stream().filter(searchByKeywords(agentFilter, policyCode, (String.valueOf(docCategory)), (fName), (lName), (pid))) .collect(Collectors.<Map<String, Object>>toList());
//		} else {
//			System.out.println("All");
//		}
		
		Map<String, Object> response = new HashMap<String, Object>();
		response.put("detail", data.subList(firstResult, lastIndex> data.size()? data.size(): lastIndex));
		response.put("totalElements", 	data.size());
		response.put("totalPage", data.size()/PAGE_SIZE);
		response.put("pageSize", PAGE_SIZE);
		response.put("element", data.size());
		
		return response;
	}
}
