package com.b3.web.agent.api.rest;

import javax.servlet.http.HttpServletRequest;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.io.Resource;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.b3.web.agent.api.service.FileStorageService;
import com.b3.web.agent.api.service.ListDocCategoryService;
import com.b3.web.agent.api.service.SearchDocumentService;

@Validated
@RestController
@RequestMapping("${api.prefix}/document")
public class SearchDocumentController {
	
	private static final Logger log = LogManager.getLogger(SearchDocumentController.class);
	@Autowired
	private SearchDocumentService SearchDocumentService;
	@Autowired
	private ListDocCategoryService ListDocCategoryService;
    
    @Value("${fileedoc-prefix-path}")
    private  String fileEdocPrefixPath;
    
    @Autowired
    private FileStorageService fileStorageService;
    
	@GetMapping("/list-doc-category")
	public ResponseEntity listDocCategory() {
		return ResponseEntity.ok(ListDocCategoryService.ListDocCategoryQuery());
	}
	
    @GetMapping("download")
    public ResponseEntity<Resource> downloadFile(@RequestParam("fileName") String fileName
    		, HttpServletRequest request)  {

    	Resource resource = null;
        // Try to determine file's content type
        String contentType = null;
        try {
            // Load file as Resource
            resource = fileStorageService.loadFileAsResource(fileEdocPrefixPath, fileName);
            contentType = request.getServletContext().getMimeType(resource.getFile().getAbsolutePath());
   

        // Fallback to the default content type if type could not be determined
        if(contentType == null) {
            contentType = "application/octet-stream";
        }

        return ResponseEntity.ok()
                .contentType(MediaType.parseMediaType(contentType))
                .header(HttpHeaders.CONTENT_DISPOSITION, "attachment; filename=\"" + resource.getFilename() + "\"")
                .body(resource);
        } catch (Exception ex) {
            log.info("Could not determine file type.");
            return ResponseEntity.ok(null);
        }
    }
    
	@GetMapping("/search-documents")
	public ResponseEntity searchEDocuments(
			@RequestParam (name="agentCode", required=true) String agentCode
			, @RequestParam (name="agentFilter", required=false) String agentFilter
			, @RequestParam (name="policyCode", required=true) String policyCode
			, @RequestParam (name="fName", required=true) String fName
			, @RequestParam (name="lName", required=true) String lName
			, @RequestParam (name="docCategory", required=true) Integer docCategory
			, @RequestParam (name="curPage", required=true) Integer curPage
	) {
//		String searchPolicyCode = "%" + policyCode + "%";
//		String searchFName = "%" + fName + "%";
//		String searchLName = "%" + lName + "%";
		return ResponseEntity.ok(SearchDocumentService.searchDocuments(
				agentCode
				, agentFilter
				, policyCode
				, fName
				, lName
				, docCategory
				, curPage)
			);
	}
}
