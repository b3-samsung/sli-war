package com.b3.web.agent.api.service;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.stereotype.Service;

import com.b3.web.agent.api.dd.entity.ApiUser;
import com.b3.web.agent.api.dd.entity.ApiUserRoles;

import javax.transaction.Transactional;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

@Service
public class MyUserDetailsService implements UserDetailsService {

	private static final Logger log = LogManager.getLogger(MyUserDetailsService.class);
    @Autowired
    private ApiUserService userService;

    public UserDetails loadUserByUsername(String username) {
        ApiUser user = userService.findUserByUsername(username);
        
        log.info( "Username: "+ username);
        System.out.println(user);
        List<GrantedAuthority> authorities = getUserAuthority(user.getUserRolesList());
        return buildUserForAuthentication(user, authorities);
    }

    private List<GrantedAuthority> getUserAuthority(Collection<ApiUserRoles> userRoles) {
        Set<GrantedAuthority> roles = new HashSet();
        for (ApiUserRoles role : userRoles) {
            roles.add(new SimpleGrantedAuthority(role.getRoleId().getRole()));
        }
        return new ArrayList(roles);
    }

    private UserDetails buildUserForAuthentication(ApiUser user, List<GrantedAuthority> authorities) {
        return new org.springframework.security.core.userdetails.User(user.getUsername(), user.getPassword(),
                user.getActive(), true, true, true, authorities);
    }
}