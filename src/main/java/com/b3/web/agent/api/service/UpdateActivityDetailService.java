package com.b3.web.agent.api.service;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.persistence.Query;
import javax.transaction.Transactional;

import org.hibernate.jpa.TypedParameterValue;
import org.hibernate.transform.Transformers;
import org.hibernate.type.ShortType;
import org.hibernate.type.StandardBasicTypes;
import org.springframework.stereotype.Service;
//import org.springframework.transaction.annotation.Transactional;

import com.b3.web.agent.api.util.ObjectUtil;

import jline.internal.Log;

@Service
public class UpdateActivityDetailService extends AbstractApiService {

	@Transactional
	public Object updateActivityDetailQuery (
			Integer activityId
			, Integer activityType
			, Integer activitySubType
			, String province
			, String activityName
			, String activityDate
			, String location
			, Integer numOfDays
			, String startTime
			, String endTime
			, String activityRegisFrom
			, String activityRegisTo
			, String activityRegisToTime
			, Integer maxCapa
			, Integer roomCapa
			, String activityDateTo
			, String remark
			) {
//		System.out.println( "activityId "+ activityId);
//		System.out.println( "activityType "+ activityType);
//		System.out.println( "activitySubType "+ activitySubType);
//		System.out.println( "province "+ province);
//		System.out.println( "activityName "+ activityName);
//		System.out.println( "activityDate "+ activityDate);
//		System.out.println( "location "+ location);
//		System.out.println( "numOfDays "+ numOfDays);
//		System.out.println( "startTime "+ startTime);
//		System.out.println( "endTime "+ endTime);
//		System.out.println( "activityRegisFrom "+ activityRegisFrom);
//		System.out.println( "activityRegisTo "+ activityRegisTo);
//		System.out.println( "activityRegisToTime "+ activityRegisToTime);
//		System.out.println("maxCapa "+  maxCapa);
//		System.out.println( "roomCapa "+ roomCapa);
//		System.out.println( "activityDateTo "+ activityDateTo);
//		System.out.println( "remark "+ remark);
		
		
		Map<String, Object> response = new HashMap<String, Object>();
		try {
			String sql = sqlTemplate.getResource("update_activity_detail.sql");
			Query query = this.apiDatasource.createNativeQuery(sql)
					.setParameter("activityType", activityType)
					.setParameter("activitySubType", new TypedParameterValue(StandardBasicTypes.INTEGER, activitySubType))
					.setParameter("province", province)
					.setParameter("activityName", activityName)
					.setParameter("activityDate", activityDate)
					.setParameter("location", location)
					.setParameter("numOfDays", numOfDays)
					.setParameter("startTime", startTime)
					.setParameter("endTime", endTime)
					.setParameter("activityRegisFrom", activityRegisFrom)
					.setParameter("activityRegisTo", activityRegisTo + " " + activityRegisToTime)
					.setParameter("maxCapa", maxCapa)
					.setParameter("roomCapa", roomCapa)
					.setParameter("activityId", activityId)
					.setParameter("activityDateTo", new TypedParameterValue(StandardBasicTypes.STRING, activityDateTo))
					.setParameter("remark", remark);
			
			int rowAffect = query.executeUpdate();
			if (rowAffect > 0) {
				response.put("message", "success");
			} else {
				response.put("message", "failed");
			}


		}catch (Exception e) {
			e.printStackTrace();
			response.put("message", "failed");
		}
		return response;
	}


	@Transactional
	public Object insertActivityDetailQuery (
			Integer activityId
			, Integer activityType
			, Integer activitySubType
			, String province
			, String activityName
			, String activityDate
			, String location
			, Integer numOfDays
			, String startTime
			, String endTime
			, String activityRegisFrom
			, String activityRegisTo
			, String activityRegisToTime
			, Integer maxCapa
			, Integer roomCapa
			, String activityDateTo
			, String remark
			) {
//		System.out.println( "activityId "+ activityId);
//		System.out.println( "activityType "+ activityType);
//		System.out.println( "activitySubType "+ activitySubType);
//		System.out.println( "province "+ province);
//		System.out.println( "activityName "+ activityName);
//		System.out.println( "activityDate "+ activityDate);
//		System.out.println( "location "+ location);
//		System.out.println( "numOfDays "+ numOfDays);
//		System.out.println( "startTime "+ startTime);
//		System.out.println( "endTime "+ endTime);
//		System.out.println( "activityRegisFrom "+ activityRegisFrom);
//		System.out.println( "activityRegisTo "+ activityRegisTo);
//		System.out.println( "activityRegisToTime "+ activityRegisToTime);
//		System.out.println("maxCapa "+  maxCapa);
//		System.out.println( "roomCapa "+ roomCapa);
//		System.out.println( "activityDateTo "+ activityDateTo);
//		System.out.println( "remark "+ remark);
	
		try {
			activityId = getMaxActivityId();
			String sql = sqlTemplate.getResource("insert_activity_detail.sql");
			Query query = this.apiDatasource.createNativeQuery(sql)
					.setParameter("activityId", activityId)
					.setParameter("activitySubType", new TypedParameterValue(StandardBasicTypes.BIG_INTEGER, activitySubType))
					.setParameter("activityType", activityType)
					.setParameter("province", province)
					.setParameter("activityName", activityName)
					.setParameter("activityDate", activityDate)
					.setParameter("location", location)
					.setParameter("numOfDays", numOfDays)
					.setParameter("startTime", startTime)
					.setParameter("endTime", endTime)
					.setParameter("activityRegisFrom", activityRegisFrom)
					.setParameter("activityRegisTo", activityRegisTo + " " + activityRegisToTime)
					.setParameter("maxCapa", maxCapa)
					.setParameter("roomCapa", roomCapa)
					.setParameter("activityDateTo", new TypedParameterValue(StandardBasicTypes.STRING, activityDateTo))
					.setParameter("remark", remark);
			
			int rowAffect = query.executeUpdate();
		
			
			return activityId;
		}catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}

	public int deleteActivityDetail(String activityId ) {
		String sql = sqlTemplate.getResource("update_activity_detail_to_delete.sql");
		int row = this.apiDatasource.createNativeQuery(sql)
				.setParameter("activityId", activityId)
				.executeUpdate();
		return row;
	}
	
	public synchronized Integer getMaxActivityId() {
		String sql = sqlTemplate.getResource("get_max_id_activity.sql");
		BigDecimal id =  (BigDecimal) this.apiDatasource.createNativeQuery(sql).getSingleResult();
		return id.intValue()+1;
	}
}
