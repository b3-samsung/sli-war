package com.b3.web.agent.api.service;

import java.util.List;
import java.util.Map;

import org.hibernate.transform.Transformers;
import org.springframework.stereotype.Service;

import com.b3.web.agent.api.util.ObjectUtil;

@Service
public class ListDocCategoryService extends AbstractApiService {

	public Object ListDocCategoryQuery() {
		
		String sql = sqlTemplate.getResource("list_doc_category.sql");
		org.hibernate.query.Query query = this.apiDatasource.createNativeQuery(sql)
				.unwrap(org.hibernate.query.Query.class)
				.setResultTransformer(Transformers.ALIAS_TO_ENTITY_MAP);
		List<Map<String, Object>> data = ObjectUtil.columnToCamelCase(query.getResultList());
		
		return data;
	}

}
