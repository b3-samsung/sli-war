package com.b3.web.agent.api.service;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.springframework.beans.factory.annotation.Autowired;

import com.fasterxml.jackson.databind.ObjectMapper;

public abstract class AbstractApiService {

	private static final ObjectMapper mapper = new ObjectMapper();
	
	@Autowired
	protected SqlTemplateFactory sqlTemplate;
	
	@PersistenceContext(unitName="apiDatasource")
	protected EntityManager apiDatasource;
	
	@PersistenceContext(unitName="ddDatasource")
	protected EntityManager ddDatasource;

	protected String toCamelCase(String txt) {
		if (txt == null || "".equals(txt))
			return txt;
		txt = txt.toLowerCase();
		while (txt.contains("_")) {
			txt = txt.replaceFirst("_[a-z0-9]", String.valueOf(Character.toUpperCase(txt.charAt(txt.indexOf("_") + 1))));
		}
		return txt;
	}

	protected <T> List<T> toObject(List<Map<String, Object>> data, Class<T> type) {
		List<T> convert = new ArrayList<T>();
		if (data == null) {
			return new ArrayList<T>();
		}
		for (Map<String, Object> m : data) {
			Map<String, Object> map = new HashMap<String, Object>();
			for (Entry<String, Object> e : m.entrySet()) {
				map.put(toCamelCase(e.getKey()), e.getValue());
			}

			T object = mapper.convertValue(map, type);
			convert.add(object);
		}

		return convert;
	}
	
	
}
