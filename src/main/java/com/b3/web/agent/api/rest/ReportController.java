package com.b3.web.agent.api.rest;

import java.io.FileNotFoundException;
import java.io.IOException;

import javax.servlet.http.HttpServletRequest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.io.Resource;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.b3.web.agent.api.service.FileStorageService;
import com.b3.web.agent.api.service.ReportService;

@RestController
@RequestMapping("${api.prefix}/report")
public class ReportController {

    private static final Logger log = LoggerFactory.getLogger(ReportController.class);
    
	@Autowired
	private ReportService reportService;
	@Value("${filereport-prefix-path}")
	private  String fileReportPrefixPath;

	@Autowired
	private FileStorageService fileStorageService;
	@GetMapping("get-report-closym")
	public ResponseEntity<?> getReportClosYM() {
		return ResponseEntity.ok(reportService.reportClosYM());
	}

	@GetMapping("get-report-master")
	public ResponseEntity<?> getReportMaster() {
		return ResponseEntity.ok(reportService.reportMaster());
	}


	@GetMapping("search-report")
	public ResponseEntity<?> searchReport(
			@RequestParam("closYm") String closYm
			, @RequestParam("reportCode") String reportCode
			, @RequestParam("agentCode") String agentCode
			, @RequestParam("filterAgentCode") String filterAgentCode
			){

		//		if( StringUtils.isEmpty(agentCode)) {
		//			agentCode = authCode;
		//		}
		return ResponseEntity.ok(this.reportService.listReport(closYm, reportCode, agentCode, filterAgentCode));
	}

	@GetMapping("download-report")
	public ResponseEntity<Resource> downloadFile(@RequestParam("reportPath") String reportPath
			, HttpServletRequest request) throws FileNotFoundException {
		// Load file as Resource
		try {
		Resource resource = fileStorageService.loadFileAsResource(fileReportPrefixPath, reportPath);

		// Try to determine file's content type
		String contentType = null;
	
			contentType = request.getServletContext().getMimeType(resource.getFile().getAbsolutePath());
	

		// Fallback to the default content type if type could not be determined
		if(contentType == null) {
			contentType = "application/octet-stream";
		}

		return ResponseEntity.ok()
				.contentType(MediaType.parseMediaType(contentType))
				.header(HttpHeaders.CONTENT_DISPOSITION, "attachment; filename=\"" + resource.getFilename() + "\"")
				.body(resource);
		
		} catch (Exception ex) {
			log.info("Could not determine file type.");
			 return ResponseEntity.ok(null);
		}
	}


}
