package com.b3.web.agent.api.dd.repository;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.b3.web.agent.api.dd.entity.ApiRole;

@Repository
public interface RoleRepository extends CrudRepository<ApiRole, Long> {
	 ApiRole findByRole(String role);
}