package com.b3.web.agent.api.service;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.hibernate.transform.Transformers;
import org.springframework.stereotype.Service;

import com.b3.web.agent.api.util.ObjectUtil;

@Service
public class SearchEDocumentsService extends AbstractApiService {
	public Object searchEDocuments(String pid, String birthdate) {
		
		String sql = sqlTemplate.getResource("edoc_get_policy.sql");
		String sqlType = sqlTemplate.getResource("edoc_get_policy_type.sql");
		
		// EDOC-DETAIL
		org.hibernate.query.Query query = this.apiDatasource.createNativeQuery(sql)
				.unwrap(org.hibernate.query.Query.class)
				.setResultTransformer(Transformers.ALIAS_TO_ENTITY_MAP)
				.setParameter("pid", pid)
				.setParameter("birthdate", birthdate);
		List<Map<String, Object>> data =  ObjectUtil.columnToCamelCase(query.getResultList());
		
		// EDOC-TYPE
		org.hibernate.query.Query queryType = this.apiDatasource.createNativeQuery(sqlType)
				.unwrap(org.hibernate.query.Query.class)
				.setResultTransformer(Transformers.ALIAS_TO_ENTITY_MAP)
				.setParameter("pid", pid)
				.setParameter("birthdate", birthdate);
		List<Map<String, Object>> dataType =  ObjectUtil.columnToCamelCase(queryType.getResultList());
		
		
		Map<String, Object> response = new HashMap<String, Object>();
		response.put("detail", data);
		response.put("type", dataType);
		response.put("status", "success");
		
		return response;
	}
}
