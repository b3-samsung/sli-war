package com.b3.web.agent.api.service;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.hibernate.transform.Transformers;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import com.b3.web.agent.api.util.ObjectUtil;

import jline.internal.Log;

@Service
public class PolicyDetailService extends AbstractApiService {

	private static final Logger log = LogManager.getLogger(PolicyDetailService.class);
	public Object getInsuranceDetail(String agentCode, String policyCode) {

		Map<String, Object> response = null;
		Object detail = getPolicyDetail(agentCode, policyCode);
		if( (detail = getPolicyDetail(agentCode, policyCode)) != null ) {
			response = new HashMap<String, Object>();
			response.put("detail", detail);
			response.put("customer", getPolicyBeneficiary(policyCode));
			response.put("product", getPolicyProduct(policyCode));
			response.put("claim", getPolicyClaim(policyCode));
			response.put("paid", getPolicyPaidPeriod(policyCode));
			response.put("barcode", listBarcodeByPolicyCode(policyCode));
			response.put("benefit", listPolicyBenefit(policyCode));
			response.put("beneficiary", listPolicybeneficiary(policyCode));
		}
		
		return response;
	}
	
	public List<Map<String, Object>>  listPolicybeneficiary( String policyCode ) {
		String sql = sqlTemplate.getResource("ins_policy_beneficiary.sql");
		org.hibernate.query.Query query = this.apiDatasource.createNativeQuery(sql)
				.unwrap(org.hibernate.query.Query.class)
				.setResultTransformer(Transformers.ALIAS_TO_ENTITY_MAP)
				.setParameter("policy_code", policyCode);
		List<Map<String, Object>> data = ObjectUtil.columnToCamelCase(query.getResultList());
		return data;
	}
	
	public List<Map<String, Object>>  listPolicyBenefit( String policyCode) {

		if( !StringUtils.hasLength(policyCode)) {
			return null;
		}
		String sql = sqlTemplate.getResource("list_policy_benefit.sql");
		org.hibernate.query.Query query = this.apiDatasource.createNativeQuery(sql)
				.unwrap(org.hibernate.query.Query.class)
				.setResultTransformer(Transformers.ALIAS_TO_ENTITY_MAP)
				.setParameter("POLICY_CODE", policyCode);
		List<Map<String, Object>> data = ObjectUtil.columnToCamelCase(query.getResultList());
		return data;
	}


	public String listBarcodeByPolicyCode( String policyCode) {
		String barcode = null;
		if( !StringUtils.hasLength(policyCode)) {
			return null;
		}
		try {
			String sql = sqlTemplate.getResource("list_barcode_by_policy_code.sql");
			barcode = (String) this.apiDatasource.createNativeQuery(sql)
					.setParameter("policy_code", policyCode)
					.getSingleResult();
		}catch (Exception e) {
			log.error(e);
		}
		return barcode;
	}
	public Object getInsuranceDetailForLineBot(String agentCode, String policyCode) {
		Map<String, Object> response = new HashMap<String, Object>();
		response.put("detail", getPolicyDetail(agentCode, policyCode));
		response.put("beneficiary", getPolicyBeneficiary(policyCode));
		response.put("product", getPolicyProduct(policyCode));
		return response;
	}

	public Object getPolicyDetail(String agentCode, String policyCode) {

		String sql_detail = sqlTemplate.getResource("ins_policy_detail.sql");
		Map<String, Object> data_detail = null;
		// DETAIL
		try{
			org.hibernate.query.Query query = this.apiDatasource.createNativeQuery(sql_detail)
					.unwrap(org.hibernate.query.Query.class)
					.setResultTransformer(Transformers.ALIAS_TO_ENTITY_MAP)
					.setParameter("policy_code", policyCode)
					.setParameter("login_agent_code", agentCode);
			data_detail = ObjectUtil.columnToCamelCase(query.getSingleResult());
		}catch (Exception e) {
			log.error(e);
		}

		return data_detail;
	}
	public Object getPolicyClaim( String policyCode) {

		String sql_claim = sqlTemplate.getResource("ins_policy_claim.sql");

		// CLAIM
		org.hibernate.query.Query query4 = this.apiDatasource.createNativeQuery(sql_claim)
				.unwrap(org.hibernate.query.Query.class)
				.setResultTransformer(Transformers.ALIAS_TO_ENTITY_MAP)
				.setParameter("policy_code", policyCode);
		List<Map<String, Object>> data_claim = ObjectUtil.columnToCamelCase(query4.getResultList());
		return data_claim;
	}

	public Object getPolicyProduct( String policyCode ) {

		String sql_product = sqlTemplate.getResource("ins_policy_product.sql");

		// PRODUCT
		org.hibernate.query.Query query3 = this.apiDatasource.createNativeQuery(sql_product)
				.unwrap(org.hibernate.query.Query.class)
				.setResultTransformer(Transformers.ALIAS_TO_ENTITY_MAP)
				.setParameter("policy_code", policyCode);
		List<Map<String, Object>> data_product = ObjectUtil.columnToCamelCase(query3.getResultList());

		return data_product;
	}


	public Object getPolicyBeneficiary( String policyCode ) {

		String sql_customer = sqlTemplate.getResource("ins_policy_customer.sql");

		// CUSTOMER
		org.hibernate.query.Query query2 = this.apiDatasource.createNativeQuery(sql_customer)
				.unwrap(org.hibernate.query.Query.class)
				.setResultTransformer(Transformers.ALIAS_TO_ENTITY_MAP)
				.setParameter("policy_code", policyCode);
		List<Map<String, Object>> data_customer = ObjectUtil.columnToCamelCase(query2.getResultList());

		return data_customer;
	}

	public Object getPolicyPaidPeriod( String policyCode ) {

		String sql_paid = sqlTemplate.getResource("ins_policy_paid.sql");

		// PAID
		org.hibernate.query.Query query5 = this.apiDatasource.createNativeQuery(sql_paid)
				.unwrap(org.hibernate.query.Query.class)
				.setResultTransformer(Transformers.ALIAS_TO_ENTITY_MAP)
				.setParameter("POLICY_CODE", policyCode);
		List<Map<String, Object>> data_paid = ObjectUtil.columnToCamelCase(query5.getResultList());

		return data_paid;
	}

}
