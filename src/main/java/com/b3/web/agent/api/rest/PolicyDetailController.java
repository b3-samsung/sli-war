package com.b3.web.agent.api.rest;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.b3.web.agent.api.service.PolicyDetailService;
import com.b3.web.agent.api.service.SearchInsuranceService;

@Validated
@RestController
@RequestMapping("${api.prefix}/policy")
public class PolicyDetailController {

	@Autowired
	private PolicyDetailService PolicyDetailService;
	@Autowired
	private SearchInsuranceService searchInsuranceService;
	
	@GetMapping("/ins-policy-detail")
	public ResponseEntity searchInsureDetail(
			@RequestParam(name = "agentCode", required = true) String agentCode
			, @RequestParam(name = "policyCode", required = true) String policyCode
			, @RequestParam(name = "policyId", required = false) String policyId) {
		return ResponseEntity.ok(PolicyDetailService.getInsuranceDetail(agentCode, policyCode));
	}
	@GetMapping("/cb/ins-policy-detail")
	public ResponseEntity getInsuranceDetailForLineBot(
			@RequestParam(name = "agentCode", required = true) String agentCode
			, @RequestParam(name = "policyCode", required = true) String policyCode) {
		return ResponseEntity.ok(PolicyDetailService.getInsuranceDetailForLineBot(agentCode, policyCode));
	}
	
	
	@GetMapping("/policy-detail")
	public ResponseEntity getPolicyDetail(@RequestParam(name = "agentCode", required = true) String agentCode,
			@RequestParam(name = "policyCode", required = true) String policyCode) {
		return ResponseEntity.ok(PolicyDetailService.getPolicyDetail(agentCode, policyCode));
	}

	@GetMapping("/policy-product")
	public ResponseEntity getPolicyProduct(@RequestParam(name = "policyCode", required = true) String policyCode) {
		return ResponseEntity.ok(PolicyDetailService.getPolicyProduct(policyCode));
	}

	@GetMapping("/policy-claim")
	public ResponseEntity getPolicyClaim(@RequestParam(name = "policyCode", required = true) String policyCode) {
		return ResponseEntity.ok(PolicyDetailService.getPolicyClaim(policyCode));
	}
	

	@GetMapping("/policy-beneficiary")
	public ResponseEntity getPolicyBeneficiary(@RequestParam(name = "policyCode", required = true) String policyCode) {
		return ResponseEntity.ok(PolicyDetailService.getPolicyBeneficiary(policyCode));
	}
	
	@GetMapping("search-policy-by-insuerd-name")
	public ResponseEntity<List<Map<String, Object>>> searchPolicyByInsuerdName(
			@RequestParam(name = "agentCode") String agentCode, @RequestParam(name = "filter") String filter) {

		List<Map<String, Object>> result = new ArrayList<Map<String, Object>>();
		try {
			result = searchInsuranceService.searchPolicyByInsuerdName(agentCode, filter);
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		}

		return ResponseEntity.ok(result);
	}

}
