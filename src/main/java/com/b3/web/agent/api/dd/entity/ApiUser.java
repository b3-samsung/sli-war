/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.b3.web.agent.api.dd.entity;

import java.util.Date;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author user
 */
@Entity
@Table(name = "API_USER", catalog = "", schema = "", uniqueConstraints = {@UniqueConstraint(columnNames = {"USERNAME"})})
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "ApiUser.findAll", query = "SELECT u FROM ApiUser u")
    , @NamedQuery(name = "ApiUser.findById", query = "SELECT u FROM ApiUser u WHERE u.id = :id")
    , @NamedQuery(name = "ApiUser.findByUsername", query = "SELECT u FROM ApiUser u WHERE u.username = :username")
    , @NamedQuery(name = "ApiUser.findByPassword", query = "SELECT u FROM ApiUser u WHERE u.password = :password")
    , @NamedQuery(name = "ApiUser.findByFirstname", query = "SELECT u FROM ApiUser u WHERE u.firstname = :firstname")
    , @NamedQuery(name = "ApiUser.findByLastname", query = "SELECT u FROM ApiUser u WHERE u.lastname = :lastname")
    , @NamedQuery(name = "ApiUser.findByCreatedDate", query = "SELECT u FROM ApiUser u WHERE u.createdDate = :createdDate")
    , @NamedQuery(name = "ApiUser.findByCreatedBy", query = "SELECT u FROM ApiUser u WHERE u.createdBy = :createdBy")
    , @NamedQuery(name = "ApiUser.findByUpdatedDate", query = "SELECT u FROM ApiUser u WHERE u.updatedDate = :updatedDate")
    , @NamedQuery(name = "ApiUser.findByUpdatedBy", query = "SELECT u FROM ApiUser u WHERE u.updatedBy = :updatedBy")
    , @NamedQuery(name = "ApiUser.findByIsDelete", query = "SELECT u FROM ApiUser u WHERE u.isDelete = :isDelete")})
public class ApiUser extends StandardField<Long> {

    private static final long serialVersionUID = 1L;

    @Basic(optional = false)
    @Column(name = "USERNAME", nullable = false, length = 256)
    private String username;
    @Basic(optional = false)
    @Column(name = "PASSWORD", nullable = false, length = 256)
    private String password;
    @Column(name = "FIRSTNAME", length = 256)
    private String firstname;
    @Column(name = "LASTNAME", length = 256)
    private String lastname;
    @Basic(optional = false)
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "userId", fetch = FetchType.EAGER)
    private List<ApiUserRoles> userRolesList;

    public ApiUser() {}

    public ApiUser(Long id) {
       super();
       this.id = id;
    }

    public ApiUser(Long id, String username, String password, Date createdDate, Character isDelete) {
    	super();
        this.id = id;
        this.username = username;
        this.password = password;
        this.createdDate = createdDate;
        this.isDelete = isDelete;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getFirstname() {
        return firstname;
    }

    public void setFirstname(String firstname) {
        this.firstname = firstname;
    }

    public String getLastname() {
        return lastname;
    }

    public void setLastname(String lastname) {
        this.lastname = lastname;
    }

    @XmlTransient
    public List<ApiUserRoles> getUserRolesList() {
        return userRolesList;
    }

    public void setUserRolesList(List<ApiUserRoles> userRolesList) {
        this.userRolesList = userRolesList;
    }

}
