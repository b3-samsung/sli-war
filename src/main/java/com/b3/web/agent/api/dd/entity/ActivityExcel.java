package com.b3.web.agent.api.dd.entity;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

import javax.persistence.Entity;
import javax.xml.bind.annotation.XmlRootElement;

@Entity
@XmlRootElement
public class ActivityExcel extends StandardField<Long> implements Serializable{
	
	private static final long serialVersionUID = 1L;
	
	private BigDecimal activityId;
	private int activityType;
	private String activityName;
	private Date activityDate;
	private String location;
	private String province;
	private int numofdays;
	private String startTime;
	private String endTime;
	private Date activityRegisFrom;
	private Date activityRegisTo;
	private int maxCapa;
	private int roomCapa;
	private String delIndi;
	private Date insertTime;
	private Date updateTime;
	private String isTest;
	private Date activityDateTo;
	private int activitySub;
	private String remark1;
	private String insertBy;
	private String updateBy;
	
	public BigDecimal getActivityId() {
		return activityId;
	}
	public void setActivityId(BigDecimal activityId) {
		this.activityId = activityId;
	}
	public int getActivityType() {
		return activityType;
	}
	public void setActivityType(int activityType) {
		this.activityType = activityType;
	}
	public String getActivityName() {
		return activityName;
	}
	public void setActivityName(String activityName) {
		this.activityName = activityName;
	}
	public Date getActivityDate() {
		return activityDate;
	}
	public void setActivityDate(Date activityDate) {
		this.activityDate = activityDate;
	}
	public String getLocation() {
		return location;
	}
	public void setLocation(String location) {
		this.location = location;
	}
	public String getProvince() {
		return province;
	}
	public void setProvince(String province) {
		this.province = province;
	}
	public int getNumofdays() {
		return numofdays;
	}
	public void setNumofdays(int numofdays) {
		this.numofdays = numofdays;
	}
	public String getStartTime() {
		return startTime;
	}
	public void setStartTime(String startTime) {
		this.startTime = startTime;
	}
	public String getEndTime() {
		return endTime;
	}
	public void setEndTime(String endTime) {
		this.endTime = endTime;
	}
	public Date getActivityRegisFrom() {
		return activityRegisFrom;
	}
	public void setActivityRegisFrom(Date activityRegisFrom) {
		this.activityRegisFrom = activityRegisFrom;
	}
	public Date getActivityRegisTo() {
		return activityRegisTo;
	}
	public void setActivityRegisTo(Date activityRegisTo) {
		this.activityRegisTo = activityRegisTo;
	}
	public int getMaxCapa() {
		return maxCapa;
	}
	public void setMaxCapa(int maxCapa) {
		this.maxCapa = maxCapa;
	}
	public int getRoomCapa() {
		return roomCapa;
	}
	public void setRoomCapa(int roomCapa) {
		this.roomCapa = roomCapa;
	}
	public String getDelIndi() {
		return delIndi;
	}
	public void setDelIndi(String delIndi) {
		this.delIndi = delIndi;
	}
	public Date getInsertTime() {
		return insertTime;
	}
	public void setInsertTime(Date insertTime) {
		this.insertTime = insertTime;
	}
	public Date getUpdateTime() {
		return updateTime;
	}
	public void setUpdateTime(Date updateTime) {
		this.updateTime = updateTime;
	}
	public String getIsTest() {
		return isTest;
	}
	public void setIsTest(String isTest) {
		this.isTest = isTest;
	}
	public Date getActivityDateTo() {
		return activityDateTo;
	}
	public void setActivityDateTo(Date activityDateTo) {
		this.activityDateTo = activityDateTo;
	}
	public int getActivitySub() {
		return activitySub;
	}
	public void setActivitySub(int activitySub) {
		this.activitySub = activitySub;
	}
	public String getRemark1() {
		return remark1;
	}
	public void setRemark1(String remark1) {
		this.remark1 = remark1;
	}
	public String getInsertBy() {
		return insertBy;
	}
	public void setInsertBy(String insertBy) {
		this.insertBy = insertBy;
	}
	public String getUpdateBy() {
		return updateBy;
	}
	public void setUpdateBy(String updateBy) {
		this.updateBy = updateBy;
	}
	
}
