package com.b3.web.agent.api.rest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.io.Resource;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.context.support.ServletContextResource;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import com.b3.web.agent.api.model.RequestModel;
import com.b3.web.agent.api.service.FileStorageService;

import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.nio.file.Path;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

@RestController
@RequestMapping("${api.prefix}/file")
public class FileController {

    private static final Logger log = LoggerFactory.getLogger(FileController.class);
    
    @Value("${fileedoc-prefix-path}")
    private  String fileEdocPrefixPath;
    
    @Autowired
    private FileStorageService fileStorageService;
    
    
    
//    @PostMapping("/uploadFile")
//    public UploadFileResponse uploadFile(@RequestParam("file") MultipartFile file) {
//        String fileName = fileStorageService.storeFile(file);
//
//        String fileDownloadUri = ServletUriComponentsBuilder.fromCurrentContextPath()
//                .path("/downloadFile/")
//                .path(fileName)
//                .toUriString();
//
//        return new UploadFileResponse(fileName, fileDownloadUri,
//                file.getContentType(), file.getSize());
//    }

//    @PostMapping("/uploadMultipleFiles")
//    public List<UploadFileResponse> uploadMultipleFiles(@RequestParam("files") MultipartFile[] files) {
//        return Arrays.asList(files)
//                .stream()
//                .map(file -> uploadFile(file))
//                .collect(Collectors.toList());
//    }
    

    @GetMapping("download")
    public ResponseEntity<Resource> downloadFile(@RequestParam("fileName") String fileName
    		, HttpServletRequest request)  {

    	Resource resource = null;
        // Try to determine file's content type
        String contentType = null;
        try {
            // Load file as Resource
            resource = fileStorageService.loadFileAsResource(fileEdocPrefixPath, fileName);
            contentType = request.getServletContext().getMimeType(resource.getFile().getAbsolutePath());
   

        // Fallback to the default content type if type could not be determined
        if(contentType == null) {
            contentType = "application/octet-stream";
        }

        return ResponseEntity.ok()
                .contentType(MediaType.parseMediaType(contentType))
                .header(HttpHeaders.CONTENT_DISPOSITION, "attachment; filename=\"" + resource.getFilename() + "\"")
                .body(resource);
        } catch (Exception ex) {
            log.info("Could not determine file type.");
            return ResponseEntity.ok(null);
        }
    }
}