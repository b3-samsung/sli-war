package com.b3.web.agent.api.rest;

import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.b3.web.agent.api.service.TrainingExaminationService;

@RestController
@RequestMapping("${api.prefix}/training-examination")
public class TrainingExaminationController {

	@Autowired
	private TrainingExaminationService trainingExaminationService;

//	@GetMapping("/search-training")
//	public ResponseEntity<?> searchTraining(@RequestParam("agentCode") String agentCode, @RequestParam("filter") String filter ) {
//		List<Map<String, Object>> result = trainingExaminationService.searchTraining(agentCode, filter);
//		return ResponseEntity.ok(result);
//	}
//
//	@GetMapping("/search-examination")
//	public ResponseEntity<?> searchExamination(@RequestParam("agentCode") String agentCode, @RequestParam("filter") String filter ) {
//		List<Map<String, Object>> result =trainingExaminationService.searchExamination(agentCode, filter);
//		return ResponseEntity.ok(result);
//	}
//	
	
	@GetMapping("/get-agent-activities")
	public ResponseEntity<?> getAgentActivities(@RequestParam("agentCode") String agentCode, @RequestParam("filter") String filter ) {
		Map<String, List<Map<String, Object>>> result =trainingExaminationService.getAgentActivities(agentCode, filter);
		return ResponseEntity.ok(result);
	}
	
	@GetMapping("/get-agent-activities-result")
	public ResponseEntity<?> getAgentActivitiesResult(@RequestParam("agentCode") String agentCode, @RequestParam("filter") String filter ) {
		Map<String, List<Map<String, Object>>> result =trainingExaminationService.getAgentActivitiesResult(agentCode, filter);
		return ResponseEntity.ok(result);
	}
	
	
}
