package com.b3.web.agent.api.util;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import com.fasterxml.jackson.core.JsonParser.Feature;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;

public class ObjectUtil {

	public static final ObjectMapper mapper = new ObjectMapper();
	static {
		mapper.configure(Feature.ALLOW_UNQUOTED_FIELD_NAMES, true)
		.configure(Feature.ALLOW_COMMENTS, true)
		.configure(Feature.ALLOW_SINGLE_QUOTES, true)
		.configure(DeserializationFeature.ACCEPT_SINGLE_VALUE_AS_ARRAY, true)
		.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false)
		.configure(SerializationFeature.INDENT_OUTPUT, true)
		.configure(SerializationFeature.WRITE_NULL_MAP_VALUES, true);
	}
	public static String toCamelCase(String txt) {
		if (txt == null || "".equals(txt))
			return txt;
		if (txt.contains("ROWNUM_"))
			return txt;
		txt = txt.toLowerCase();
		while (txt.contains("_")) {
			txt = txt.replaceFirst("_[a-z0-9]",
					String.valueOf(Character.toUpperCase(txt.charAt(txt.indexOf("_") + 1))));
		}
		return txt;
	}

	public static <T> List<T> toObject(List<Map<String, Object>> data, Class<T> type) {
		List<T> convert = new ArrayList<T>();
		if (data == null) {
			return new ArrayList<T>();
		}
		for (Map<String, Object> m : data) {
			Map<String, Object> map = new HashMap<String, Object>();
			for (Entry<String, Object> e : m.entrySet()) {
				map.put(toCamelCase(e.getKey()), e.getValue());
			}

			T object = mapper.convertValue(map, type);
			convert.add(object);
		}

		return convert;
	}

	public static Map<String, Object> columnToCamelCase(Object data) {
		Map<String, Object> map = new HashMap<String, Object>();
		if (data == null) {
			return map;
		}
		map = mapper.convertValue(data, new TypeReference<HashMap<String, Object>>() {});
		return columnToCamelCase(map);
	}
	
	public static Map<String, Object> columnToCamelCase(Map<String, Object> data) {

		if (data == null) {
			return data;
		}

		Map<String, Object> map = new HashMap<String, Object>();
		for (Entry<String, Object> e : data.entrySet()) {
			String kc = toCamelCase(e.getKey());
			map.put(kc, e.getValue());
		}

		return map;
	}

	public static List<Map<String, Object>> columnToCamelCase(List<Map<String, Object>> data) {
		List<Map<String, Object>> convert = new ArrayList<Map<String, Object>>();
		if (data == null) {
			return data;
		}
		for (Map<String, Object> m : data) {
			convert.add(columnToCamelCase(m));
		}

		return convert;
	}
}
