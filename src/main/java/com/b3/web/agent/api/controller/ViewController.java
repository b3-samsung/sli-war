package com.b3.web.agent.api.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;

import com.b3.web.agent.api.dd.entity.ApiClientDetails;
import com.b3.web.agent.api.dd.repository.ClientDetailRepository;

@Controller
public class ViewController {

	@Autowired
	private ClientDetailRepository clientDetailRepository;
    @GetMapping(value= {"/", "index"})
    public String root(Model model) {
    	model.addAttribute("clients", clientDetailRepository.findAll());
        return "index";
    }

    @GetMapping("/user")
    public String userIndex() {
        return "user/index";
    }
    
    @GetMapping("/manage")
    public String manage( @RequestParam(value = "id", required = false) Long id, Model model ) {
    
    	if( id == null ) {
    		return "redirect:/";
    	}
    	model.addAttribute("data", clientDetailRepository.findById(id).get());
   
        return "manage";
    }
    
    @PostMapping("/manage")
    public String manageSubmit( @RequestParam(value = "id", required = false) Long id, @ModelAttribute ApiClientDetails data, Model model ) {
    
    	System.out.println(id);
    	System.out.println(data);
    	System.out.println(	model.getAttribute("data"));
    	model.addAttribute("data", data);
        return "manage";
    }
    

    @GetMapping("/login")
    public String login( Authentication auth) {
    	if( auth != null) {
    		return "redirect:index";
    	}
        return "login";
    }

    @GetMapping("/access-denied")
    public String accessDenied() {
        return "/error/access-denied";
    }
}