package com.b3.web.agent.api.rest;

import java.util.HashMap;
import java.util.Map;

import org.hibernate.ResourceClosedException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.b3.web.agent.api.model.ActivityDetailModel;
import com.b3.web.agent.api.service.UpdateActivityDetailService;

@RestController
@RequestMapping("${api.prefix}/update-activity")
public class UpdateActivityDetailController {
	
	@Autowired
	private UpdateActivityDetailService UpdateActivityDetailService;

	@PostMapping("update-detail")
	public ResponseEntity<?> eRegister(@RequestBody ActivityDetailModel model) 
	 {
		return ResponseEntity.ok(UpdateActivityDetailService.updateActivityDetailQuery(
				model.getActivityId()
				, model.getActivityType()
				,model.getActivitySubType()
				, model.getProvince()
				, model.getActivityName()
				, model.getActivityDate()
				, model.getLocation()
				, model.getNumOfDays()
				, model.getStartTime()
				, model.getEndTime()
				, model.getActivityRegisFrom()
				, model.getActivityRegisTo()
				, model.getActivityRegisToTime()
				, model.getMaxCapa()
				, model.getRoomCapa()
				, model.getActivityDateTo()
				, model.getRemark())
			);
	}
	@GetMapping("delete")
	public ResponseEntity<?> deleteActivity(@RequestParam (name="activityId", required=true) String activityId )  {
		int row = UpdateActivityDetailService.deleteActivityDetail(activityId);
		if(row>0) {
			return ResponseEntity.ok(row);
		}
		return ResponseEntity.ok(null);
			
	}
	
	@PostMapping("insert-detail")
	public ResponseEntity<?> insertERegister(@RequestBody ActivityDetailModel model)  {

		return ResponseEntity.ok(UpdateActivityDetailService.insertActivityDetailQuery(
				model.getActivityId()
				, model.getActivityType()
				,model.getActivitySubType()
				, model.getProvince()
				, model.getActivityName()
				, model.getActivityDate()
				, model.getLocation()
				, model.getNumOfDays()
				, model.getStartTime()
				, model.getEndTime()
				, model.getActivityRegisFrom()
				, model.getActivityRegisTo()
				, model.getActivityRegisToTime()
				, model.getMaxCapa()
				, model.getRoomCapa()
				, model.getActivityDateTo()
				,model.getRemark()
				)
			);
		
	}
}
