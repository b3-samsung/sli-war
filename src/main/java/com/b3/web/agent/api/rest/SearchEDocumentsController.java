package com.b3.web.agent.api.rest;

import java.time.Instant;
import java.time.temporal.ChronoUnit;
import java.util.Base64;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.io.Resource;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.b3.web.agent.api.service.FileStorageService;
import com.b3.web.agent.api.service.SearchEDocumentsService;

@Validated
@RestController
@RequestMapping("${api.prefix}/public")
public class SearchEDocumentsController {

    private static final Logger log = LoggerFactory.getLogger(SearchEDocumentsController.class);
    
    @Value("${fileedoc-prefix-path}")
    private  String fileEdocPrefixPath;
	@Autowired
	private SearchEDocumentsService SearchEDocumentsService;
	
	
    @Autowired
    private FileStorageService fileStorageService;
    
    
    @GetMapping("download")
    public ResponseEntity<Resource> downloadFile(@RequestParam("filePath") String filePath
    		, HttpServletRequest request)  {

    	Resource resource = null;
        // Try to determine file's content type
        String contentType = null;
        try {
            // Load file as Resource
            resource = fileStorageService.loadFileAsResource(fileEdocPrefixPath, filePath);
            contentType = request.getServletContext().getMimeType(resource.getFile().getAbsolutePath());
   

        // Fallback to the default content type if type could not be determined
        if(contentType == null) {
            contentType = "application/octet-stream";
        }

        return ResponseEntity.ok()
                .contentType(MediaType.parseMediaType(contentType))
                .header(HttpHeaders.CONTENT_DISPOSITION, "attachment; filename=\"" + filePath + "\"")
                .body(resource);
        } catch (Exception ex) {
            log.info("Could not determine file type.");
            return ResponseEntity.ok(null);
        }
    }
    
	@GetMapping("/search-edocuments")
	public ResponseEntity searchEDocumentsByPid(
			@RequestParam (name="key", required=true) String key
	) {
		try {
			String decodeNew = new String(Base64.getUrlDecoder().decode(key));
//			System.out.println("TRY DECODE: " + decodeNew);
			
			// PREPARING JSON FIELD
			decodeNew = decodeNew.replace("{", "");
			decodeNew = decodeNew.replace("}", "");
			decodeNew = decodeNew.replace("\"", "");
			String attrKey [] = decodeNew.split(",");
			
			// GET ATTIBUTE NAME
			String pid = "", birthdate = "", token = "";
			Long reqDate = 0L;
			long unixTime = Instant.now().toEpochMilli();
			
			// CONVERT UNIX TIMESTAMP 13 DIGIT TO DATE
			// Instant.ofEpochMilli(Long.parseLong(reqDate))
			
			for (int i = 0; i < attrKey.length; i++) {
				String objVal [] = attrKey[i].split(":");
				String keyname = objVal[0];
				String value = objVal[1];
				if (keyname.equalsIgnoreCase("token")) token = value;
				else if (keyname.equalsIgnoreCase("id")) pid = value;
				else if (keyname.equalsIgnoreCase("dob")) birthdate = value;
				else if (keyname.equalsIgnoreCase("datenow")) reqDate = Long.parseLong(value);
			}
			
			long rangeAccessTime = Instant.ofEpochMilli(reqDate).plus(5, ChronoUnit.MINUTES).toEpochMilli();
//			System.out.println(token + " = " + pid + " - " + birthdate + " - " + reqDate + " > " + rangeAccessTime + " | " + unixTime);
			System.out.println(Instant.ofEpochMilli(reqDate) + " > " + Instant.ofEpochMilli(rangeAccessTime) + " | " + Instant.ofEpochMilli(unixTime));
			if (unixTime < rangeAccessTime) {
				return ResponseEntity.ok(SearchEDocumentsService.searchEDocuments(pid, birthdate));
			}
			else {
				Map<String, Object> response = new HashMap<String, Object>();
				response.put("status", "failed");
				return ResponseEntity.ok(response);
			}
		} catch (Exception e) {
			Map<String, Object> response = new HashMap<String, Object>();
			response.put("status", "failed");
			return ResponseEntity.ok(response);
		}
	}
}