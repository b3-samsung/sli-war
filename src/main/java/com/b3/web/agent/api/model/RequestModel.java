package com.b3.web.agent.api.model;

import java.util.Date;

public class RequestModel {
	private Integer registerId;
	private Integer registerType;
	private Integer activityId;
	private Integer activityTypeId;
	
	private String id;
	private String title;
	private String name;
	private String surname;
	private String gender;
	private String dob;
	private Integer age;
	private String educationLevel;
	
	private String address1;
	private String address2;
	private String address3;
	private String address4;
	private String address5;
	
	private String zip;
	private String mobile;
	private String email;
	private String line;
	
	private String recommenderCode;
	private String recommendLicenseNumber;
	private String recommendDateOfLicenseExpiration;
                       
	private String delIndi;
	private String attendanceResult;
	private String examResult;
	
	private String pictureLocal;
	private String payinLocal;
	private String idcardLocal;
	private String dateExpire;
	private String dateCreate;
	
	private Date insertTime;
	private Date updateTime;
	
	private String insertBy;
	private String updateBy;
	
	private String agentCode;
	
	private String fileName;
	private Long fileSize;
	private String fileBased64;
	private String fileType;
	
	private String sortType;
	
	private String bankCode;
	private String payinNum;
	private Double payinAmount;
	private String payinDate;
	private String payinTime;
	private String payinRef;
	
	private String attend;
	
	public Integer getRegisterId() {
		return registerId;
	}
	public void setRegisterId(Integer registerId) {
		this.registerId = registerId;
	}
	public Integer getRegisterType() {
		return registerType;
	}
	public void setRegisterType(Integer registerType) {
		this.registerType = registerType;
	}
	public Integer getActivityId() {
		return activityId;
	}
	public void setActivityId(Integer activityId) {
		this.activityId = activityId;
	}
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getTitle() {
		return title;
	}
	public void setTitle(String title) {
		this.title = title;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getSurname() {
		return surname;
	}
	public void setSurname(String surname) {
		this.surname = surname;
	}
	public String getGender() {
		return gender;
	}
	public void setGender(String gender) {
		this.gender = gender;
	}
	public String getDob() {
		return dob;
	}
	public void setDob(String dob) {
		this.dob = dob;
	}
	public Integer getAge() {
		return age;
	}
	public void setAge(Integer age) {
		this.age = age;
	}
	public String getAddress1() {
		return address1;
	}
	public void setAddress1(String address1) {
		this.address1 = address1;
	}
	public String getAddress2() {
		return address2;
	}
	public void setAddress2(String address2) {
		this.address2 = address2;
	}
	public String getAddress3() {
		return address3;
	}
	public void setAddress3(String address3) {
		this.address3 = address3;
	}
	public String getAddress4() {
		return address4;
	}
	public void setAddress4(String address4) {
		this.address4 = address4;
	}
	public String getAddress5() {
		return address5;
	}
	public void setAddress5(String address5) {
		this.address5 = address5;
	}
	public String getZip() {
		return zip;
	}
	public void setZip(String zip) {
		this.zip = zip;
	}
	public String getMobile() {
		return mobile;
	}
	public void setMobile(String mobile) {
		this.mobile = mobile;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public String getLine() {
		return line;
	}
	public void setLine(String line) {
		this.line = line;
	}
	public String getRecommenderCode() {
		return recommenderCode;
	}
	public void setRecommenderCode(String recommenderCode) {
		this.recommenderCode = recommenderCode;
	}

	public String getDelIndi() {
		return delIndi;
	}
	public void setDelIndi(String delIndi) {
		this.delIndi = delIndi;
	}
	public String getAttendanceResult() {
		return attendanceResult;
	}
	public void setAttendanceResult(String attendanceResult) {
		this.attendanceResult = attendanceResult;
	}
	public String getExamResult() {
		return examResult;
	}
	public void setExamResult(String examResult) {
		this.examResult = examResult;
	}
	public String getPictureLocal() {
		return pictureLocal;
	}
	public void setPictureLocal(String pictureLocal) {
		this.pictureLocal = pictureLocal;
	}

	public String getIdcardLocal() {
		return idcardLocal;
	}
	public void setIdcardLocal(String idcardLocal) {
		this.idcardLocal = idcardLocal;
	}
	public Date getInsertTime() {
		return insertTime;
	}
	public void setInsertTime(Date insertTime) {
		this.insertTime = insertTime;
	}
	public Date getUpdateTime() {
		return updateTime;
	}
	public void setUpdateTime(Date updateTime) {
		this.updateTime = updateTime;
	}
	public String getInsertBy() {
		return insertBy;
	}
	public void setInsertBy(String insertBy) {
		this.insertBy = insertBy;
	}
	public String getUpdateBy() {
		return updateBy;
	}
	public void setUpdateBy(String updateBy) {
		this.updateBy = updateBy;
	}
	public String getFileName() {
		return fileName;
	}
	public void setFileName(String fileName) {
		this.fileName = fileName;
	}
	public Long getFileSize() {
		return fileSize;
	}
	public void setFileSize(Long fileSize) {
		this.fileSize = fileSize;
	}
	public String getFileBased64() {
		return fileBased64;
	}
	public void setFileBased64(String fileBased64) {
		this.fileBased64 = fileBased64;
	}
	public String getFileType() {
		return fileType;
	}
	public void setFileType(String fileType) {
		this.fileType = fileType;
	}
	public String getAgentCode() {
		return agentCode;
	}
	public void setAgentCode(String agentCode) {
		this.agentCode = agentCode;
	}
	public String getSortType() {
		return sortType;
	}
	public void setSortType(String sortType) {
		this.sortType = sortType;
	}
	public String getBankCode() {
		return bankCode;
	}
	public void setBankCode(String bankCode) {
		this.bankCode = bankCode;
	}
	public String getPayinNum() {
		return payinNum;
	}
	public void setPayinNum(String payinNum) {
		this.payinNum = payinNum;
	}
	public Double getPayinAmount() {
		return payinAmount;
	}
	public void setPayinAmount(Double payinAmount) {
		this.payinAmount = payinAmount;
	}
	public String getPayinDate() {
		return payinDate;
	}
	public void setPayinDate(String payinDate) {
		this.payinDate = payinDate;
	}
	public String getPayinTime() {
		return payinTime;
	}
	public void setPayinTime(String payinTime) {
		this.payinTime = payinTime;
	}
	public String getPayinRef() {
		return payinRef;
	}
	public void setPayinRef(String payinRef) {
		this.payinRef = payinRef;
	}
	public String getAttend() {
		return attend;
	}
	public void setAttend(String attend) {
		this.attend = attend;
	}
	public String getRecommendLicenseNumber() {
		return recommendLicenseNumber;
	}
	public void setRecommendLicenseNumber(String recommendLicenseNumber) {
		this.recommendLicenseNumber = recommendLicenseNumber;
	}
	public String getRecommendDateOfLicenseExpiration() {
		return recommendDateOfLicenseExpiration;
	}
	public void setRecommendDateOfLicenseExpiration(String recommendDateOfLicenseExpiration) {
		this.recommendDateOfLicenseExpiration = recommendDateOfLicenseExpiration;
	}
	public String getDateExpire() {
		return dateExpire;
	}
	public void setDateExpire(String dateExpire) {
		this.dateExpire = dateExpire;
	}
	public String getDateCreate() {
		return dateCreate;
	}
	public void setDateCreate(String dateCreate) {
		this.dateCreate = dateCreate;
	}
	public String getEducationLevel() {
		return educationLevel;
	}
	public void setEducationLevel(String educationLevel) {
		this.educationLevel = educationLevel;
	}
	public String getPayinLocal() {
		return payinLocal;
	}
	public void setPayinLocal(String payinLocal) {
		this.payinLocal = payinLocal;
	}
	public Integer getActivityTypeId() {
		return activityTypeId;
	}
	public void setActivityTypeId(Integer activityTypeId) {
		this.activityTypeId = activityTypeId;
	}

}
