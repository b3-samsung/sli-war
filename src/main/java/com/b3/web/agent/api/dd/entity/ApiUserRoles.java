/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.b3.web.agent.api.dd.entity;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author user
 */
@Entity
@Table(name = "API_USER_ROLES", catalog = "", schema = "", uniqueConstraints = {
    @UniqueConstraint(columnNames = {"USER_ID", "ROLE_ID"})})
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "ApiUserRoles.findAll", query = "SELECT u FROM ApiUserRoles u")
    , @NamedQuery(name = "ApiUserRoles.findById", query = "SELECT u FROM ApiUserRoles u WHERE u.id = :id")
    , @NamedQuery(name = "ApiUserRoles.findByCreatedDate", query = "SELECT u FROM ApiUserRoles u WHERE  u.createdDate = :createdDate")
    , @NamedQuery(name = "ApiUserRoles.findByCreatedBy", query = "SELECT u FROM ApiUserRoles u WHERE u.createdBy = :createdBy")
    , @NamedQuery(name = "ApiUserRoles.findByUpdatedDate", query = "SELECT u FROM ApiUserRoles u WHERE u.updatedDate = :updatedDate")
    , @NamedQuery(name = "ApiUserRoles.findByUpdatedBy", query = "SELECT u FROM ApiUserRoles u WHERE u.updatedBy = :updatedBy")
    , @NamedQuery(name = "ApiUserRoles.findByIsDelete", query = "SELECT u FROM ApiUserRoles u WHERE u.isDelete = :isDelete")})
public class ApiUserRoles extends StandardField<Long> implements Serializable {

    private static final long serialVersionUID = 1L;
   
    @JoinColumn(name = "ROLE_ID", referencedColumnName = "ID", nullable = false)
    @ManyToOne(optional = false, fetch = FetchType.EAGER)
    private ApiRole roleId;
    @JoinColumn(name = "USER_ID", referencedColumnName = "ID", nullable = false)
    @ManyToOne(optional = false, fetch = FetchType.EAGER)
    private ApiUser userId;

    public ApiUserRoles() {
    }
 
    public ApiUserRoles(Long id) {
        this.id = id;
    }

    public ApiUserRoles(Long id, Date createdDate, Character isDelete) {
        this.id = id;
        this.createdDate = createdDate;
        this.isDelete = isDelete;
    }

    public ApiRole getRoleId() {
        return roleId;
    }

    public void setRoleId(ApiRole roleId) {
        this.roleId = roleId;
    }

    public ApiUser getUserId() {
        return userId;
    }

    public void setUserId(ApiUser userId) {
        this.userId = userId;
    }



    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof ApiUserRoles)) {
            return false;
        }
        ApiUserRoles other = (ApiUserRoles) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }


    
}
