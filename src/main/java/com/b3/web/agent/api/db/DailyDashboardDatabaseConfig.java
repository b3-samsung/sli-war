package com.b3.web.agent.api.db;

import java.util.Properties;

import javax.sql.DataSource;

import org.hibernate.jpa.HibernatePersistenceProvider;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.boot.jdbc.DataSourceBuilder;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Primary;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.orm.jpa.JpaTransactionManager;
import org.springframework.orm.jpa.LocalContainerEntityManagerFactoryBean;
import org.springframework.transaction.PlatformTransactionManager;
import org.springframework.transaction.annotation.EnableTransactionManagement;

import com.zaxxer.hikari.HikariDataSource;

@Configuration
@EnableTransactionManagement
@EnableJpaRepositories(basePackages = "com.b3.web.agent.api.dd.repository",
entityManagerFactoryRef = "ddEntityManagerFactory",
transactionManagerRef= "ddTransactionManager"
		)
//@PropertySource("file:${config.path}/application-${spring.profiles.active}.properties")
public class DailyDashboardDatabaseConfig  {


	@Primary
	@Bean("ddDatasource")
	@ConfigurationProperties("spring.datasource-dd")
	public DataSource ddDatasource() {
		return DataSourceBuilder.create().type(HikariDataSource.class).build();
	}

	@Primary
	@Bean(name = "ddEntityManagerFactory")
	public LocalContainerEntityManagerFactoryBean ddEntityManagerFactory() {
		return new LocalContainerEntityManagerFactoryBean() {{
			setDataSource(ddDatasource());
			setPersistenceProviderClass(HibernatePersistenceProvider.class);
			setPersistenceUnitName("ddDatasource");
			setPackagesToScan("com.b3.web.agent.api.dd.entity");
			setJpaProperties(ddJpaProperties());
		}};
	}

	@Bean("ddJpaProperties")
	@ConfigurationProperties("spring.jpa.properties")
	public Properties ddJpaProperties() {
		return new Properties();
	}

	@Primary
	@Bean("ddTransactionManager")
	public PlatformTransactionManager ddTransactionManager(
			final @Qualifier("ddEntityManagerFactory") LocalContainerEntityManagerFactoryBean ddEntityManagerFactory) {
		return new JpaTransactionManager(ddEntityManagerFactory.getObject());
	}


}
