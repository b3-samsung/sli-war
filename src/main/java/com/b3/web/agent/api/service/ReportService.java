package com.b3.web.agent.api.service;

import java.io.File;
import java.nio.file.Path;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.hibernate.transform.Transformers;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import com.b3.web.agent.api.util.ObjectUtil;

@Service
@SuppressWarnings({"rawtypes", "deprecation", "unchecked"})
public class ReportService extends AbstractApiService {

	private static final Logger log = LogManager.getLogger(ReportService.class);
	private static final SimpleDateFormat SDF = new SimpleDateFormat("yyyyMM");
	private static final SimpleDateFormat SDF2 = new SimpleDateFormat("yyyyMMdd");

	private static final String REPORT_FIRST_DATE = "201812";

	@Value("${filereport-prefix-path}")
	private String reportRrefixPath;


	public List<Map<String, Object>> reportMaster() {
//		Timer.start();
		String sql = sqlTemplate.getResource("report-master.sql");
		
		org.hibernate.query.Query query = this.apiDatasource.createNativeQuery(sql)
				.unwrap(org.hibernate.query.Query.class)
				.setResultTransformer(Transformers.ALIAS_TO_ENTITY_MAP);
		List<Map<String, Object>> data = ObjectUtil.columnToCamelCase(query.getResultList());

//		Timer.stop();
//		System.out.println("reportMaster ->" + Timer.end());
		return data;
	}

	public  List<Map<String, String>> reportClosYM() {
//		Timer.start();
		List<Map<String, String>> result = new ArrayList<Map<String, String>>();
		try {
			Calendar now = Calendar.getInstance();
			Calendar past = Calendar.getInstance(Locale.ENGLISH);
			past.setTime(SDF.parse(REPORT_FIRST_DATE));

			while (now.after(past)) {
			
				int year = now.get(Calendar.YEAR);
				int month = now.get(Calendar.MONTH);

				String desc = String.format("%04d / %02d", year, month + 1);
				String code = String.format("%04d%02d", year, month + 1);
				Map<String, String> data = new HashMap<String, String>();
				data.put("code", code);
				data.put("desc", desc);
				result.add(data);
				now.add(Calendar.MONDAY, -1);
			}
		} catch (ParseException e) {
			log.error(e);
		}
//		Timer.stop();
//		List<Map<String, String>> list = new ArrayList<Map<String, String>>();
//		while( !result.isEmpty()) {
//			list.add(result.pop());
//		}
//		System.out.println("reportClosYM ->" + Timer.end());
		return result;
	}

	public List<Map<String, Object>>  listReport(
			String closYm
			,  String reportCode
			,  String agentCode
			, String filterAgentCode
			){
		
		List<Map<String, Object>> list = new ArrayList<Map<String,Object>>();
		Map<String, Map<String, Object>> downline = listDownlineReport(agentCode, filterAgentCode);
		List<Map<String, Object>> rpt = reportMaster() ;

		if( !"*".equals(reportCode)) {
			rpt = rpt.stream().filter( item -> reportCode.equals(item.get("code").toString() )).collect(Collectors.toList());
		}
		rpt = rpt.stream().filter( item -> !"*".equals(item.get("code").toString() )).collect(Collectors.toList());
		

		String path = this.reportRrefixPath+  File.separator +closYm ;
		for(Map<String, Object> r : rpt  ) {
			String reportName = r.get("reportName").toString();
			list.addAll( listReport(closYm, path, reportName, agentCode, filterAgentCode, downline) );
		}

		return list;
	}

	private List<Map<String, Object>> listReport(	
			String closYm
			,  String path
			,  String reportName
			,  String agentCode
			, String filterAgentCode
			, Map<String,  Map<String, Object>> downline
			){
		Set dataSet = new HashSet<>();
		File dirPath = new File( path, reportName);
		List<Map<String, Object>> list = new ArrayList<Map<String,Object>>();
		//tem.out.println(path);
		//System.out.println(reportName);
		if( !dirPath.exists() ) {
			log.info( String.format("Directory dose not exist %s/%s" , path, reportName ));
			return list;
		}else {
			log.info( String.format("Directory  exist %s/%s" , path, reportName ));
		}
		int i = 1;
		String rpt = "";
		String[] rptArr = reportName.split("-", 0);

		if( rptArr.length <= 1 ) {
			rpt = rptArr[0].trim();
		}else {
			rpt = rptArr[1].trim();
		}

		for( File f : dirPath.listFiles() ) {
			if( f.isDirectory() ) {

				String dirDate = f.getName();
				String dateFormat = "";
				try {
					Date dt = SDF2.parse(dirDate);
					Calendar cal = Calendar.getInstance();
					cal.setTime(dt);
					dateFormat = String.format("%04d/%02d/%02d", cal.get(Calendar.YEAR), cal.get(Calendar.MONTH) +1, cal.get(Calendar.DAY_OF_MONTH));
					log.info(dateFormat);
				} catch (Exception e) {
					e.printStackTrace();
					continue;
				}

				for( File target : f.listFiles()) {
					
					if( target.isDirectory()) {
						log.info("Is a directory : " + target.getPath());
						continue;
					}
					
				
//					System.out.println("1");
//					String[] fileElement = ff.getName().split("\\.");
					String fileName = target.getName();
//					log.info("FileName "+ fileName);
//					String  agentCodeFromFile = null; 
//					if( (agentCodeFromFile= findAgentCode(fileName)) == null){
//						continue;
//					}
					
//					System.out.println("2");
//					System.out.println("agentCodeFromFile " + agentCodeFromFile);
//					System.out.println("agentCodeFromFile " + agentCode);
					
					if( dataSet.contains(fileName) ) {
						continue;
					}
					if( !fileName.contains(agentCode)  || !fileName.contains(filterAgentCode) )  {
						continue;
					}
					
					
					
//					System.out.println("fileName " + fileName);
//					System.out.println(downline.keySet());
					Map<String, Object> agent = null;
					try {

						agent = new HashMap(downline.get(StringUtils.hasLength(filterAgentCode)?filterAgentCode:agentCode ));
						agent.put("id", (i++));
						agent.put("date", dateFormat);
						agent.put("reportName", rpt);
						agent.put("reportPath", closYm+ File.separator + reportName+ File.separator + dirDate + File.separator + target.getName());
						list.add(agent);
						dataSet.add(rpt);
						
					}catch (NullPointerException e) {
						log.info("Error fileName:", fileName);
//						e.printStackTrace();
						continue;	
					}
			

//					System.out.println( fileName );
				}
			}


		}
//		Timer.stop();
//		System.out.println("listReport ->" + Timer.end());
		System.gc();
		return list;
	}


	private Map<String, Map<String, Object>>  listDownlineReport( String agentCode, String filterAgentCode){
//		Timer.start();
		String sql = sqlTemplate.getResource("list-report.sql");
		org.hibernate.query.Query query = this.apiDatasource.createNativeQuery(sql)
				.unwrap(org.hibernate.query.Query.class)
				.setResultTransformer(Transformers.ALIAS_TO_ENTITY_MAP)
				.setParameter("AGENT_CODE", agentCode)
				.setParameter("FILTER_AGENT_CODE", filterAgentCode);
		
		List<Map<String, Object>> data = ObjectUtil.columnToCamelCase(query.getResultList());
//		Timer.stop();
//		System.out.println("listDownlineReport S1 -> " + Timer.end());
		Map<String, Map<String, Object>> map = new HashMap();
		for(Map<String, Object> row : data ) {
			map.put((String) row.get("agentCode"), row);
		}
//		Timer.stop();
		
//		System.out.println("listDownlineReport S2-> " + Timer.end());
		return map;
	}

	public String findAgentCode(String fileName) {
		if( !StringUtils.hasLength(fileName ) ) {
			return null;
		}
		String arr[] = fileName.split("_");

		if(arr.length != 4 ) {
			return null;
		}
		String agentCode = arr[3].split("\\.")[0];

		return agentCode;

	}
	
	
//	public static class Timer {
//		private static Long start = 0L;
//		private static Long stop = 0L;
//		
//		public static void start() {
//			start = Calendar.getInstance().getTimeInMillis();
//		}
//		
//		public static void stop() {
//			stop = Calendar.getInstance().getTimeInMillis();
//		}
//		
//		public static Long end() {
//			Long diff = stop - start;
//			start = stop;
//			return diff;
//		}
//	}
}
