package com.b3.web.agent.api.service;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.function.Predicate;
import java.util.stream.Collectors;

import org.hibernate.transform.Transformers;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import com.b3.web.agent.api.util.ObjectUtil;

@Service
public class ActivityDetailService extends AbstractApiService {

	final int PAGE_SIZE = 25;
	private static Predicate<Map<String, Object>> searchByKeywords(final String keywords) {
		return new Predicate<Map<String, Object>>() {
			public boolean test(Map<String, Object> t) {
				return t.get("registerId").toString().contains(keywords)
						|| t.get("id").toString().contains(keywords)
						|| t.get("name").toString().contains(keywords)
						|| t.get("recommenderCode").toString().contains(keywords);
			}
		};
	}
	private static List<Map<String, Object>> filterByKeywords (List<Map<String, Object>> data, Predicate<Map<String, Object>> predicate) {
		return data.stream()
				.filter(predicate)
				.collect(Collectors.<Map<String, Object>>toList());
	}
	
	public Object queryActivityDetail(String activityId) {
		
		String sqlDetail = sqlTemplate.getResource("activity_detail.sql");
		
		org.hibernate.query.Query query = this.apiDatasource.createNativeQuery(sqlDetail)
				.unwrap(org.hibernate.query.Query.class)
				.setResultTransformer(Transformers.ALIAS_TO_ENTITY_MAP)
				.setParameter("activityId", activityId);
		List<Map<String, Object>> data = ObjectUtil.columnToCamelCase(query.getResultList());
		
		Map<String, Object> res = data.stream().findFirst().get();
		
		return res;
	}
	
	public Object queryActivityDetailList(String activityId, String keywords, int curPage) {
		
		String sqlDetailList = sqlTemplate.getResource("activity_detail_list.sql");
		int firstResult = curPage * PAGE_SIZE;
		int lastResult = firstResult+PAGE_SIZE;
		org.hibernate.query.Query queryList = this.apiDatasource.createNativeQuery(sqlDetailList)
				.unwrap(org.hibernate.query.Query.class)
				.setResultTransformer(Transformers.ALIAS_TO_ENTITY_MAP)
				.setParameter("activityId", activityId);
		List<Map<String, Object>> data = ObjectUtil.columnToCamelCase(queryList.getResultList());
		
		if (StringUtils.hasLength(keywords)) {
			data = filterByKeywords(data, searchByKeywords(keywords));
		}
		
		List<Map<String, Object>> result = new ArrayList<Map<String, Object>>();
		for(int i=firstResult; i< (lastResult<=data.size()?lastResult:data.size()); i++) {
			Map<String, Object> m = data.get(i);
			m.put("id", m.get("id").toString().substring(0,10)+"XXX");
			result.add(m);
		}
		
		Map<String, Object> response = new HashMap<String, Object>();
		response.put("detail", result);
		response.put("totalElements", data.size());
		response.put("totalPage", data.size()/PAGE_SIZE);
		response.put("pageSize", PAGE_SIZE);
		
		return response;
	}
	
	public Object queryProvinceList() {
		
		String sqlDetail = sqlTemplate.getResource("get_province.sql");
		
		org.hibernate.query.Query query = this.apiDatasource.createNativeQuery(sqlDetail)
				.unwrap(org.hibernate.query.Query.class)
				.setResultTransformer(Transformers.ALIAS_TO_ENTITY_MAP);
		List<Map<String, Object>> data = ObjectUtil.columnToCamelCase(query.getResultList());
		
		return data;
	}
	
	public Object queryProvinceWAllList() {
		
		String sqlDetail = sqlTemplate.getResource("list_province_w_all.sql");
		
		org.hibernate.query.Query query = this.apiDatasource.createNativeQuery(sqlDetail)
				.unwrap(org.hibernate.query.Query.class)
				.setResultTransformer(Transformers.ALIAS_TO_ENTITY_MAP);
		List<Map<String, Object>> data = ObjectUtil.columnToCamelCase(query.getResultList());
		
		return data;
	}
	
	public Object queryActivityList() {
		
		String sqlDetail = sqlTemplate.getResource("get_activity_type.sql");
		
		org.hibernate.query.Query query = this.apiDatasource.createNativeQuery(sqlDetail)
				.unwrap(org.hibernate.query.Query.class)
				.setResultTransformer(Transformers.ALIAS_TO_ENTITY_MAP);
		List<Map<String, Object>> data = ObjectUtil.columnToCamelCase(query.getResultList());
		
		return data;
	}
}
