package com.b3.web.agent.api.dd.repository;

import java.util.List;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.b3.web.agent.api.dd.entity.ApiRole;
import com.b3.web.agent.api.dd.entity.ApiUserRoles;

@Repository
public interface UserRolesRepository extends CrudRepository<ApiUserRoles, Long> {
}