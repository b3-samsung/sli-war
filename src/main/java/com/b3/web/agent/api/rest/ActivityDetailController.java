package com.b3.web.agent.api.rest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.b3.web.agent.api.service.ActivityDetailService;
import com.b3.web.agent.api.service.ActivityListService;

@Validated
@RestController
@RequestMapping("${api.prefix}/activity")
public class ActivityDetailController {

	@Autowired
	private ActivityDetailService ActivityDetailService;
	
	@Autowired
	private ActivityListService ActivityListService;
	
	@GetMapping("/activity-list")
	public ResponseEntity activityList(
			@RequestParam (name="activityType", required=true) String activityType
			, @RequestParam (name="provinceId", required=true) String provinceId
			, @RequestParam (name="keywords", required=true) String keywords
			, @RequestParam (name="curPage", required=true) int curPage
			, @RequestParam (name="sortingType", required=true) String sortingType
			, @RequestParam (name="webRole", required=true) String webRole
	) {
		return ResponseEntity.ok(ActivityListService.queryActivityList(
				activityType
				, provinceId
				, keywords
				, curPage
				, sortingType
				,webRole
				)
			);
	}
	
    @GetMapping("activity-sub-type-list")
	public ResponseEntity<?> getActivitySubType( @RequestParam("activityType") String activityType) {
		return ResponseEntity.ok(ActivityListService.getActivitySubType(activityType));
	}
    
    
	@GetMapping("/activity-info-by-id")
	public ResponseEntity getActivityInfoById(
			@RequestParam (name="activityId", required=true) String activityId
	) {
		return ResponseEntity.ok(ActivityListService.queryActivityListById(activityId));
	}
	
	@GetMapping("/activity-detail")
	public ResponseEntity getActivityDetail(
			@RequestParam (name="activityId", required=true) String activityId
	) {
		return ResponseEntity.ok(ActivityDetailService.queryActivityDetail(activityId));
	}
	
	@GetMapping("/activity-detail-list")
	public ResponseEntity getActivityDetailList(
			@RequestParam (name="activityId", required=true) String activityId
			, @RequestParam (name="keywords", required=true) String keywords
			, @RequestParam (name="curPage", required=true) Integer curPage
	) {
		return ResponseEntity.ok(ActivityDetailService.queryActivityDetailList(
				activityId
				, keywords
				, curPage
				)
			);
	}
	
	@GetMapping("/province-list")
	public ResponseEntity getProvinceList() {
		return ResponseEntity.ok(ActivityDetailService.queryProvinceList());
	}
	
	@GetMapping("/province-list-w-all")
	public ResponseEntity getProvinceListWAll() {
		return ResponseEntity.ok(ActivityDetailService.queryProvinceWAllList());
	}
	
	@GetMapping("/activity-type-list")
	public ResponseEntity getActivityList() {
		return ResponseEntity.ok(ActivityDetailService.queryActivityList());
	}
}