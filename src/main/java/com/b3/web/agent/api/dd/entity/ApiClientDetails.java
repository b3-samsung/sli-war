/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.b3.web.agent.api.dd.entity;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Lob;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.UniqueConstraint;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author user
 */
@Entity
@Table(name = "API_CLIENT_DETAILS", catalog = "", schema = "", uniqueConstraints = {
    @UniqueConstraint(columnNames = {"API_KEY"})
    , @UniqueConstraint(columnNames = {"CLIENT_ID"})})
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "ApiClientDetails.findAll", query = "SELECT c FROM ApiClientDetails c")
    , @NamedQuery(name = "ApiClientDetails.findById", query = "SELECT c FROM ApiClientDetails c WHERE c.id = :id")
    , @NamedQuery(name = "ApiClientDetails.findByClientId", query = "SELECT c FROM ApiClientDetails c WHERE c.clientId = :clientId")
    , @NamedQuery(name = "ApiClientDetails.findByExpiredDate", query = "SELECT c FROM ApiClientDetails c WHERE c.expiredDate = :expiredDate")
    , @NamedQuery(name = "ApiClientDetails.findByCreatedDate", query = "SELECT c FROM ApiClientDetails c WHERE c.createdDate = :createdDate")
    , @NamedQuery(name = "ApiClientDetails.findByCreatedBy", query = "SELECT c FROM ApiClientDetails c WHERE c.createdBy = :createdBy")
    , @NamedQuery(name = "ApiClientDetails.findByUpdatedDate", query = "SELECT c FROM ApiClientDetails c WHERE c.updatedDate = :updatedDate")
    , @NamedQuery(name = "ApiClientDetails.findByUpdatedBy", query = "SELECT c FROM ApiClientDetails c WHERE c.updatedBy = :updatedBy")
    , @NamedQuery(name = "ApiClientDetails.findByIsDelete", query = "SELECT c FROM ApiClientDetails c WHERE c.isDelete = :isDelete")})
public class ApiClientDetails extends StandardField<Long> implements Serializable {

    private static final long serialVersionUID = 1L;
   
    @Basic(optional = false)
    @Lob
    @Column(name = "API_KEY", nullable = false)
    private String apiKey;
    @Basic(optional = false)
    @Column(name = "CLIENT_ID", nullable = false, length = 500)
    private String clientId;
    @Column(name = "EXPIRED_DATE")
    @Temporal(TemporalType.TIMESTAMP)
    private Date expiredDate;

    public ApiClientDetails() {
    }

    public ApiClientDetails(Long id) {
        this.id = id;
    }

    public ApiClientDetails(Long id, String apiKey, String clientId, Date createdDate, Character isDelete) {
        this.id = id;
        this.apiKey = apiKey;
        this.clientId = clientId;
        this.createdDate = createdDate;
        this.isDelete = isDelete;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getApiKey() {
        return apiKey;
    }

    public void setApiKey(String apiKey) {
        this.apiKey = apiKey;
    }

    public String getClientId() {
        return clientId;
    }

    public void setClientId(String clientId) {
        this.clientId = clientId;
    }

    public Date getExpiredDate() {
        return expiredDate;
    }

    public void setExpiredDate(Date expiredDate) {
        this.expiredDate = expiredDate;
    }


    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof ApiClientDetails)) {
            return false;
        }
        ApiClientDetails other = (ApiClientDetails) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

	@Override
	public String toString() {
		return "ClientDetails [apiKey=" + apiKey + ", clientId=" + clientId + ", expiredDate=" + expiredDate + ", id="
				+ id + ", createdBy=" + createdBy + ", createdDate=" + createdDate + ", updatedBy=" + updatedBy
				+ ", updatedDate=" + updatedDate + ", isDelete=" + isDelete + "]";
	}


 
    
}
