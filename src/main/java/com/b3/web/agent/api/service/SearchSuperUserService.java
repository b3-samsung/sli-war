package com.b3.web.agent.api.service;

import java.util.List;
import java.util.Map;

import org.hibernate.transform.Transformers;
import org.springframework.data.jdbc.repository.query.Modifying;
import org.springframework.stereotype.Service;

@Service
public class SearchSuperUserService  extends AbstractApiService{

	
	public Object searchPolicy(String username) {
		String sql = sqlTemplate.getResource("super_users.sql");
		
		org.hibernate.query.Query query = this.ddDatasource.createNativeQuery(sql).unwrap(org.hibernate.query.Query.class)
				.setResultTransformer(
						Transformers.ALIAS_TO_ENTITY_MAP
						)
				.setParameter("username", username);

		List<Map<String, Object>> data = query.getResultList();

		return data;
	}
	

	@Modifying
	public void updatehPolicy(String username) {
		String sql = sqlTemplate.getResource("super_users.sql");
		
		org.hibernate.query.Query query = this.ddDatasource.createNativeQuery(sql).unwrap(org.hibernate.query.Query.class)
				.setResultTransformer(
						Transformers.ALIAS_TO_ENTITY_MAP
						)
				.setParameter("username", username);

		query.executeUpdate();

	}
	
	
}
