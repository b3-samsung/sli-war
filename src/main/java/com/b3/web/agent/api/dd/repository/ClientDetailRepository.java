package com.b3.web.agent.api.dd.repository;

import org.springframework.data.repository.CrudRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;
import org.springframework.stereotype.Repository;

import com.b3.web.agent.api.dd.entity.ApiClientDetails;

@RepositoryRestResource
public interface ClientDetailRepository extends CrudRepository<ApiClientDetails, Long> {
	
}