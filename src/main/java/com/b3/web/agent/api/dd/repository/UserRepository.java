package com.b3.web.agent.api.dd.repository;

import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.b3.web.agent.api.dd.entity.ApiUser;

@Repository
public interface UserRepository extends CrudRepository<ApiUser, Long> {
	ApiUser findByUsername(@Param("username") String username);
}