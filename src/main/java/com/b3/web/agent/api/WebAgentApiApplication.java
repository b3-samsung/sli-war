package com.b3.web.agent.api;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.boot.web.servlet.support.SpringBootServletInitializer;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Lazy;
import org.springframework.web.servlet.config.annotation.ResourceHandlerRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

import com.fasterxml.jackson.databind.ObjectMapper;

import nz.net.ultraq.thymeleaf.LayoutDialect;

@Lazy(true)
@SpringBootApplication(scanBasePackages = "com.b3.web.agent.api")
public class WebAgentApiApplication extends SpringBootServletInitializer implements WebMvcConfigurer {
	@Value("${fileupload-prefix-path}")
	private String fileuploadPrefixPath;

	public static void main(String... args) {
		SpringApplication.run(WebAgentApiApplication.class, args);
	}

	@Override
	protected SpringApplicationBuilder configure(SpringApplicationBuilder application) {
		return application.sources(WebAgentApiApplication.class);
	}

	public void addResourceHandlers(final ResourceHandlerRegistry registry) {
		String path = "file:";
		if (!("" + fileuploadPrefixPath).endsWith("/")) {
			path += (fileuploadPrefixPath + "/");
		} else {
			path += fileuploadPrefixPath;
		}
		registry.addResourceHandler("/resources/e-register/images/photo/**","/**/e-register/images/photo/**/*", "/e-register/images/photo/**/*").addResourceLocations(path);
//		registry.addResourceHandler().addResourceLocations(path);
	}

	@Bean
	public ObjectMapper objectMapper() {
		return new ObjectMapper();
	}

	@Bean
	public LayoutDialect layoutDialect() {
		return new LayoutDialect();
	}

}
