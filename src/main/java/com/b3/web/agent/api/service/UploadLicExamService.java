package com.b3.web.agent.api.service;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.text.DecimalFormat;
import java.text.ParseException;
import java.util.Base64;
import java.util.HashMap;
import java.util.Map;

import javax.persistence.Query;
import javax.transaction.Transactional;

import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.CellType;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.ss.usermodel.WorkbookFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.config.ConfigurableBeanFactory;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;


import com.b3.web.agent.api.model.ExcelUploadModel;
import com.b3.web.agent.api.service.UpdateLicActRegisService;

@Component
@org.springframework.context.annotation.Scope(value = ConfigurableBeanFactory.SCOPE_PROTOTYPE)
@Service
public class UploadLicExamService extends AbstractApiService {
	
	
	@Autowired
	private UpdateLicActRegisService UpdateLicActRegisService;
	
	
	private static final Logger log = LoggerFactory.getLogger(UploadLicExamService.class);
	private static final  String[] DATA_COLUMN = { 
			"YEAR",
			"MONTH", 
			"DAY",
			"SUB2ID",
		    "EXAMT",
		    "EXAMP", 
		    "EXAMPV", 
		    "REGID", 
		    "TITLE", 
		    "FNAME", 
		    "LNAME", 
		    "CID", 
		    "EXAMTYPE",
		   	"EXAMTYPE1",
		    "RESULT",
		    "COMPANY",
		    "REGISTER_ID",
		    "ACTIVITY_ID",
		    "INSERT_TIME",
		    "UPDATE_TIME",
		    "INSERT_BY",
		    "UPDATE_BY"
	};
	private final static DecimalFormat DF = new DecimalFormat("0");
	
	private int FIRST_ROW = 0;
	private int SHEET_DATA = 0;
	
	private int uploadToLicExam = 0;
	//private String updateBy = "";
	
	public void importFile(ExcelUploadModel file) throws IOException {
		InputStream is = null;
		Boolean isUploadActivityStatus = false;
		try {
			log.info("Start import activity file: " + file.getFilename() );
			
			is = new ByteArrayInputStream(decodeString(file.getData()));
			
			Workbook workbooks = WorkbookFactory.create(is);
			Sheet sheet = workbooks.getSheetAt(SHEET_DATA);
			log.info("Start read activity data row" );
			readRow(sheet);
			workbooks.close();
		} 
		catch (IOException e) {
			log.error("Upload Activity File Error: " + e.getMessage());
			throw new IOException(e);
		} 
		finally {
			try {
				is.close();
			} catch (Exception e2) {
				log.error(e2.getMessage());
				throw new IOException(e2);
			}
		}
	}
	
	private byte[] decodeString( String urlBase64) {
		String[] data = urlBase64.split("base64,");
		return Base64.getDecoder().decode(data[1]);
	}
	

	private void readRow(Sheet sheet) throws IOException {
		log.info("ROW NUM: " + sheet.getLastRowNum());
		Boolean uploadStatus = false;
		for (int r = FIRST_ROW + 1; r <= sheet.getLastRowNum(); r++) {
			log.info("# " + r + " READING Activity data...");
			Row row = sheet.getRow(r);
			Map<String, String> dataRow = readCellConfigDataAndInsert(row);
			//log.info("UPDATE " + dataRow.get("ACTIVITY_TYPE") + " = " + this.uploadToLicExam);
			insertByRow(dataRow);
			
			//updateActRegisService.setActRegis(dataRow);
			UpdateLicActRegisService.setActRegis(dataRow);
		}
	}
	

	private Map<String, String> readCellConfigDataAndInsert(Row row) {
		Map<String, String> data = new HashMap<String, String>();
		for (int i = 0; i < DATA_COLUMN.length; i++) {
			String value = toCellString(row.getCell(i));
			data.put(DATA_COLUMN[i], value);
			
		}
		
		return data;
	}
	
	
	private String toCellString(Cell cell) {
		if (cell == null)
			return "";
		Object o = null;
		CellType type = cell.getCellType();
		if (type == CellType.STRING) {
			o = cell.getRichStringCellValue();
		} else if (type == CellType.NUMERIC) {
			o = DF.format(cell.getNumericCellValue());
		} else if (type == CellType.BOOLEAN) {
			o = cell.getBooleanCellValue();
		} else if (type == CellType.BLANK) {
			o = "";
		} else {
			o = new String("");
		}
		return String.format("%s", o.toString());
	}
	
	
	@Transactional
	public void insertByRow(Map<String, String> data) throws IOException {
		String sqlInsertLicExam = sqlTemplate.getResource("update_insert_lic_exam.sql");
        log.info("DATA.ROW: " + data);
        log.info("ACT.Year: " + data.get("YEAR"));
        log.info("ACT.CID: " + data.get("CID"));
		try {
			java.sql.Date stampDate = new java.sql.Date(new java.util.Date().getTime());
			Query query = this.apiDatasource.createNativeQuery(sqlInsertLicExam)
					.setParameter("year", Integer.parseInt(data.get("YEAR")))
					.setParameter("month", Integer.parseInt(data.get("MONTH")))
					.setParameter("day", Integer.parseInt(data.get("DAY")))
					.setParameter("sub2Id", Integer.parseInt(data.get("SUB2ID")))
					.setParameter("examt", Integer.parseInt(data.get("EXAMT")))
					.setParameter("examp", Integer.parseInt(data.get("EXAMP")))
					.setParameter("examPv", Integer.parseInt(data.get("EXAMPV")))
					.setParameter("regid", Integer.parseInt(data.get("REGID")))
					.setParameter("title", data.get("TITLE"))
					.setParameter("fname", data.get("FNAME")) 
					.setParameter("lname", data.get("LNAME")) 
					.setParameter("cid", data.get("CID"))
					.setParameter("examtype", data.get("EXAMTYPE"))
					.setParameter("examtype1", data.get("EXAMTYPE1"))
					.setParameter("result", data.get("RESULT"))
					.setParameter("company", Integer.parseInt(data.get("COMPANY")))
					.setParameter("registerId", Integer.parseInt(data.get("REGISTER_ID"))) 
					.setParameter("activityId", Integer.parseInt(data.get("ACTIVITY_ID")))
					.setParameter("insertTime", stampDate)
					.setParameter("updateTime", stampDate)
					.setParameter("insertBy", Integer.parseInt(data.get("INSERT_BY")))
					.setParameter("updateBy", Integer.parseInt(data.get("UPDATE_BY"))); 
			
			int rowAffect = query.executeUpdate();
			if (rowAffect > 0) {
				log.info("INSERT LicExam.EXCEL SUCCESS!");
			} else { 
				log.error("INSERT LicExam.EXCEL FAILED, no row affect");
			}
		} catch (Exception e) {
			log.error("INSERT LicExam.EXCEL FAILED: " + e);
			throw new IOException(e);
		}
	}
	
}
