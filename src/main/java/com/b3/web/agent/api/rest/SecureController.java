package com.b3.web.agent.api.rest;
/**
 * Copyright 2019 Greg Whitaker
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("${api.prefix}")
public class SecureController extends AbstractRestApi{
    private static final Logger LOG = LogManager.getLogger(SecureController.class);
    @GetMapping("/v1/secure")
    public ResponseEntity secure() throws NoSuchMethodException, SecurityException, NoSuchFieldException {
    	

    	LOG.info(super.urlName());
        LOG.info("Received request: " + urlName());
        return ResponseEntity.ok().build();
    }
}