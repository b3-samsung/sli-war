package com.b3.web.agent.api.rest;

import java.util.HashMap;
import java.util.Map;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.b3.web.agent.api.service.AgentApiService;


@RestController
@RequestMapping("${api.prefix}/agent")
public class AgentController {

	private Logger log = LogManager.getLogger(AgentController.class);
	
	@Autowired
	private AgentApiService agentApiService;

	@GetMapping("/search-agent")
	public ResponseEntity<Object> searchAgentApi(
			@RequestParam("agentCode") String agentCode) {
		return ResponseEntity.ok(agentApiService.searchAgent(agentCode, agentCode));
	}

	@GetMapping("/search-recommender-agent")
	public ResponseEntity<Object> searchRecommenderAgentApi(
			@RequestParam("agentCode") String agentCode) {
		return ResponseEntity.ok(agentApiService.searchRecommenderAgent(agentCode));
	}
	
	@GetMapping("/search-agent-by-pid")
	public ResponseEntity searchAgentByPid(
			  @RequestParam("pid") String pid
			, @RequestParam("activityId") String activityId
			, @RequestParam("activityType") String activityType) {
		
		
		String agentCode = agentApiService.searchAgentCode(pid);

		Map<String, Object> response = new HashMap<String, Object>();
		
		response.put("agentDetail", agentApiService.searchAgent(agentCode, agentCode, Boolean.TRUE.toString()));
		response.put("repeat", agentApiService.searchIsRepeat(pid, activityType));
		response.put("agentRegis", agentApiService.searchAgentInActivity(pid, activityId));
		response.put("alreadyRegis", agentApiService.searchIsRegister(pid, activityId));
		
		return ResponseEntity.ok(response);
	}

	@GetMapping("/search-team")
	public ResponseEntity<Object> searchTeamApi(
			@RequestParam("leadAgentCode") String leadAgentCode,
			@RequestParam("agentCode") String agentCode) {
		return ResponseEntity.ok(agentApiService.searchAgent(leadAgentCode, agentCode));
	}
	
	@GetMapping("/check-payin-num")
	public ResponseEntity<Object> checkPayinNum(
			@RequestParam("payinNum") String payinNum
			, @RequestParam("activityId") String activityId
			, @RequestParam("registerId") String registerId
			, @RequestParam("isAdmin") String isAdmin) {
		if (isAdmin.equalsIgnoreCase("N") || StringUtils.hasLength(registerId)) {
			return ResponseEntity.ok(agentApiService.searchIsPayinNumByAgent(payinNum));
		}
		else {
			return ResponseEntity.ok(agentApiService.searchIsPayinNumByAdmin(payinNum, activityId, registerId));
		}
	}
	
	@GetMapping("get-memo-oic")
	public ResponseEntity<?> getMemoOic(@RequestParam("agentCode") String agentCode){
		return ResponseEntity.ok(agentApiService.getMemoOic(agentCode));		
	}
	@GetMapping("get-memo-agent")
	public ResponseEntity<?> getMemoAgent(@RequestParam("agentCode") String agentCode){
		return ResponseEntity.ok(agentApiService.getMemoAgent(agentCode));		
	}
}
