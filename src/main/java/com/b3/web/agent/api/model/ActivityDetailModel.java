package com.b3.web.agent.api.model;

public class ActivityDetailModel {

	private String activityDate;
	private Integer activityId;
	private String activityName;
	private String activityRegisFrom;
	private String activityRegisTo;
	private String activityRegisToTime;
	private Integer activityType;
	private Integer activitySubType;
	private String endTime;
	private String location;
	private Integer maxCapa;
	private Integer numOfDays;
	private String province;
	private Integer roomCapa;
	private String startTime;
	private String delIndi;
	private String activityDateTo;
	private String remark;
	
	
	public String getRemark() {
		return remark;
	}
	public void setRemark(String remark) {
		this.remark = remark;
	}
	public Integer getActivityId() {
		return activityId;
	}
	public void setActivityId(Integer activityId) {
		this.activityId = activityId;
	}
	public Integer getActivityType() {
		return activityType;
	}
	public void setActivityType(Integer activityType) {
		this.activityType = activityType;
	}
	public String getProvince() {
		return province;
	}
	public void setProvince(String province) {
		this.province = province;
	}
	public String getActivityName() {
		return activityName;
	}
	public void setActivityName(String acitivityName) {
		this.activityName = acitivityName;
	}
	public String getActivityDate() {
		return activityDate;
	}
	public void setActivityDate(String activityString) {
		this.activityDate = activityString;
	}
	public String getLocation() {
		return location;
	}
	public void setLocation(String location) {
		this.location = location;
	}
	public Integer getNumOfDays() {
		return numOfDays;
	}
	public void setNumOfDays(Integer numOfDays) {
		this.numOfDays = numOfDays;
	}
	public String getStartTime() {
		return startTime;
	}
	public void setStartTime(String startTime) {
		this.startTime = startTime;
	}
	public String getEndTime() {
		return endTime;
	}
	public void setEndTime(String endTime) {
		this.endTime = endTime;
	}
	public String getActivityRegisFrom() {
		return activityRegisFrom;
	}
	public void setActivityRegisFrom(String activityRegisFrom) {
		this.activityRegisFrom = activityRegisFrom;
	}
	public String getActivityRegisTo() {
		return activityRegisTo;
	}
	public void setActivityRegisTo(String activityRegisTo) {
		this.activityRegisTo = activityRegisTo;
	}
	public Integer getMaxCapa() {
		return maxCapa;
	}
	public void setMaxCapa(Integer maxCapa) {
		this.maxCapa = maxCapa;
	}
	public Integer getRoomCapa() {
		return roomCapa;
	}
	public void setRoomCapa(Integer roomCapa) {
		this.roomCapa = roomCapa;
	}
	public String getActivityRegisToTime() {
		return activityRegisToTime;
	}
	public void setActivityRegisToTime(String activityRegisToTime) {
		this.activityRegisToTime = activityRegisToTime;
	}
	public Integer getActivitySubType() {
		return activitySubType;
	}
	public void setActivitySubType(Integer activitySubType) {
		this.activitySubType = activitySubType;
	}
	public String getDelIndi() {
		return delIndi;
	}
	public void setDelIndi(String delIndi) {
		this.delIndi = delIndi;
	}
	public String getActivityDateTo() {
		return activityDateTo;
	}
	public void setActivityDateTo(String activityDateTo) {
		this.activityDateTo = activityDateTo;
	}
}
