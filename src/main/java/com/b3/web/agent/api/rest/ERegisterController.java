package com.b3.web.agent.api.rest;

import java.io.IOException;
import java.math.BigDecimal;
import java.nio.file.Path;
import java.util.Arrays;
import java.util.List;


import org.hibernate.ResourceClosedException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.b3.web.agent.api.model.RequestModel;
import com.b3.web.agent.api.service.ERegisterService;
import com.b3.web.agent.api.service.FileStorageService;

@RestController
@RequestMapping("${api.prefix}/register")
public class ERegisterController {

    private static final Logger log = LoggerFactory.getLogger(ERegisterController.class);

    @Autowired
    private FileStorageService fileStorageService;
    @Value("${fileupload-prefix-path}")
    private  String FILE_PATH;
    
    @Autowired
    private ERegisterService eRegisterService;
    
    @GetMapping("get-bank-master")
	public ResponseEntity<?> getBankMaster() {
		return ResponseEntity.ok(eRegisterService.bankListMaster());
	}
    
    @GetMapping("get-education-master")
	public ResponseEntity<?> getEducationMaster() {
		return ResponseEntity.ok(eRegisterService.getEducationMaster());
	}
    
    @GetMapping("get-gender-master")
	public ResponseEntity<?> getGenderMaster() {
		return ResponseEntity.ok(eRegisterService.genderListMaster());
	}
    
    @GetMapping("get-title-master")
	public ResponseEntity<?> getTitleMaster() {
		return ResponseEntity.ok(eRegisterService.titleListMaster());
	}
    
	@PostMapping("register-activity")
	public ResponseEntity<?> eRegister(@RequestBody RequestModel request) 
			throws ResourceClosedException {
		// Set RegisterId as Running No. (by query from count + 1)
		BigDecimal registerId = eRegisterService.nextRegisterIdQuery(request.getActivityId());
	
		// Set TimeStamp for insert&update Time
		java.sql.Date insertTime = new java.sql.Date(new java.util.Date().getTime());
		java.sql.Date updateTime = new java.sql.Date(new java.util.Date().getTime());
		
		// Check and Set ImagePath
		String pictureLocal = null, payinLocal = null, attendanceResult = request.getAttendanceResult(), examResult = "N";
		if (StringUtils.hasLength(request.getPictureLocal())) {
			pictureLocal = request.getPictureLocal();
		}
		
		if (StringUtils.hasLength(request.getPayinLocal())) {
			payinLocal = request.getPayinLocal();
		}

		return ResponseEntity.ok(eRegisterService.eRegisterQuery(
				Integer.valueOf(registerId.intValueExact()), request.getRegisterType(), request.getActivityId()
				, request.getId(), request.getTitle(), request.getName()
				, request.getSurname(), request.getGender(), request.getAge(), request.getDob()
				, request.getAddress1(), request.getAddress2(), request.getAddress3()
				, request.getAddress4(), request.getAddress5()
				, request.getZip(), request.getMobile(), request.getEmail(), request.getLine()
				, request.getRecommenderCode()
				, attendanceResult, examResult
				, payinLocal
				, pictureLocal
				, request.getIdcardLocal()
				, insertTime, updateTime
				, request.getInsertBy(), request.getUpdateBy()
				, request.getAgentCode()
				, request.getBankCode()
				, request.getPayinNum(), request.getPayinAmount(), request.getPayinDate()
				, request.getPayinTime(), request.getPayinRef()
				, request.getEducationLevel()
			));
	}
	
	@GetMapping("flag-to-delete")
	public ResponseEntity<?> flagDelete(			
			@RequestParam (name="activityId", required=true) Integer activityId ,
			@RequestParam (name="registerId", required=true) Integer registerId ,
			@RequestParam (name="editor", required=true) String editor){
		return ResponseEntity.ok(eRegisterService.flagDeleteERegister(editor, activityId, registerId));
	}
	
	
	@PostMapping("update-agent-activity")
	public ResponseEntity<?> updateAgentERegister(@RequestBody RequestModel request) 
			throws ResourceClosedException {
		
		// Set TimeStamp for updateTime
		java.sql.Date updateTime = new java.sql.Date(new java.util.Date().getTime());

		return ResponseEntity.ok(eRegisterService.attendanceByAdminQuery(
				  request.getRegisterId()
				, request.getRegisterType()
				, request.getActivityId()
				, request.getId(), request.getTitle(), request.getName()
				, request.getSurname(), request.getGender(), request.getAge(), request.getDob()
				, request.getAddress1(), request.getAddress2(), request.getAddress3()
				, request.getAddress4(), request.getAddress5()
				, request.getZip(), request.getMobile(), request.getEmail(), request.getLine()
				, request.getRecommenderCode()
				, request.getPayinLocal(), request.getPictureLocal(), request.getIdcardLocal()
				, request.getAttendanceResult()
				, updateTime
				, request.getUpdateBy()
				, request.getBankCode()
				, request.getPayinNum(), request.getPayinAmount(), request.getPayinDate()
				, request.getPayinTime(), request.getPayinRef()
				, request.getEducationLevel()
			));
	}
	
	@PostMapping("upload-file")
	public ResponseEntity uploadFile(@RequestBody RequestModel request) throws IOException {
		try {
			String fileBase64 = request.getFileBased64();
			String fileType = request.getFileType();
			String fileName = request.getFileName();
			Long fileSize = request.getFileSize();
			
//			log.info("Upload file: " + fileName);
//			log.info("Upload first 20 byte data: " + fileBase64.substring(0, 90) + "...");
//			log.info("Upload file type: " + fileType);
//			log.info("Upload file size: " + fileSize/1024 + "kb");
			

			Path path = fileStorageService.writeFile(FILE_PATH, fileName, fileBase64);
			return ResponseEntity.ok().body(path);
			
		}catch (Exception e) {
		e.printStackTrace();
			log.error(e.getLocalizedMessage());
			ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(e.getLocalizedMessage());
			List<String> messages = Arrays.asList("failed");
			return ResponseEntity.ok().body(messages);
		}
	} 
}