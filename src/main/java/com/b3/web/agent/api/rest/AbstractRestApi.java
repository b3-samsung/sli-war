package com.b3.web.agent.api.rest;

import java.lang.reflect.Method;

import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PatchMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestMapping;

public abstract class AbstractRestApi {

	private static Class[] clazz = { RequestMapping.class, GetMapping.class, PostMapping.class, PutMapping.class,
			DeleteMapping.class, PatchMapping.class };

	protected AbstractRestApi() {

	}

	protected String urlName()  {
		String[] values = null;
		Object o = null;

		try {
			Method method  = getClass().getDeclaredMethod(Thread.currentThread().getStackTrace()[2].getMethodName(), null);
			if ((o = method.getAnnotation(GetMapping.class)) != null) {
				values = ((GetMapping) o).value();
			} else if ((o = method.getAnnotation(RequestMapping.class)) != null) {
				values = ((GetMapping) o).value();
			} else if ((o = method.getAnnotation(PostMapping.class)) != null) {
				values = ((PostMapping) o).value();
			} else if ((o = method.getAnnotation(PutMapping.class)) != null) {
				values = ((PutMapping) o).value();
			} else if ((o = method.getAnnotation(DeleteMapping.class)) != null) {
				values = ((DeleteMapping) o).value();
			} else if ((o = method.getAnnotation(PatchMapping.class)) != null) {
				values = ((PatchMapping) o).value();
			} else {
				values = new String[] {};
			}
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			values = new String[] {};
		}


		return String.join(",", values);
	}

}
