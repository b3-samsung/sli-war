package com.b3.web.agent.api.rest;

import java.io.IOException;
import java.util.Arrays;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.b3.web.agent.api.model.ExcelUploadModel;
import com.b3.web.agent.api.model.RequestModel;
import com.b3.web.agent.api.service.UploadLicExamService;
import com.b3.web.agent.api.service.UpdateLicActRegisService;
import com.b3.web.agent.api.service.UploadExcelHistoryService;

@RestController
@RequestMapping("${api.prefix}/agent")
public class UploadLicExam {

	private static final Logger log = LoggerFactory.getLogger(UploadLicExam.class);
	
	@Autowired
	private UploadLicExamService UploadLicExamService;
	
	@Autowired
	private UploadExcelHistoryService UploadExcelHistoryService;
    
    @PostMapping("upload-LICExam")
    public ResponseEntity ExcelLicExam(@RequestBody RequestModel request) throws IOException {
		try {
			String fileBase64 = request.getFileBased64();
			String fileType = request.getFileType();
			String fileName = request.getFileName();
			Long fileSize = request.getFileSize();
			String updateBy = request.getUpdateBy();
			
			log.info("Upload activity file name: " + fileName);
			log.info("Upload activity file type: " + fileType);
			log.info("Upload activity file size: " + fileSize/1024 + "kb");
			
			ExcelUploadModel f = new ExcelUploadModel();
			f.setData(fileBase64);
			f.setExtension(fileType);
			f.setFilename(fileName);
			f.setSize(fileSize);
			
			UploadLicExamService.importFile(f);
			
			// Insert History
			UploadExcelHistoryService.insertExcelHistoryByType(f, "LIC-EXAM", updateBy);

			List<String> messages = Arrays.asList("success");
			return ResponseEntity.ok().body(messages);
			
		}catch (Exception e) {
			log.error(e.getLocalizedMessage());
			e.printStackTrace();
			ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(e.getLocalizedMessage());
			List<String> messages = Arrays.asList("failed");
			return ResponseEntity.ok().body(messages);
		}
	}


}

