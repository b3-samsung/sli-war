package com.b3.web.agent.api.service;


import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.NoOpPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import com.b3.web.agent.api.dd.entity.ApiRole;
import com.b3.web.agent.api.dd.entity.ApiUser;
import com.b3.web.agent.api.dd.repository.RoleRepository;
import com.b3.web.agent.api.dd.repository.UserRepository;
import com.b3.web.agent.api.dd.repository.UserRolesRepository;


@Service
public class ApiUserService {
	private static final Logger log = LogManager.getLogger(ApiUserService.class);
	private UserRolesRepository userRolesRepository;
    private UserRepository userRepository;
    private RoleRepository roleRepository;
    private PasswordEncoder passwordEncoder;

    public ApiUserService(UserRepository userRepository,
                       RoleRepository roleRepository,
                       PasswordEncoder passwordEncoder) {
        this.userRepository = userRepository;
        this.roleRepository = roleRepository;
        this.passwordEncoder = passwordEncoder;
    }

//    public List<Role> getRoles(String username) {
//        return userRolesRepository.findByUserIdUsername(username);
//    }
    public ApiUser findUserByUsername(String username) {
    	log.info( "Username: "+ username);
        return userRepository.findByUsername(username);
    }

//    public User saveUser(User user) {
//        user.setPassword(passwordEncoder.encode(user.getPassword()));
//        user.setActive(true);
//        Role userRole = roleRepository.findByRole("ADMIN");
//        user.setRoles(new HashSet<Role>(Arrays.asList(userRole)));
//        return userRepository.save(user);
//    }

}