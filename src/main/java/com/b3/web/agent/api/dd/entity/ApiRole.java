/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.b3.web.agent.api.dd.entity;

import java.io.Serializable;
import java.util.Date;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author user
 */
@Entity
@Table(name = "API_ROLE", catalog = "", schema = "", uniqueConstraints = {
    @UniqueConstraint(columnNames = {"ROLE"})})
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "ApiRole.findAll", query = "SELECT r FROM ApiRole r")
    , @NamedQuery(name = "ApiRole.findById", query = "SELECT r FROM ApiRole r WHERE r.id = :id")
    , @NamedQuery(name = "ApiRole.findByRole", query = "SELECT r FROM ApiRole r WHERE r.role = :role")
    , @NamedQuery(name = "ApiRole.findByCreatedDate", query = "SELECT r FROM ApiRole r WHERE r.createdDate = :createdDate")
    , @NamedQuery(name = "ApiRole.findByCreatedBy", query = "SELECT r FROM ApiRole r WHERE r.createdBy = :createdBy")
    , @NamedQuery(name = "ApiRole.findByUpdatedDate", query = "SELECT r FROM ApiRole r WHERE r.updatedDate = :updatedDate")
    , @NamedQuery(name = "ApiRole.findByUpdatedBy", query = "SELECT r FROM ApiRole r WHERE r.updatedBy = :updatedBy")
    , @NamedQuery(name = "ApiRole.findByIsDelete", query = "SELECT r FROM ApiRole r WHERE r.isDelete = :isDelete")})
public class ApiRole extends StandardField<Long> implements Serializable {

    private static final long serialVersionUID = 1L;

    @Basic(optional = false)
    @Column(name = "ROLE", nullable = false, length = 256)
    private String role;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "roleId", fetch = FetchType.LAZY)
    private List<ApiUserRoles> userRolesList;

    public ApiRole() {
    }

    public ApiRole(Long id) {
        this.id = id;
    }

    public ApiRole(Long id, String role, Date createdDate, Character isDelete) {
        this.id = id;
        this.role = role;
        this.createdDate = createdDate;
        this.isDelete = isDelete;
    }

    public String getRole() {
        return role;
    }

    public void setRole(String role) {
        this.role = role;
    }

    @XmlTransient
    public List<ApiUserRoles> getUserRolesList() {
        return userRolesList;
    }

    public void setUserRolesList(List<ApiUserRoles> userRolesList) {
        this.userRolesList = userRolesList;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof ApiRole)) {
            return false;
        }
        ApiRole other = (ApiRole) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.b3.web.agent.api.entity.Role[ id=" + id + " ]";
    }
    
}
