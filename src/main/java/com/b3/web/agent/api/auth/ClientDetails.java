package com.b3.web.agent.api.auth;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Map;

import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;

public class ClientDetails implements UserDetails {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private String cliendId;
	private String apiKey;
	private Collection<? extends GrantedAuthority> authorities;
	private Map<String, Object> optional;

	public ClientDetails() {
	}

	public ClientDetails(String cliendId, String apiKey) {
		super();
		this.cliendId = cliendId;
		this.apiKey = apiKey;
		this.authorities = new ArrayList<GrantedAuthority>();
	}

	public ClientDetails(String cliendId, String apiKey, Collection<? extends GrantedAuthority> authorities) {
		super();
		this.cliendId = cliendId;
		this.apiKey = apiKey;
		this.authorities = authorities;
	}

	public ClientDetails(String cliendId, String apiKey, Collection<? extends GrantedAuthority> authorities,
			Map<String, Object> optional) {
		super();
		this.cliendId = cliendId;
		this.apiKey = apiKey;
		this.authorities = authorities;
		this.optional = optional;
	}

	public Collection<? extends GrantedAuthority> getAuthorities() {
		return authorities;
	}

	public boolean isAccountNonExpired() {
		return true;
	}

	public boolean isAccountNonLocked() {
		return true;
	}

	public boolean isCredentialsNonExpired() {
		return true;
	}

	public boolean isEnabled() {
		return true;
	}

	public Map<String, Object> getOptional() {
		return optional;
	}

	public void setOptional(Map<String, Object> optional) {
		this.optional = optional;
	}

	public String getCliendId() {
		return cliendId;
	}

	public void setCliendId(String cliendId) {
		this.cliendId = cliendId;
	}

	public String getApiKey() {
		return apiKey;
	}

	public void setApiKey(String apiKey) {
		this.apiKey = apiKey;
	}

	public String getPassword() {
		return this.apiKey;
	}

	public String getUsername() {
		return this.cliendId;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((apiKey == null) ? 0 : apiKey.hashCode());
		result = prime * result + ((cliendId == null) ? 0 : cliendId.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		ClientDetails other = (ClientDetails) obj;
		if (apiKey == null) {
			if (other.apiKey != null)
				return false;
		} else if (!apiKey.equals(other.apiKey))
			return false;
		if (cliendId == null) {
			if (other.cliendId != null)
				return false;
		} else if (!cliendId.equals(other.cliendId))
			return false;

		return true;
	}

	@Override
	public String toString() {
		return "ClientDetails [cliendId=" + cliendId + ", apiKey=" + apiKey + ", authorities=" + authorities
				+ ", optional=" + optional + "]";
	}

	
}
