package com.b3.web.agent.api.rest;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;

/**
 * Copyright 2019 Greg Whitaker
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import org.springframework.http.ResponseEntity;
import org.springframework.lang.NonNull;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.b3.web.agent.api.service.ApiService;
import com.b3.web.agent.api.service.SearchSuperUserService;


@Validated
@RestController
@RequestMapping("${api.prefix}/nonsecure")
public class NonSecureController extends AbstractRestApi {
	private static final Logger LOG = LogManager.getLogger(NonSecureController.class);

	@Autowired
	private ApiService apiService;
	@Autowired
	private SearchSuperUserService searchSuperUserService;
	
	@Validated
	@GetMapping(value = "/test")
	public ResponseEntity nonsecure(
			@RequestParam   @NonNull  String agentCode,
			@RequestParam @NonNull Integer pagesize,
			@RequestParam @NonNull  Integer pageid 
	) {
		
		LOG.info("Received request:" + urlName());
		return ResponseEntity.ok(apiService.searchPolicy(agentCode, pagesize, pageid));
	
	}

	@GetMapping(value = "/searchSuperUser")
	public ResponseEntity nonsecure2(@RequestParam(name = "username") String username) {
		return ResponseEntity.ok(searchSuperUserService.searchPolicy(username));
	}
	
	
}












