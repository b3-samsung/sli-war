package com.b3.web.agent.api.service;

import java.util.List;
import java.util.Map;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.hibernate.transform.Transformers;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
@SuppressWarnings({ "deprecation", "rawtypes", "unchecked" })
public class ApiService extends AbstractApiService{

//	@Autowired
//	private SqlTemplateFactory sqlTemplate;
//	
//	@PersistenceContext(unitName="apiDatasource")
//	private EntityManager apiDatasource;
//	
//	@PersistenceContext(unitName="ddDatasource")
//	private EntityManager ddDatasource;
	
	

	public Object searchPolicy(String agentCode, int pageSize, int pageId) {
		String sql = sqlTemplate.getResource("search_policy.sql");
		
		org.hibernate.query.Query query = this.apiDatasource.createNativeQuery(sql).unwrap(org.hibernate.query.Query.class)
				.setResultTransformer(
						Transformers.ALIAS_TO_ENTITY_MAP
						)
				.setParameter("agent_code", agentCode)
				.setParameter("pagesize", pageSize)
				.setParameter("pageid", pageId);
		List<Map<String, Object>> data = query.getResultList();
		return data;
//		List<DdTClosCalendar> listModel = 	ObjectUtils.toObject(data, DdTClosCalendar.class );
		
	}
	
	
	public Object searchStructure() {
		String sql = sqlTemplate.getResource("super_users.sql");
		
		org.hibernate.query.Query query = this.ddDatasource.createNativeQuery(sql).unwrap(org.hibernate.query.Query.class)
				.setResultTransformer(
						Transformers.ALIAS_TO_ENTITY_MAP
						);
		List<Map<String, Object>> data = query.getResultList();
		return data;
//		List<DdTClosCalendar> listModel = 	ObjectUtils.toObject(data, DdTClosCalendar.class );
		
	}
}
