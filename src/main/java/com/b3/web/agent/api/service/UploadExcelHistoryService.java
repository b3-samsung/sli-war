package com.b3.web.agent.api.service;

import java.io.IOException;
import java.util.List;
import java.util.Map;

import javax.persistence.Query;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.hibernate.transform.Transformers;
import org.springframework.stereotype.Service;

import com.b3.web.agent.api.model.ExcelUploadModel;
import com.b3.web.agent.api.util.ObjectUtil;

@Service
public class UploadExcelHistoryService extends AbstractApiService {

	private static final Logger log = LogManager.getLogger(UploadExcelHistoryService.class);
	
	public void insertExcelHistoryByType(ExcelUploadModel file, String activityId, String updateBy) throws IOException {
		try {
			java.sql.Date stampDate = new java.sql.Date(new java.util.Date().getTime());
			String sql = sqlTemplate.getResource("insert_upload_history.sql");
			
			Query query = this.ddDatasource.createNativeQuery(sql)
					.setParameter("fileName", file.getFilename())
					.setParameter("header", "empty")
					.setParameter("createDate", stampDate)
					.setParameter("createBy", updateBy)
					.setParameter("updateDate", stampDate)
					.setParameter("updateBy", updateBy)
					.setParameter("activityType", activityId);
			int rowAffect = query.executeUpdate();
			if (rowAffect > 0) {
				log.info("INSERT UPLOAD.HISTORY SUCCESS!");
			} else {
				log.error("INSERT UPLOAD.HISTORY FAILED, no row affect");
			}
		} catch (Exception e) {
			log.error("INSERT UPLOAD.HISTORY FAILED: " + e.getMessage());
			throw new IOException(e);
		}
	}
	
	public Object getUploadHistoryByType(String uploadFileType) {
		String sql = sqlTemplate.getResource("get_upload_history_by_type.sql");
		
		org.hibernate.query.Query query = this.ddDatasource.createNativeQuery(sql)
				.unwrap(org.hibernate.query.Query.class)
				.setResultTransformer(Transformers.ALIAS_TO_ENTITY_MAP)
				.setParameter("fileType", uploadFileType);
		List<Map<String, Object>> data = ObjectUtil.columnToCamelCase(query.getResultList());
		
		return data;
	}
}
