package com.b3.web.agent.api.service;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.function.Predicate;
import java.util.stream.Collectors;

import org.hibernate.transform.Transformers;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import com.b3.web.agent.api.util.ObjectUtil;

@Service
public class SearchInsuranceService extends AbstractApiService {
	
	
	final int PAGE_SIZE = 25;
	
	public List<Map<String, Object>> getAgentChild( String parentAgentCode ) {
		String sql = sqlTemplate.getResource("list_agent_policy_by_level.sql");
		org.hibernate.query.Query query = this.apiDatasource.createNativeQuery(sql)
				.unwrap(org.hibernate.query.Query.class)
				.setResultTransformer(Transformers.ALIAS_TO_ENTITY_MAP)
				.setParameter("agent_code", parentAgentCode);
		List<Map<String, Object>> data = ObjectUtil.columnToCamelCase(query.getResultList());
		return data;
	}
	
	private Predicate<Map<String, Object>> searchByKeywords(
			final String agentFilter, final String policyCode, final String policyId, final String fName, final String lName, final String pid
	) {
		return new Predicate<Map<String, Object>>() {
			public boolean test(Map<String, Object> t) {
				return stringContrain(t.get("policyCode"),policyCode)
						&& stringContrain(t.get("policyId"),policyId)
						&& stringContrain(t.get("serviceAgentCode"),agentFilter)
						&& stringContrain(t.get("insuredFname"),fName)
						&& stringContrain(t.get("insuredLname"),lName)
						&& stringContrain(t.get("certiCode"),pid);
			}
		};
	}
	
	private  boolean stringContrain( Object str, String test ) {
		if(!StringUtils.hasLength(test) || "null".equals(test)|| "*".equals(test)) return true;
		if( str == null  ) str = "";
		return str.toString().contains(test) ;
		
	}
	private static List<Map<String, Object>> filterByKeywords (List<Map<String, Object>> data, Predicate<Map<String, Object>> predicate) {
		return data.stream()
				.filter(predicate)
				.collect(Collectors.<Map<String, Object>>toList());
	}
	
	public Map<String, Object> searchInsurance(String agentCode, String agentFilter, String policyCode, String policyId, String fName, String lName, String pid, Integer curPage) {
		

		String sql = sqlTemplate.getResource("search_insurance.sql");
		int firstResult = curPage * PAGE_SIZE;
		int lastIndex = firstResult+PAGE_SIZE;
		System.out.println("curPage "+curPage);
		System.out.println("firstResult " + firstResult);
		System.out.println("PAGE_SIZE " + PAGE_SIZE);
		
	
		org.hibernate.query.Query query = this.apiDatasource.createNativeQuery(sql)
				.unwrap(org.hibernate.query.Query.class)
				.setResultTransformer(Transformers.ALIAS_TO_ENTITY_MAP);
				
		if( StringUtils.hasLength(agentFilter)  && !"null".equals(agentFilter) && !"*".equals(agentFilter)){
			query.setParameter("agent_code", agentFilter);
		}else {
			query.setParameter("agent_code", agentCode);
		}

		List<Map<String, Object>> data = ObjectUtil.columnToCamelCase(query.getResultList());
		System.out.println("policyCode " + policyCode);
		System.out.println("policyId " + policyId);
		if (StringUtils.hasLength(policyCode) 
				|| StringUtils.hasLength(agentFilter) 
				|| StringUtils.hasLength(policyId)
				|| StringUtils.hasLength(fName) 
				|| StringUtils.hasLength(lName)
				|| StringUtils.hasLength(pid)) {
		
			data =  data.stream().filter(searchByKeywords(agentFilter, policyCode, (policyId), (fName), (lName), (pid))) .collect(Collectors.<Map<String, Object>>toList());

		}else {
			System.out.println("All");
		}

		
		Map<String, Object> response = new HashMap<String, Object>();
		
		response.put("detail", data.subList(firstResult, lastIndex> data.size()? data.size(): lastIndex));
		response.put("totalElements", 	data.size());
		response.put("totalPage", data.size()/PAGE_SIZE);
		response.put("pageSize", PAGE_SIZE);
		response.put("element", data.size());
		
//		response.put("detail", data);
//		response.put("totalElements", pageable.get("totalElement"));
//		response.put("totalPage", pageable.get("totalPage"));
//		response.put("pageSize", PAGE_SIZE);
//		response.put("element", data.size());

//		
		return response;
	}
	
	public List<Map<String, Object>> searchPolicyByInsuerdName(String agentCode, String filter ){
		String sql = sqlTemplate.getResource("search_policy_by_insuerd_name.sql");
		List<Map<String, Object>> list = this.apiDatasource.createNativeQuery(sql)
		.unwrap(org.hibernate.query.Query.class)
		.setResultTransformer(Transformers.ALIAS_TO_ENTITY_MAP)
		.setParameter("AGENT_CODE", agentCode)
		.setParameter("FILTER", filter)
		.getResultList();
		
		List<Map<String, Object>> data = ObjectUtil.columnToCamelCase(list);
		return data;
	}
	
	public String searchEdocBypolicy( String policyCode) {
		String sql = sqlTemplate.getResource("search_edoc_by_policy.sql");
		String rs = "";
		try{
			rs = (String) this.apiDatasource.createNativeQuery(sql)
				.setParameter("policy_code", policyCode)
				.getSingleResult();
		}catch (Exception e) {
			
		}
		
		return rs;
	}
}
