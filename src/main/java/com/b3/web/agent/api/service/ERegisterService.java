package com.b3.web.agent.api.service;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.persistence.NoResultException;
import javax.persistence.Query;
import javax.transaction.Transactional;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.hibernate.jpa.TypedParameterValue;
import org.hibernate.transform.Transformers;
import org.hibernate.type.StandardBasicTypes;
import org.springframework.stereotype.Service;

import com.b3.web.agent.api.util.ObjectUtil;

@Service
public class ERegisterService extends AbstractApiService {
	
	private static final Logger log = LogManager.getLogger(ReportService.class);
	
	public List<Map<String, Object>> bankListMaster() {
		String sql = sqlTemplate.getResource("bank-master.sql");
		org.hibernate.query.Query query = this.apiDatasource.createNativeQuery(sql)
				.unwrap(org.hibernate.query.Query.class)
				.setResultTransformer(Transformers.ALIAS_TO_ENTITY_MAP);
		List<Map<String, Object>> data = ObjectUtil.columnToCamelCase(query.getResultList());
		return data;
	}
	
	public List<Map<String, Object>> getEducationMaster() {
		String sql = sqlTemplate.getResource("get_education_master.sql");
		org.hibernate.query.Query query = this.apiDatasource.createNativeQuery(sql)
				.unwrap(org.hibernate.query.Query.class)
				.setResultTransformer(Transformers.ALIAS_TO_ENTITY_MAP);
		List<Map<String, Object>> data = ObjectUtil.columnToCamelCase(query.getResultList());
		return data;
	}
	
	public List<Map<String, String>> genderListMaster() {
		List<Map<String, String>> result = new ArrayList<Map<String, String>>();
		
		String [] code = {"M", "F"};
		String [] desc = {"ชาย", "หญิง"};
		for (int i = 0; i < code.length; i++) {
			Map<String, String> data = new HashMap<String, String>();
			data.put("code", code[i]);
			data.put("desc", desc[i]);
			result.add(data);
		}
		return result;
	}
	
	public List<Map<String, String>> titleListMaster() {
		List<Map<String, String>> result = new ArrayList<Map<String, String>>();
		
		String [] code = {"นาย", "นาง", "น.ส."};
		for (int i = 0; i < code.length; i++) {
			Map<String, String> data = new HashMap<String, String>();
			data.put("code", code[i]);
			result.add(data);
		}
		return result;
	}

	public synchronized BigDecimal nextRegisterIdQuery(Integer activityId) {
		String sql = sqlTemplate.getResource("e_regis_get_register_id.sql");
		
		try{
			org.hibernate.query.Query query = this.apiDatasource.createNativeQuery(sql)
					.unwrap(org.hibernate.query.Query.class)
					.setResultTransformer(Transformers.ALIAS_TO_ENTITY_MAP)
					.setParameter("activityId", activityId);

			Map<String, Object> data = ObjectUtil.columnToCamelCase(query.getSingleResult());

			BigDecimal rs = (BigDecimal) data.get("nextRegisterId");
			return  rs == null ?BigDecimal.ONE : rs;

		}catch (NoResultException e) {
			return  BigDecimal.ONE ;
		}catch (NullPointerException e) {
			return  BigDecimal.ONE ;
		}
	}

	public Map<String, Object> flagDeleteERegister(String editor, Integer activityId, Integer registerId) {
		String sql = sqlTemplate.getResource("update_register_id_to_delete.sql");
		Map<String, Object> response = new HashMap<String, Object>();
		try {
			org.hibernate.query.Query query = this.apiDatasource.createNativeQuery(sql)
					.unwrap(org.hibernate.query.Query.class)
			.setParameter("activityId", activityId)
			.setParameter("editor", editor)
			.setParameter("registerId", registerId);
			int rowAffect = query.executeUpdate();
			if (rowAffect > 0) {
				response.put("message", "success");
			} else {
				response.put("message", "failed");
			}
		}catch (Exception e) {
			e.printStackTrace();
			response.put("message", "failed");
		}
		return response;
	}

	@Transactional
	public Object eRegisterQuery(
			  Integer registerId
			, Integer registerType
			, Integer activityId
			, String id
			, String title
			, String name
			, String surname
			, String gender
			, Integer age
			, String dob
			, String address1
			, String address2
			, String address3
			, String address4
			, String address5
			, String zip
			, String mobile
			, String email
			, String line
			, String recommenderCode
			, String attendanceResult
			, String examResult
			, String payinLocal
			, String pictureLocal
			, String idcardLocal
			, Date insertTime
			, Date updateTime
			, String insertBy
			, String updateBy
			, String agentCode
			, String bankCode
			, String payinNum
			, Double payinAmount
			, String payinDate
			, String payinTime
			, String payinRef
			, String educationLevel
			) {
		Map<String, Object> response = new HashMap<String, Object>();
		String sql = sqlTemplate.getResource("e_regis_register.sql");

		try {

			if( registerId == -1  ) {
				response.put("message", "full");
			}else {
				
				Query query = this.apiDatasource.createNativeQuery(sql)
						//				.unwrap(org.hibernate.query.Query.class)
						.setParameter("register_id", new TypedParameterValue(StandardBasicTypes.INTEGER,  registerId))
						.setParameter("activity_id", new TypedParameterValue(StandardBasicTypes.INTEGER,  activityId))
						.setParameter("register_type", new TypedParameterValue(StandardBasicTypes.INTEGER,  registerType))
						.setParameter("activity_id", activityId)

						.setParameter("id", id)
						.setParameter("title", title)
						.setParameter("name", name)
						.setParameter("surname", surname)
						.setParameter("gender", gender)
						.setParameter("age", new TypedParameterValue(StandardBasicTypes.INTEGER, age))
						.setParameter("dob", dob)

						.setParameter("address1", address1)
						.setParameter("address2", address2)
						.setParameter("address3", address3)
						.setParameter("address4", address4)
						.setParameter("address5", address5)

						.setParameter("zip", zip)
						.setParameter("mobile", mobile)
						.setParameter("email", email)
						.setParameter("line", line)

						.setParameter("recommender_code", recommenderCode)

						.setParameter("attendance_result", attendanceResult)
						.setParameter("exam_result", examResult)

						.setParameter("payin_local", payinLocal)
						.setParameter("picture_local", pictureLocal)
						.setParameter("idcard_local", idcardLocal)

						.setParameter("insert_time", insertTime)
						.setParameter("update_time", updateTime)

						.setParameter("insert_by", insertBy)
						.setParameter("update_by", updateBy)
						.setParameter("agent_code", agentCode)

						.setParameter("bank_code", bankCode)
						.setParameter("payin_num", payinNum)
						.setParameter("payin_amount", new TypedParameterValue(StandardBasicTypes.DOUBLE, payinAmount))
						.setParameter("payin_date", payinDate)
						.setParameter("payin_time", payinTime)
						.setParameter("payin_ref", payinRef)
						.setParameter("education_level", educationLevel);

				int rowAffect = query.executeUpdate();
				if (rowAffect > 0) {
					response.put("message", "success");
					response.put("registerId", registerId);
				} else {
					response.put("message", "failed");
				}
			}
		}catch (Exception e) {
			e.printStackTrace();
			response.put("message", "failed");
		}
		return response;
	}
	
	@Transactional
	public Object attendanceByAdminQuery(
			  Integer registerId
			, Integer registerType
			, Integer activityId
			, String id
			, String title
			, String name
			, String surname
			, String gender
			, Integer age
			, String dob
			, String address1
			, String address2
			, String address3
			, String address4
			, String address5
			, String zip
			, String mobile
			, String email
			, String line
			, String recommenderCode
			, String payinLocal
			, String pictureLocal
			, String idcardLocal
			, String attendanceResult
			, Date updateTime
			, String updateBy
			, String bankCode
			, String payinNum
			, Double payinAmount
			, String payinDate
			, String payinTime
			, String payinRef
			, String educationLevel
			) {
		
		String sql = sqlTemplate.getResource("e_regis_attendance.sql");
		Map<String, Object> response = new HashMap<String, Object>();
		try {
		Query query = this.apiDatasource.createNativeQuery(sql)
				.setParameter("activity_id", new TypedParameterValue(StandardBasicTypes.INTEGER,  activityId))
				.setParameter("id", id)
				.setParameter("register_type", new TypedParameterValue(StandardBasicTypes.INTEGER,  registerType)) 
				.setParameter("title", title)
				.setParameter("name", name)
				.setParameter("surname", surname)
				.setParameter("gender", gender)
				.setParameter("age", new TypedParameterValue(StandardBasicTypes.INTEGER, age))
				.setParameter("dob", dob)
				
				.setParameter("address1", address1)
				.setParameter("address2", address2)
				.setParameter("address3", address3)
				.setParameter("address4", address4)
				.setParameter("address5", address5)
				
				.setParameter("zip", zip)
				.setParameter("mobile", mobile)
				.setParameter("email", email)
				.setParameter("line", line)
				
				.setParameter("recommender_code", recommenderCode)
				.setParameter("payin_local", payinLocal)
				.setParameter("picture_local", pictureLocal)
				.setParameter("idcard_local", idcardLocal)
				.setParameter("attendance_result", attendanceResult)
				.setParameter("update_time", updateTime)
				.setParameter("update_by", updateBy)
				
				.setParameter("bank_code", bankCode)
				.setParameter("payin_num", payinNum)
				.setParameter("payin_amount", new TypedParameterValue(StandardBasicTypes.DOUBLE, payinAmount))
				.setParameter("payin_date", payinDate)
				.setParameter("payin_time", payinTime)
				.setParameter("payin_ref", payinRef)
				.setParameter("education_level", educationLevel);
	
			int rowAffect = query.executeUpdate();
			if (rowAffect > 0) {
				response.put("message", "success");
			} else {
				response.put("message", "failed");
			}
		}catch (Exception e) {
			e.printStackTrace();
			response.put("message", "failed");
		}
		return response;
	}

}