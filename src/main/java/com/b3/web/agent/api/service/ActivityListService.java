package com.b3.web.agent.api.service;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.function.Predicate;
import java.util.stream.Collectors;

import org.hibernate.transform.Transformers;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import com.b3.web.agent.api.util.ObjectUtil;

@Service
public class ActivityListService extends AbstractApiService {

	final int PAGE_SIZE = 25;
	private static Predicate<Map<String, Object>> searchByKeywords(final String keywords) {
		return new Predicate<Map<String, Object>>() {
			public boolean test(Map<String, Object> t) {
				return t.get("activityId").toString().contains(keywords)
						|| t.get("activityName").toString().contains(keywords)
						|| t.get("location").toString().contains(keywords)
						|| t.get("activityDate").toString().contains(keywords);
			}
		};
	}
	private static List<Map<String, Object>> filterByKeywords (List<Map<String, Object>> data, Predicate<Map<String, Object>> predicate) {
		return data.stream()
				.filter(predicate)
				.collect(Collectors.<Map<String, Object>>toList());
	}
	
	public Object queryActivityList(String activityType, String provinceId, String keywords, int curPage, String sortingType , String webRole) {
		
		String sql = sqlTemplate.getResource("activity_list.sql");
//		String sqlPageable = sqlTemplate.getResource("activity_list_pageable.sql");
		int firstResult = curPage * PAGE_SIZE;
		int lastResult = firstResult+PAGE_SIZE;
		
		org.hibernate.query.Query queryList = this.apiDatasource.createNativeQuery(sql)
				.unwrap(org.hibernate.query.Query.class)
				.setResultTransformer(Transformers.ALIAS_TO_ENTITY_MAP)
				.setParameter("activityType", activityType)
				.setParameter("province", provinceId)
				.setParameter("sortType", sortingType)
				.setParameter("webRole", webRole);
//				.setFirstResult(firstResult);
//				.setMaxResults(PAGE_SIZE);
		
		List<Map<String, Object>> data = ObjectUtil.columnToCamelCase(queryList.getResultList());
		
		if (StringUtils.hasLength(keywords)) {
			data = filterByKeywords(data, searchByKeywords(keywords));
		}
		
		Integer totalElements = data.size();
		List<Map<String, Object>> result = data.subList(firstResult, lastResult<=data.size()?lastResult:data.size());

		
		Map<String, Object> response = new HashMap<String, Object>();

		response.put("element", totalElements);
		response.put("detail", result);
		response.put("totalElements", totalElements);
		response.put("totalPage", totalElements/PAGE_SIZE);
		response.put("pageSize", PAGE_SIZE);
		
		return response;
	}
	
	public List<Map<String, Object>> getActivitySubType( String activityType){
		String sql = sqlTemplate.getResource("activity_sub_type.sql");
		org.hibernate.query.Query query = this.apiDatasource.createNativeQuery(sql)
				.unwrap(org.hibernate.query.Query.class)
				.setResultTransformer(Transformers.ALIAS_TO_ENTITY_MAP)
				.setParameter("activity_type", activityType);
		List<Map<String, Object>> data = ObjectUtil.columnToCamelCase(query.getResultList());
		return data;
	}
	
	
	public Object queryActivityListById(String activityId) {
		
		String sql = sqlTemplate.getResource("activity_list_by_id.sql");
		
		org.hibernate.query.Query queryList = this.apiDatasource.createNativeQuery(sql)
				.unwrap(org.hibernate.query.Query.class)
				.setResultTransformer(Transformers.ALIAS_TO_ENTITY_MAP)
				.setParameter("activityId", activityId);
		List<Map<String, Object>> data = ObjectUtil.columnToCamelCase(queryList.getResultList());
		
		Map<String, Object> response = data.stream().findFirst().get();
	
		return response;
	}
}
