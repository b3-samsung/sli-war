package com.b3.web.agent.api.service;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.util.HashMap;
import java.util.Map;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.io.Resource;
import org.springframework.stereotype.Component;

@Component
public class SqlTemplateFactory {

	private static final Logger log = LogManager.getLogger(SqlTemplateFactory.class);
	private Map<String, String> resources;

	private SqlTemplateFactory(@Value("${sql-template-path}") Resource resourceFile) {

		log.info("Read sql template.");
		try {
			this.resources = new HashMap<String, String>();
			listFolder(resourceFile.getFile());
		} catch (IOException e) {
			new AssertionError("Error while read sql template.", e);
			System.exit(-1);
		}
	}

	private void listFolder(final File folder) throws IOException {
		for (final File fileEntry : folder.listFiles()) {
			if (fileEntry.isDirectory()) {
				listFolder(fileEntry);
			} else {
				readFile(fileEntry);

			}
		}
	}

	private void readFile(File file) throws IOException {
		String fileName = file.getName();
		if (!resources.containsKey(fileName)) {
			String value = new String(Files.readAllBytes(file.toPath()));
			this.resources.put(fileName, value);
		}
	}
	
	final public String getResource(String key) {
		return this.resources.get(key);
	}

}
