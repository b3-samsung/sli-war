package com.b3.web.agent.api.service;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.core.io.Resource;
import org.springframework.core.io.UrlResource;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.net.MalformedURLException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Base64;
import java.util.UUID;

@Service
public class FileStorageService {

	private static final Logger log = LogManager.getLogger(AgentApiService.class);
//    private final Path fileStorageLocation;
//
//    @Autowired
//    public FileStorageService(@Value("")String filePath) {
//        this.fileStorageLocation = Paths.get( filePath )
//                .toAbsolutePath().normalize();
//
//        try {
//            Files.createDirectories(this.fileStorageLocation);
//        } catch (Exception ex) {
//            throw new FileStorageException("Could not create the directory where the uploaded files will be stored.", ex);
//        }
//    }

//    public String storeFile(MultipartFile file) {
//        // Normalize file name
//        String fileName = StringUtils.cleanPath(file.getOriginalFilename());
//
//        try {
//            // Check if the file's name contains invalid characters
//            if(fileName.contains("..")) {
//                throw new FileStorageException("Sorry! Filename contains invalid path sequence " + fileName);
//            }
//
//            // Copy file to the target location (Replacing existing file with the same name)
//            Path targetLocation = Paths.get(directory, fileName);
//            Files.copy(file.getInputStream(), targetLocation, StandardCopyOption.REPLACE_EXISTING);
//
//            return fileName;
//        } catch (IOException ex) {
//            throw new FileStorageException("Could not store file " + fileName + ". Please try again!", ex);
//        }
//    }

    public Path writeFile( String directory, String fileName, String dataBase64 ) throws IOException {
    	String myPath = "";
    	try {
			 myPath = makeDirectory(directory);
	    	String[] component = dataBase64.split(",");
	    	log.info( String.format("DIrectory: %s , Filename: %s", myPath, fileName ));
	    	byte[] decodedFile  = Base64.getDecoder().decode(component[1]);
	    	Path destinationFile = Paths.get(myPath, fileName);
	    	return Files.write(destinationFile, decodedFile );
    	}catch (IOException e) {
    		deleteDirectory(myPath);
			throw new IOException(e);
		}
    
    }
    
    private String makeDirectory( String directory ) {
    	String uuid = UUID.randomUUID().toString();
    	String myPath = String.format("%s/%s", directory, uuid);
    	File theDir = new File(myPath);
    	if (!theDir.exists()){
    	    theDir.mkdirs();
    	}
		return myPath;
    	
    }
    private void deleteDirectory( String directory ) {
    	File myPath = new File(directory);
    	if( myPath.exists() ) {
    		myPath.deleteOnExit();
    	}
    }
    
    
    
    public Resource loadFileAsResource(String directory , String fileName) throws FileNotFoundException {
        try {
        	log.info( String.format("DIrectory: %s , Filename: %s", directory, fileName ));
        	if(StringUtils.hasLength( fileName) ) {
        		fileName = fileName.replace("\\", File.separator);
        		fileName = fileName.replace("/", File.separator);
        	}
            Path filePath = Paths.get(directory, fileName);
            Resource resource = new UrlResource(filePath.toUri());
            if(resource.exists()) {
                return resource;
            } else {
                throw new FileNotFoundException("File not found " + fileName);
            }
        } catch (MalformedURLException ex) {
            throw new FileNotFoundException("File not found " + fileName );
        }
    }
}