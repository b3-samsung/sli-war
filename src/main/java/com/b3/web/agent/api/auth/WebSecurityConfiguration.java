package com.b3.web.agent.api.auth;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.annotation.Order;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.builders.WebSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.core.session.SessionRegistry;
import org.springframework.security.core.session.SessionRegistryImpl;
import org.springframework.security.web.authentication.logout.LogoutSuccessHandler;
import org.springframework.security.web.authentication.logout.SecurityContextLogoutHandler;
import org.springframework.security.web.util.matcher.AntPathRequestMatcher;

import com.b3.web.agent.api.controller.LoggingAccessDeniedHandler;

@Order(2)
@Configuration
@EnableWebSecurity
public class WebSecurityConfiguration extends WebSecurityConfigurerAdapter {
    
	private static final Logger log = LogManager.getLogger(WebSecurityConfiguration.class);
    private static final String[] IGNORE = {
            "/css/**", "/js/**", "/img/**", "/webjars/**", "/webjarsjs", "/h2-console/**"
        };

	@Value("/${api.prefix}")
	private  String ROOT_PATTERN;
    @Autowired
    private LoggingAccessDeniedHandler accessDeniedHandler;
    
    @Override
    public void configure(WebSecurity web) {
        web.ignoring().antMatchers(IGNORE);
    }

    @Override
    protected void configure(HttpSecurity http) throws Exception {
        http
        .csrf().disable()
        .antMatcher("/**")
			.authorizeRequests()
			.antMatchers( ROOT_PATTERN+"/**").permitAll()
		    .anyRequest().authenticated()
        
        .and()
        .formLogin()
        .loginPage("/login")
//            .loginProcessingUrl("/login")
        .defaultSuccessUrl("/user", true)
        .permitAll()
    .and()
    .logout()
    	.logoutSuccessHandler(logoutSuccessHandler())
        .invalidateHttpSession(true)
        .clearAuthentication(true)
        .deleteCookies("JSESSIONID")
        .logoutRequestMatcher(new AntPathRequestMatcher("/logout"))
        .logoutSuccessUrl("/login?logout")
        .permitAll()
    .and()
    .exceptionHandling()
    	.accessDeniedPage("/login?error")
        .accessDeniedHandler(accessDeniedHandler);
        
        http.sessionManagement().maximumSessions(1).sessionRegistry(sessionRegistry());
        
    }
    
    @Bean
    public LogoutSuccessHandler logoutSuccessHandler() {
        return new CustomLogoutSuccessHandler();
    }
    
    @Bean
    public SessionRegistry sessionRegistry() {
        return new SessionRegistryImpl();
    }
}


