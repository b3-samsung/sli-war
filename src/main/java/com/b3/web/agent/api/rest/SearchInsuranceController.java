package com.b3.web.agent.api.rest;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.io.Resource;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.b3.web.agent.api.service.FileStorageService;
import com.b3.web.agent.api.service.SearchInsuranceService;

@Validated
@RestController
@RequestMapping("${api.prefix}/insurance")
public class SearchInsuranceController {
	
	private static final Logger log = LoggerFactory.getLogger(SearchInsuranceController.class);
	
	@Autowired
	private SearchInsuranceService searchInsuranceService;
    
	@Value("${fileedoc-prefix-path}")
    private  String fileEdocPrefixPath;
    
    @Autowired
    private FileStorageService fileStorageService;
    
	@GetMapping("/get-agent-child")
	public ResponseEntity getAgentChild(	@RequestParam (name="agentCode") String parentAgentCode) {
		return ResponseEntity.ok( searchInsuranceService.getAgentChild(parentAgentCode));
	}
	
	@GetMapping("/search-insurance")
	public ResponseEntity searchEDocuments(
			@RequestParam (name="agentCode", required=false) String agentCode
			, @RequestParam (name="agentFilter", required=false) String agentFilter
			, @RequestParam (name="policyCode", required=false) String policyCode
			, @RequestParam (name="policyId", required=false) String policyId
			, @RequestParam (name="fName", required=false) String fName
			, @RequestParam (name="lName", required=false) String lName
			, @RequestParam (name="pid", required=false) String pid
			, @RequestParam (name="curPage", required=false) Integer curPage
			
	) {
//		System.out.println("agentFilter "+ agentFilter);
//		System.out.println("agentCode "+ agentCode);
//		System.out.println("policyCode "+ policyCode);
//		System.out.println("policyId "+ policyId);
//		System.out.println("fName "+ fName);
//		System.out.println("lName "+ lName);
//		System.out.println("pid "+ pid);
		try {
			Map<String, Object>  data = searchInsuranceService.searchInsurance( agentCode 	,agentFilter 	, policyCode , policyId 	, fName , lName , pid , curPage);
			return ResponseEntity.ok(data);
		}catch (Exception e) {
			e.printStackTrace();
			return ResponseEntity.noContent().build();
		}
	}
    @GetMapping("download")
    public ResponseEntity<Resource> downloadFile(@RequestParam("policyCode") String policyCode
    		, HttpServletRequest request)  {

    	Resource resource = null;
        // Try to determine file's content type
        String contentType = null;
        try {
        	String fileName = searchInsuranceService.searchEdocBypolicy(policyCode);
            // Load file as Resource
            resource = fileStorageService.loadFileAsResource(fileEdocPrefixPath, fileName);
            contentType = request.getServletContext().getMimeType(resource.getFile().getAbsolutePath());

        // Fallback to the default content type if type could not be determined
        if(contentType == null) {
            contentType = "application/octet-stream";
        }

        return ResponseEntity.ok()
                .contentType(MediaType.parseMediaType(contentType))
                .header(HttpHeaders.CONTENT_DISPOSITION, "attachment; filename=\"" + resource.getFilename() + "\"")
                .body(resource);
        } catch (Exception ex) {
            log.info("Could not determine file type.");
            return ResponseEntity.ok(null);
        }
    }
    

    
    
}
