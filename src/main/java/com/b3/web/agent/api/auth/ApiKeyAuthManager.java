package com.b3.web.agent.api.auth;


import com.github.benmanes.caffeine.cache.CacheLoader;
import com.github.benmanes.caffeine.cache.Caffeine;
import com.github.benmanes.caffeine.cache.LoadingCache;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;

import javax.persistence.EntityManager;
import javax.transaction.Transactional;

import java.util.concurrent.TimeUnit;

/**
 * Handles authenticating api keys against the database.
 */
public class ApiKeyAuthManager implements AuthenticationManager {
	private static final Logger LOG = LogManager.getLogger(ApiKeyAuthManager.class);

	private final LoadingCache<ClientDetails, Boolean> keys;

	public ApiKeyAuthManager(EntityManager em) {
		this.keys = Caffeine.newBuilder()
				.expireAfterAccess(1, TimeUnit.MINUTES)
				.build(new DatabaseCacheLoader(em));
	}

	public Authentication authenticate(Authentication authentication) throws AuthenticationException {
		String principal = (String) authentication.getPrincipal();
		String credential = (String) authentication.getCredentials();
	
		ClientDetails client = new ClientDetails(principal, credential);
		if (!keys.get(client)) {
			throw new BadCredentialsException("The API key was not found or not the expected value.");
		} else {
			authentication.setAuthenticated(true);
			return authentication;
		}
	}

	/**
	 * Caffeine CacheLoader that checks the database for the api key if it not found in the cache.
	 */
	private static class DatabaseCacheLoader implements CacheLoader<ClientDetails, Boolean> {
		private final EntityManager em;

		DatabaseCacheLoader(EntityManager em) {
			this.em =em;
		}

		@Transactional
		public Boolean load(ClientDetails key) throws Exception {


			LOG.info("Loading api key from database: [key: {}]", key);

			try {
				return !this.em.createNativeQuery("SELECT * FROM CLIENT_DETAILS WHERE API_KEY = :apiKey and CLIENT_ID = :clientId AND IS_DELETE = 'F'")
						.setParameter("apiKey", key.getApiKey() )
						.setParameter("clientId",key.getCliendId()) 
						.getResultList().isEmpty();
			}catch (Exception e) {
				LOG.error("An error occurred while retrieving api key from database", e);
				return false;
			}


		}
	}
}