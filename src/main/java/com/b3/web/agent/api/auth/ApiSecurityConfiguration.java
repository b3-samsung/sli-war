package com.b3.web.agent.api.auth;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.annotation.Order;
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.config.http.SessionCreationPolicy;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

@Configuration
@EnableWebSecurity
@Order(1)
@EnableGlobalMethodSecurity(prePostEnabled = true)
public class ApiSecurityConfiguration extends WebSecurityConfigurerAdapter {
    
	private static final Logger log = LogManager.getLogger(ApiSecurityConfiguration.class);
	
	private static final String API_KEY_AUTH_HEADER_NAME = "API_KEY";
	private static final String CLIENT_ID_AUTH_HEADER_NAME = "CLIENT_ID";
	
	@Value("/${api.prefix}")
	private  String ROOT_PATTERN;

    @PersistenceContext(unitName="ddDatasource")
    private EntityManager em;

    @Override
    protected void configure(HttpSecurity http) throws Exception {
    	
    	log.info(""+ ROOT_PATTERN);
        ApiKeyAuthFilter filter = new ApiKeyAuthFilter(API_KEY_AUTH_HEADER_NAME, CLIENT_ID_AUTH_HEADER_NAME);
        filter.setAuthenticationManager(new ApiKeyAuthManager( em));
        http
        .antMatcher(ROOT_PATTERN+"/*").
                sessionManagement().sessionCreationPolicy(SessionCreationPolicy.STATELESS).
                and()
                    .addFilter(filter)
                    .authorizeRequests()
                	.antMatchers( ROOT_PATTERN+"/**").permitAll()
                    .anyRequest()
                    .authenticated();
    }
    

}


