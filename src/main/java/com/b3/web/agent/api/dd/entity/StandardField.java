package com.b3.web.agent.api.dd.entity;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.MappedSuperclass;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.xml.bind.annotation.XmlRootElement;

import org.springframework.format.annotation.DateTimeFormat;

@MappedSuperclass
@XmlRootElement
public abstract class StandardField<T> implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	// @Max(value=?)  @Min(value=?)//if you know range of your decimal fields consider using these annotations to enforce field validation
	@Id
	@Basic(optional = false)
	@Column(name = "ID", nullable = false)
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	protected T id;
	@Column(name = "CREATED_BY", length = 256)
	protected String createdBy;
//	@DateTimeFormat(pattern = "yyyy-MM-dd'T'hh:mm:ss")
	@Column(name = "CREATED_DATE")
	@Temporal(TemporalType.TIMESTAMP)
	protected Date createdDate;
	@Column(name = "UPDATED_BY", length = 256)
	protected String updatedBy;
//	@DateTimeFormat(pattern = "yyyy-MM-dd'T'hh:mm:ss")
	@Column(name = "UPDATED_DATE")
	@Temporal(TemporalType.TIMESTAMP)
	protected Date updatedDate;
	@Column(name = "IS_DELETE")
	protected Character isDelete;

	public static class DELETE {
		public static Character F = 'F';
		public static Character T = 'T';
	}
	
	public StandardField(T id) {
		super();
		this.id = id;
		this.createdDate = new Date();
		this.isDelete = 'F';
	}
	
	public StandardField() {
		super();
		this.createdDate = new Date();
		this.isDelete = DELETE.F;
	}

	
	public T getId() {
		return id;
	}

	public void setId(T id) {
		this.id = id;
	}

	public String getCreatedBy() {
		return createdBy;
	}

	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}

	public Date getCreatedDate() {
		return createdDate;
	}

	public void setCreatedDate(Date createdDate) {
		this.createdDate = createdDate;
	}

	public String getUpdatedBy() {
		return updatedBy;
	}

	public void setUpdatedBy(String updatedBy) {
		this.updatedBy = updatedBy;
	}

	public Date getUpdatedDate() {
		return updatedDate;
	}

	public void setUpdatedDate(Date updatedDate) {
		this.updatedDate = updatedDate;
	}

	public Character getIsDelete() {
		return isDelete;
	}

	public void setIsDelete(Character isDelete) {
		this.isDelete = isDelete;
	}

	public boolean getActive() {
		return !DELETE.T.equals(this.isDelete);
	}
	
	
	@Override
	public int hashCode() {
		int hash = 0;
		hash += (id != null ? id.hashCode() : 0);
		return hash;
	}

	@Override
	public boolean equals(Object object) {
		
		if (this == object)
			return true;
		if (object == null)
			return false;
		if (getClass() != object.getClass())
			return false;
		
		@SuppressWarnings("unchecked")
		StandardField<T> other = (StandardField<T>) object;
		if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
			return false;
		}
		return true;
	}

	@Override
	public String toString() {
		return getClass()+"[ id=" + id + " ]";
	}
}
