package com.b3.web.agent.api.service;

import java.util.List;
import java.util.Map;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.hibernate.transform.Transformers;
import org.springframework.stereotype.Service;

import com.b3.web.agent.api.util.ObjectUtil;


@Service
@SuppressWarnings({"deprecation", "unchecked"})
public class AgentApiService extends AbstractApiService {

	private static final Logger log = LogManager.getLogger(AgentApiService.class);
	private static final String SEARCH_AGENT_DETAIL = "search_agent_detail.sql";
	private static final String SEARCH_RECOMMENDER_AGENT_DETAIL = "search_recommender_agent_detail.sql";
	
	public Object searchAgent(String leadAgentCode, String agentCode ) {
		return  this.searchAgent( leadAgentCode,  agentCode, "false");
	}
	
	public Object searchAgent(String leadAgentCode, String agentCode, String ignoreStatus ) {
		String sql = sqlTemplate.getResource(SEARCH_AGENT_DETAIL);
		log.info("Search Agent Filter " + agentCode + ", Search Agent Leader " + leadAgentCode);
		Map<String, Object> data = null;
		try {
			Object query = this.apiDatasource.createNativeQuery(sql)
				.setParameter("agent_code", agentCode)
				.setParameter("lead_agent_code", leadAgentCode)
				.setParameter("ignoreStatus", ignoreStatus)
				.unwrap(org.hibernate.query.Query.class)
				.setResultTransformer(Transformers.ALIAS_TO_ENTITY_MAP)
				.uniqueResult();
			data = ObjectUtil.columnToCamelCase(query);
		}catch (Exception e) {

		}
		return data;
	}
	
	public Object searchRecommenderAgent(String agentCode) {
		String sql = sqlTemplate.getResource(SEARCH_RECOMMENDER_AGENT_DETAIL);
		Map<String, Object> data = null;
		try {
			Object query = this.apiDatasource.createNativeQuery(sql)
					.setParameter("agent_code", agentCode)
					.unwrap(org.hibernate.query.Query.class)
					.setResultTransformer(Transformers.ALIAS_TO_ENTITY_MAP)
					.uniqueResult();
			data = ObjectUtil.columnToCamelCase(query);
		}catch (Exception e) {

		}
		return data;
	}
	
	public String searchAgentCode(String pid) {
		String sql = sqlTemplate.getResource("search_agent_in_activity.sql");
		String agentCode = null;
		try {
			Object query = this.apiDatasource.createNativeQuery(sql)
					.setParameter("pid", pid)
					.unwrap(org.hibernate.query.Query.class)
					.setResultTransformer(Transformers.ALIAS_TO_ENTITY_MAP)
					.uniqueResult();
			Map<String, Object> data = ObjectUtil.columnToCamelCase(query);

			if (data != null) {
				agentCode = (String) data.get("agentCode");
			} 

		}catch (Exception e) {

		}
		return agentCode;
	}

	public Object searchIsRepeat(String pid, String activityType) {
		String sql = sqlTemplate.getResource("check_activity_repeat.sql");
		Map<String, Object> data = null;
		try {
			Object query = this.apiDatasource.createNativeQuery(sql)
				.setParameter("pid", pid)
				.setParameter("ACTIVITY_TYPE", activityType)
				.unwrap(org.hibernate.query.Query.class)
				.setResultTransformer(Transformers.ALIAS_TO_ENTITY_MAP)
				.uniqueResult();
			data = ObjectUtil.columnToCamelCase(query);
		
		}catch (Exception e) {

		}

		return data;
	}
	
	public Object searchIsRegister(String pid, String activityId) {
		String sql = sqlTemplate.getResource("check_already_register.sql");
		Map<String, Object> data = null;
		try {
			Object query = this.apiDatasource.createNativeQuery(sql)
				.setParameter("pid", pid)
				.setParameter("activityId", activityId)
				.unwrap(org.hibernate.query.Query.class)
				.setResultTransformer(Transformers.ALIAS_TO_ENTITY_MAP)
				.uniqueResult();
			data = ObjectUtil.columnToCamelCase(query);
		}catch (Exception e) {

		}
		return data;
	}
	
	public Object searchIsPayinNumByAgent(String payinNum) {
		String sql = sqlTemplate.getResource("check_activity_payin_num_agent.sql");
		Map<String, Object> data = null;
		try {
			Object query = this.apiDatasource.createNativeQuery(sql)
					.setParameter("payinNum", payinNum)
					.unwrap(org.hibernate.query.Query.class)
					.setResultTransformer(Transformers.ALIAS_TO_ENTITY_MAP)
					.uniqueResult();
			data = ObjectUtil.columnToCamelCase(query);
		}catch (Exception e) {

		}
		return data;
	}
	
	public Object searchIsPayinNumByAdmin(String payinNum, String activityId, String registerId) {
		String sql = sqlTemplate.getResource("check_activity_payin_num_admin.sql");
		Map<String, Object> data = null;
		try {
			Object query = this.apiDatasource.createNativeQuery(sql)
				.setParameter("payinNum", payinNum)
				.setParameter("activityId", activityId)
				.setParameter("registerId", registerId)
				.unwrap(org.hibernate.query.Query.class)
				.setResultTransformer(Transformers.ALIAS_TO_ENTITY_MAP)
				.uniqueResult();
			data = ObjectUtil.columnToCamelCase(query);
		}catch (Exception e) {

		}
		return data;
	}
	
	
	public Object getMemoAgent(String agentCode) {
		return getMemo( agentCode,  "MEMO_AGENT");
	}
	
	public Object getMemoOic(String agentCode) {
		return getMemo( agentCode,  "MEMO_OIC");
	}
	
	/**
	 * 
	 * @param agentCode
	 * @param memoType 		MEMO_OIC, MEMO_AGENT
	 * @return
	 */
	public Object getMemo(String agentCode, String memoType) {

		String sql = sqlTemplate.getResource("get_memo.sql");

		javax.persistence.Query query = this.apiDatasource.createNativeQuery(sql)
				.setParameter("AGENT_CODE", agentCode)
				.setParameter("MEMO_TYPE", memoType)
				.unwrap(org.hibernate.query.Query.class)
				.setResultTransformer(Transformers.ALIAS_TO_ENTITY_MAP);

		List<Map<String, Object>>  data = ObjectUtil.columnToCamelCase(query.getResultList());

		return data;
	}
	
	public Object searchAgentInActivity(String pid, String activityId) {
		String sql = sqlTemplate.getResource("search_agent_detail_activity.sql");
		Map<String, Object> data = null;
		try {

			Object query = this.apiDatasource.createNativeQuery(sql)
					.setParameter("pid", pid)
					.setParameter("activityId", activityId)
					.unwrap(org.hibernate.query.Query.class)
					.setResultTransformer(Transformers.ALIAS_TO_ENTITY_MAP)
					.uniqueResult();
			data = ObjectUtil.columnToCamelCase(query);
		}catch (Exception e) {
	
		}
		return data;
	}

}
