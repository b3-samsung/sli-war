package com.b3.web.agent.api.service;

import java.util.Base64;
import java.util.HashMap;
import java.util.Map;

import javax.persistence.Query;
import javax.transaction.Transactional;

import org.springframework.beans.factory.config.ConfigurableBeanFactory;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;

import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.CellType;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.ss.usermodel.WorkbookFactory;
import org.hibernate.jpa.TypedParameterValue;
import org.hibernate.type.StandardBasicTypes;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.text.DecimalFormat;

import com.b3.web.agent.api.model.ExcelUploadModel;

@Component
@org.springframework.context.annotation.Scope(value = ConfigurableBeanFactory.SCOPE_PROTOTYPE)
@Service
public class UploadActivityExcelService extends AbstractApiService {
	
	private static final Logger log = LoggerFactory.getLogger(UploadActivityExcelService.class);
	private static final  String[] DATA_COLUMN = { 
			"ACTIVITY_ID", 
			"ACTIVITY_TYPE", 
			"ACTIVITY_NAME", 
			"ACTIVITY_DATE", 
			"LOCATION", 
			"PROVINCE", 
			"NUMOFDAYS", 
			"START_TIME", 
			"END_TIME", 
			"ACTIVITY_REGIS_FROM", 
			"ACTIVITY_REGIS_TO", 
			"MAX_CAPA", 
			"ROOM_CAPA", 
			"DEL_INDI", 
			"INSERT_TIME", 
			"UPDATE_TIME", 
			"IS_TEST", 
			"ACTIVITY_DATE_TO", 
			"ACTIVITY_SUB",
			"REMARK1",
			"INSERT_BY", 
			"UPDATE_BY"
	};
	private final static DecimalFormat DF = new DecimalFormat("0");
	
	private int FIRST_ROW = 0;
	private int SHEET_DATA = 0;
	
	private int uploadToActivityType = 0;
	private String updateBy = "";
	
	// Import File Method: call by restController
	public void importFile(ExcelUploadModel file, int activityTypeId, String updateBy) throws IOException {
		InputStream is = null;
		Boolean isUploadActivityStatus = false;
		this.uploadToActivityType = activityTypeId;
		this.updateBy = updateBy;
		
		try {
			log.info("Start import activity file: " + file.getFilename() );
			
			is = new ByteArrayInputStream(decodeString(file.getData()));
			
			Workbook workbooks = WorkbookFactory.create(is);
			Sheet sheet = workbooks.getSheetAt(SHEET_DATA);
			log.info("Start read activity data row" );
			readRow(sheet);
			workbooks.close();
		} 
		catch (IOException e) {
			log.error("Upload Activity File Error: " + e.getMessage());
			throw new IOException(e);
		} 
		finally {
			try {
				is.close();
			} catch (Exception e2) {
				log.error(e2.getMessage());
				throw new IOException(e2);
			}
		}
	}
	
	private byte[] decodeString(String urlBase64) {
		String[] data = urlBase64.split("base64,");
		return Base64.getDecoder().decode(data[1]);
	}
	
	// Collect data of each rows
	private void readRow(Sheet sheet) throws IOException {
		log.info("ROW NUM: " + sheet.getLastRowNum());
		Boolean uploadStatus = false;
		for (int r = FIRST_ROW + 1; r <= sheet.getLastRowNum(); r++) {
			log.info("# " + r + " READING Activity data...");
			Row row = sheet.getRow(r);
			Map<String, String> dataRow = readCellConfigDataAndInsert(row);
			//log.info("UPDATE " + dataRow.get("ACTIVITY_TYPE") + " = " + this.uploadToActivityType);
//			if (Integer.parseInt(dataRow.get("ACTIVITY_TYPE")) == this.uploadToActivityType) {
			insertByRow(dataRow);
//			} else {
//				continue;
//			}
		}
	}
	
	// Read of each column
	private Map<String, String> readCellConfigDataAndInsert(Row row) {
		Map<String, String> data = new HashMap<String, String>();
		for (int i = 0; i < DATA_COLUMN.length; i++) {
			String value = toCellString(row.getCell(i));
			data.put(DATA_COLUMN[i], value);
		}
		return data;
	}
	
	// Extract Value in Column
	private String toCellString(Cell cell) {
		if (cell == null)
			return "";
		Object o = null;
		CellType type = cell.getCellType();
		if (type == CellType.STRING) {
			o = cell.getRichStringCellValue();
		} else if (type == CellType.NUMERIC) {
			o = DF.format(cell.getNumericCellValue());
		} else if (type == CellType.BOOLEAN) {
			o = cell.getBooleanCellValue();
		} else if (type == CellType.BLANK) {
			o = "";
		} else {
			o = new String("");
		}
		return String.format("%s", o.toString());
	}
	
	// Insert to Table
	@Transactional
	public void insertByRow(Map<String, String> data) throws IOException {
		String sqlInsertActivityExcel = sqlTemplate.getResource("upload_insert_activity_excel.sql");
		// INSERT TO TABLE DH_T_AG_ACTIVITY
		try {
			java.sql.Date stampDate = new java.sql.Date(new java.util.Date().getTime());
			Query query = this.apiDatasource.createNativeQuery(sqlInsertActivityExcel)
					.setParameter("activityId", Integer.parseInt(data.get("ACTIVITY_ID")))
					.setParameter("activityType", Integer.parseInt(data.get("ACTIVITY_TYPE")))
					.setParameter("activityName", data.get("ACTIVITY_NAME"))
					.setParameter("activityDate", data.get("ACTIVITY_DATE"))
					.setParameter("location", data.get("LOCATION"))
					.setParameter("province", data.get("PROVINCE"))
					.setParameter("numOfDays", Integer.parseInt(data.get("NUMOFDAYS")))
					.setParameter("startTime", data.get("START_TIME"))
					.setParameter("endTime", data.get("END_TIME"))
					.setParameter("activityRegisFrom", data.get("ACTIVITY_REGIS_FROM"))
					.setParameter("activityRegisTo", data.get("ACTIVITY_REGIS_TO"))
					.setParameter("maxCapa", Integer.parseInt(data.get("MAX_CAPA")))
					.setParameter("roomCapa", Integer.parseInt(data.get("ROOM_CAPA")))
					.setParameter("delIndi", data.get("DEL_INDI"))
					.setParameter("insertTime", stampDate)
					.setParameter("updateTime", stampDate)
					.setParameter("isTest", data.get("IS_TEST"))
					.setParameter("activityDateTo", data.get("ACTIVITY_DATE_TO"))
					.setParameter("activitySub", Integer.parseInt(data.get("ACTIVITY_SUB")))
					.setParameter("remark", data.get("REMARK1"))
					.setParameter("insertBy", data.get("INSERT_BY"))
					.setParameter("updateBy", data.get("UPDATE_BY"));
			
			int rowAffect = query.executeUpdate();
			if (rowAffect > 0) {
				log.info("INSERT ACTIVITY.EXCEL SUCCESS!");
			} else {
				log.error("INSERT ACTIVITY.EXCEL FAILED, no row affect");
			}
		} catch (Exception e) {
			log.error("INSERT ACTIVITY.EXCEL FAILED: " + e);
			throw new IOException(e);
		}
	}
}
