package com.b3.web.agent.api.db;

import java.util.Properties;

import javax.sql.DataSource;

import org.hibernate.jpa.HibernatePersistenceProvider;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.boot.jdbc.DataSourceBuilder;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.orm.jpa.JpaTransactionManager;
import org.springframework.orm.jpa.LocalContainerEntityManagerFactoryBean;
import org.springframework.transaction.PlatformTransactionManager;
import org.springframework.transaction.annotation.EnableTransactionManagement;

import com.zaxxer.hikari.HikariDataSource;

@Configuration
@EnableTransactionManagement
@EnableJpaRepositories(basePackages = {"com.b3.web.agent.api.repository", "com.b3.web.agent.api.service"},
entityManagerFactoryRef = "apiEntityManagerFactory",
transactionManagerRef= "apiTransactionManager"
		)
//@PropertySource("file:${config.path}/application-${spring.profiles.active}.properties")
public class ApiDatabaseConfig  {


	@Bean("apiDatasource")
	@ConfigurationProperties("spring.datasource-api")
	public DataSource apiDatasource() {
		return DataSourceBuilder.create().type(HikariDataSource.class).build();
	}

	@Bean(name = "apiEntityManagerFactory")
	public LocalContainerEntityManagerFactoryBean apiEntityManagerFactory() {
		return new LocalContainerEntityManagerFactoryBean() {{
			setDataSource(apiDatasource());
			setPersistenceProviderClass(HibernatePersistenceProvider.class);
			setPersistenceUnitName("apiDatasource");
			setPackagesToScan("com.b3.web.agent.api.entity");
			setJpaProperties(apiJpaProperties());
		}};
	}

	@Bean("apiJpaProperties")
	@ConfigurationProperties("spring.jpa.properties")
	public Properties apiJpaProperties() {
		return new Properties();
	}

	@Bean("apiTransactionManager")
	public PlatformTransactionManager apiTransactionManager(
			final @Qualifier("apiEntityManagerFactory") LocalContainerEntityManagerFactoryBean apiEntityManagerFactory) {
		return new JpaTransactionManager(apiEntityManagerFactory.getObject());
	}


}
