package com.b3.web.agent.api.service;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.hibernate.transform.Transformers;
import org.springframework.stereotype.Service;

import com.b3.web.agent.api.util.ObjectUtil;


@Service
public class TrainingExaminationService extends AbstractApiService {

//	public List<Map<String, Object>> searchActivities( String agentCode, String filter, String activityType) {
//
//		String sql = super.sqlTemplate.getResource("cb_search_e_regis_customer.sql");
//		org.hibernate.query.Query query = super.apiDatasource.createNativeQuery(sql)
//				.unwrap(org.hibernate.query.Query.class)
//				.setResultTransformer(Transformers.ALIAS_TO_ENTITY_MAP)
//				.setParameter("agentCode", agentCode)
//				.setParameter("filter", filter)
//				.setParameter("activityType", activityType);
//		List<Map<String, Object>> data = ObjectUtil.columnToCamelCase(query.getResultList());
//		
//		return data;
//	}
	
	
	public List<Map<String, Object>> getAgentActivities( String agentCode, String filter, String activityType, String period) {

		String sql = super.sqlTemplate.getResource("cb_get_agent_activities.sql");
		org.hibernate.query.Query query = super.apiDatasource.createNativeQuery(sql)
				.unwrap(org.hibernate.query.Query.class)
				.setResultTransformer(Transformers.ALIAS_TO_ENTITY_MAP)
				.setParameter("agentCode", agentCode)
				.setParameter("filter", filter)
				.setParameter("activityType", activityType)
				.setParameter("period", period);
		List<Map<String, Object>> data = ObjectUtil.columnToCamelCase(query.getResultList());
		
		return data;
	}
	
	public Map<String, List<Map<String, Object>>> getAgentActivities( String agentCode, String filter ) {
		Map<String, List<Map<String, Object>>> result = new HashMap();
		
		String period = "FUTURE";
		result.put("type1", getAgentActivities(  agentCode,  filter, "1", period));
		result.put("type2",getAgentActivities(  agentCode,  filter, "2", period));
		result.put("type3",getAgentActivities(  agentCode,  filter, "5", period));
		result.put("type4",getAgentActivities(  agentCode,  filter, "3", period));
		result.put("type5",getAgentActivities(  agentCode,  filter, "4", period));
		
		return result;
	}
	public Map<String, List<Map<String, Object>>> getAgentActivitiesResult( String agentCode, String filter ) {
		Map<String, List<Map<String, Object>>> result = new HashMap();
		final String period = "PAST";
		result.put("type1", getAgentActivities(  agentCode,  filter, "1", period));
		result.put("type2",getAgentActivities(  agentCode,  filter, "2", period));
		result.put("type3",getAgentActivities(  agentCode,  filter, "5", period));
		result.put("type4",getAgentActivities(  agentCode,  filter, "3", period));
		
		return result;
	}

	
	/**
	 * Examination type = 2
	 * @param agentCode
	 * @param filte
	 * @return
	 */
//	public List<Map<String, Object>> searchExamination( String agentCode, String filter ) {
//		return searchActivities(  agentCode,  filter, "2");
//	}
	
	/**
	 *  Training type = 1
	 * @param agentCode
	 * @param filte
	 * @return
	 */
//	public List<Map<String, Object>> searchTraining( String agentCode, String filter ) {
//		return searchActivities(  agentCode,  filter, "1");
//	}

}
