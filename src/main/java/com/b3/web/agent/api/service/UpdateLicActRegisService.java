package com.b3.web.agent.api.service;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.config.ConfigurableBeanFactory;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.List;
import java.util.Map;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.chrono.ChronoLocalDate;
import java.util.Calendar;

import javax.persistence.Query;
import javax.transaction.Transactional;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.apache.logging.log4j.core.impl.Log4jContextFactory;

import org.hibernate.transform.Transformers;
import org.springframework.stereotype.Service;

import com.b3.web.agent.api.rest.SearchDocumentController;
import com.b3.web.agent.api.util.ObjectUtil;

import jline.internal.Log;


@Component
@org.springframework.context.annotation.Scope(value = ConfigurableBeanFactory.SCOPE_PROTOTYPE)
@Service
public class UpdateLicActRegisService extends AbstractApiService{
	
	public void setActRegis(Map<String, String> dataRow) {
		log.info(dataRow);
		//get date
		String day = (String)dataRow.get("DAY");
		String month = (String)dataRow.get("MONTH");
		String year = (String)dataRow.get("YEAR");
		String dateString = year +"-"+ month +"-"+day;
		Date date = null;
		try {
			date = new SimpleDateFormat("yyyy-MM-dd").parse(dateString);
		} catch (ParseException e) {
			e.printStackTrace();
		}
		
		//get ID
		String cid = (String)dataRow.get("CID");
		System.out.println(day); 
		System.out.println("CID "+cid);
		if(selectLimitMonth(date)) {
			System.out.println("check date pass");
			updateActRegis(cid);
			
		}
		
	}
	
	private static final Logger log = LogManager.getLogger(UpdateLicActRegisService.class);
	public void updateActRegis(String id) {
		//log.info(id);
		String sqlUpload = sqlTemplate.getResource("update_act_regis2.sql");
		Query query = this.apiDatasource.createNativeQuery(sqlUpload)
				.setParameter("examResult", 'Y') 
				.setParameter("cid", id) 
				.setParameter("delIndi", 'Y'); 
		int rowAffect = query.executeUpdate();
		log.info(rowAffect);
			//log.info(data);
			log.info("UPDATE ACTREGIS DONE"); 	
	
	}
	
	 
	public boolean selectLimitMonth(Date dateLic) {
		log.info("dateLIC = "+dateLic);
		boolean check = false;
		String sqlLimit = sqlTemplate.getResource("search_limit_m_lic.sql");
		org.hibernate.query.Query query = this.ddDatasource.createNativeQuery(sqlLimit)
				.unwrap(org.hibernate.query.Query.class)
				.setResultTransformer(
						Transformers.ALIAS_TO_ENTITY_MAP
						)
				.setParameter("id", 61); 
		List<Map<String, Object>> data = query.getResultList();
		Map<String, Object> temp = data.get(0);
		String temp2 = (String) temp.get("TO_NCHAR(PARAM_VALUE)");
		log.info("PARA ="+data);
		log.info("LIMIT MONTH "+temp2);
		String limitPara = temp2;
		int limitMonth =Integer.parseInt(limitPara); 
		SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
		Date dateNow = new Date(); 
		//check limit date
		Calendar c = Calendar.getInstance(); 
	    c.setTime(dateNow);  
	    c.add(Calendar.MONTH, -limitMonth);
	    Date limitdate= c.getTime();
	    log.info("LimitDATE = "+limitdate);
	    if(dateLic.after(limitdate) && dateLic.before(dateNow)) {
	    	 check = true;
	    	   
	    }
	    log.info(check);
	     
		return check;
	}
	
}
