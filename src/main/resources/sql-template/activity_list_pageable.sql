SELECT 
    COUNT(ACTIVITY_ID) AS TOTAL_ELEMENT
    , CEIL(COUNT(ACTIVITY_ID) / :pageSize) AS TOTAL_PAGE
FROM dh_t_ag_activity 
WHERE 
	(NVL(DEL_INDI,'N' ) = 'N' OR ( DEL_INDI = 'Y' AND :webRole LIKE '%REGISTER_ADMIN%' ) )
	AND activity_type = :activityType 
    AND province LIKE 
        CASE
            WHEN :province <> '*' THEN :province
            ELSE '%%'
        END
    AND TRUNC(SYSDATE) - TRUNC(ACTIVITY_DATE) <= 30
