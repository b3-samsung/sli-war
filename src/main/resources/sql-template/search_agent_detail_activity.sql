SELECT regis.*
    , SUBSTR(regis.ADDRESS1, 1, INSTR(regis.ADDRESS1, '|')-1) AS BUILDING_NUMBER,
      SUBSTR(
        SUBSTR(
          regis.ADDRESS1, 
          INSTR(regis.ADDRESS1, '|') + 1, 
          Length(regis.ADDRESS1)
        ),
        1, 
        INSTR( 
          SUBSTR(
            regis.ADDRESS1, 
            INSTR(regis.ADDRESS1, '|')+1, 
            Length(regis.ADDRESS1)
          ), '|'
        ) -1
      ) AS MOO,
      SUBSTR(
        SUBSTR(
          SUBSTR(
            regis.ADDRESS1, 
            INSTR(regis.ADDRESS1, '|') + 1, 
            Length(regis.ADDRESS1)
          ), 
          INSTR(
            SUBSTR(
              regis.ADDRESS1, 
              INSTR(regis.ADDRESS1, '|') + 1, 
              Length(regis.ADDRESS1)
            ), '|'
          ) + 1 , 
          Length (
            SUBSTR(
              regis.ADDRESS1, 
              INSTR(
                SUBSTR(
                  regis.ADDRESS1, 
                  INSTR(regis.ADDRESS1, '|')+1, 
                  Length(regis.ADDRESS1)
                ), '|'
              )+1, 
              Length(
                SUBSTR(
                  regis.ADDRESS1, 
                  INSTR(regis.ADDRESS1, '|')+1, 
                  Length(regis.ADDRESS1)
                )
              )
            )
          )
        ),
        1,
        INSTR(
          SUBSTR(
            SUBSTR(
              regis.ADDRESS1, 
              INSTR(regis.ADDRESS1, '|') + 1, 
              Length(regis.ADDRESS1)
            ), 
            INSTR(
              SUBSTR(
                regis.ADDRESS1, 
                INSTR(regis.ADDRESS1, '|') + 1, 
                Length(regis.ADDRESS1)
              ), '|'
            ) + 1 , 
            Length (
              SUBSTR(
                regis.ADDRESS1, 
                INSTR(
                  SUBSTR(
                    regis.ADDRESS1, 
                    INSTR(regis.ADDRESS1, '|')+1, 
                    Length(regis.ADDRESS1)
                  ), '|'
                )+1, 
                Length(
                  SUBSTR(
                    regis.ADDRESS1, 
                    INSTR(regis.ADDRESS1, '|')+1, 
                    Length(regis.ADDRESS1)
                  )
                )
              )
            )
          ), '|'
        ) - 1
      ) AS SOI_NAME,
      SUBSTR(
        SUBSTR(
          SUBSTR(
            regis.ADDRESS1, 
            INSTR(regis.ADDRESS1, '|') + 1, 
            Length(regis.ADDRESS1)
          ), 
          INSTR(
            SUBSTR(
              regis.ADDRESS1, 
              INSTR(regis.ADDRESS1, '|') + 1, 
              Length(regis.ADDRESS1)
            ), '|'
          ) + 1 , 
          Length (
            SUBSTR(
              regis.ADDRESS1, 
              INSTR(
                SUBSTR(
                  regis.ADDRESS1, 
                  INSTR(regis.ADDRESS1, '|')+1, 
                  Length(regis.ADDRESS1)
                ), '|'
              )+1, 
              Length(
                SUBSTR(
                  regis.ADDRESS1, 
                  INSTR(regis.ADDRESS1, '|')+1, 
                  Length(regis.ADDRESS1)
                )
              )
            )
          )
        ),
        INSTR(
          SUBSTR(
            -- earth|is|flat
            SUBSTR(
              -- The|earth|is|flat
              regis.ADDRESS1, 
              INSTR(regis.ADDRESS1, '|') + 1, 
              Length(regis.ADDRESS1)
            ), 
            INSTR(
              -- earth|is|flat
              SUBSTR(
                -- The|earth|is|flat
                regis.ADDRESS1, 
                INSTR(regis.ADDRESS1, '|') + 1, 
                Length(regis.ADDRESS1)
              ), '|'
            ) + 1 , 
            Length (
              SUBSTR(
                regis.ADDRESS1, 
                INSTR(
                  -- earth|is|flat
                  SUBSTR(
                    -- The|earth|is|flat
                    regis.ADDRESS1, 
                    INSTR(regis.ADDRESS1, '|')+1, 
                    Length(regis.ADDRESS1)
                  ), '|'
                )+1, 
                Length(
                  -- earth|is|flat
                  SUBSTR(
                    -- The|earth|is|flat
                    regis.ADDRESS1, 
                    INSTR(regis.ADDRESS1, '|')+1, 
                    Length(regis.ADDRESS1)
                  )
                )
              )
            )
          ), '|'
        ) + 1 ,
        Length(
          SUBSTR(
            SUBSTR(
              regis.ADDRESS1, 
              INSTR(regis.ADDRESS1, '|') + 1, 
              Length(regis.ADDRESS1)
            ), 
            INSTR(
              SUBSTR(
                regis.ADDRESS1, 
                INSTR(regis.ADDRESS1, '|') + 1, 
                Length(regis.ADDRESS1)
              ), '|'
            ) + 1 , 
            Length (
              SUBSTR(
                regis.ADDRESS1, 
                INSTR(
                  SUBSTR(
                    regis.ADDRESS1, 
                    INSTR(regis.ADDRESS1, '|')+1, 
                    Length(regis.ADDRESS1)
                  ), '|'
                )+1, 
                Length(
                  SUBSTR(
                    regis.ADDRESS1, 
                    INSTR(regis.ADDRESS1, '|')+1, 
                    Length(regis.ADDRESS1)
                  )
                )
              )
            )
          )
        )
      ) AS ROAD_NAME
FROM DH_T_ACT_REGIS regis
WHERE ID = :pid AND ACTIVITY_ID = :activityId