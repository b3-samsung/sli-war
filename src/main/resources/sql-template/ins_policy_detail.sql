WITH w_charge_mode AS
  (SELECT charge_type,
          (CASE
               WHEN charge_type=1 THEN 'ราย 12 เดือน'
               WHEN charge_type=2 THEN 'ราย 6 เดือน'
               WHEN charge_type=3 THEN 'ราย 3 เดือน'
               WHEN charge_type=4 THEN 'รายเดือน'
               WHEN charge_type=5 THEN 'ชำระครั้งเดียว'
               ELSE '(' || charge_type || ':' || charge_name || ')'
           END) AS charge_name
   FROM t_charge_mode), -- w_pay_mode
w_pay_mode AS
  (SELECT mode_id,
          (CASE
               WHEN mode_id=1 THEN 'เงินสด'
               WHEN mode_id=3 THEN 'หักบัญชีเงินฝากอัตโนมัติ'
               WHEN mode_id=30 THEN 'หักบัตรเครดิตต่อเนื่อง'
               ELSE '(' || mode_id || ':' || mode_name || ')'
           END) AS mode_name
   FROM t_pay_mode),
 w_policy_status AS
    (SELECT tcm.policy_id AS policy_id,
          (CASE --            WHEN tcm.suspend='Y' AND tcm.suspend_cause<>'99' THEN 'อยู่ระหว่างดำเนินการ กรุณาติดต่อทางบริษัท'

               WHEN tcm.liability_state=3 THEN CASE
                                                   WHEN tcp.end_cause = 51 THEN 'สิ้นผลบังคับ/ขอยกเลิกกรมธรรม์ (Freelook)'
                                                   WHEN tcp.end_cause = 31 THEN 'สิ้นผลบังคับ/เวนคืนกรมธรรม์'
                                                   WHEN tcp.end_cause = 12 THEN 'สิ้นสุดความคุ้มครอง'
                                                   WHEN tcp.end_cause IS NULL THEN 'สิ้นผลบังคับ'
                                                   WHEN tcp.end_cause = 21 THEN 'สิ้นผลบังคับ'
                                                   WHEN tcp.end_cause = 22
                                                        OR tcp.end_cause = 23 THEN 'สิ้นผลบังคับ'
                                                   WHEN tcp.end_cause = 34 THEN 'ยกเลิกกรมธรรม์'
                                                   WHEN tcp.end_cause = 35 THEN 'ยกเลิกกรมธรรม์'
                                                   ELSE ' สิ้นผลบังคับ (' || end_cause.cause_name || ')'
                                               END
               WHEN tcm.liability_state=2 THEN 'ขาดอายุ'
               WHEN tcm.liability_state=1 THEN CASE
                                                   WHEN tce.prem_status IS NULL THEN 'มีผลบังคับ'
                                                   WHEN tce.prem_status = 11 THEN 'มีผลบังคับ/ขยายระยะเวลาอัตโนมัติ'
                                                   WHEN tce.prem_status = 8 THEN 'มีผลบังคับ/ขยายระยะเวลา'
                                                   WHEN tce.prem_status = 6 THEN 'มีผลบังคับ/มูลค่าใช้เงินสำเร็จอัตโนมัติ'
                                                   WHEN tce.prem_status = 3 THEN 'มีผลบังคับ/มูลค่าใช้เงินสำเร็จ'
                                                   WHEN apl.sum_apl > 0 THEN 'มีผลบังคับ/กู้อัตโนมัติชำระเบี้ย'
                                                   WHEN tce.prem_status = 1
                                                        OR tce.prem_status = 2
                                                        OR tce.prem_status = 4 THEN 'มีผลบังคับ'
                                                   ELSE ' มีผลบังคับ (' || prem_sts.status_name || ')'
                                               END
               WHEN tcm.liability_state=0 THEN CASE
                                                   WHEN pros.proposal_status=32 THEN 'อยู่ระหว่างการพิจารณา'
                                                   WHEN pros.proposal_status=80 THEN 'อยู่ระหว่างการพิจารณา (Accepted)'
                                                   WHEN pros.proposal_status=81 THEN 'อยู่ระหว่างการพิจารณา (Conditionally Accepted)'
                                                   ELSE ' อยู่ระหว่างการพิจารณา (' || pro_sts.status_desc || ')'
                                               END
               ELSE 'ไม่ระบุ'
           END) AS status_name,
          tce.billing_date,
          tce.DUE_DATE
   FROM mv_contract_master tcm
   INNER JOIN mv_contract_product tcp ON tcm.policy_id=tcp.policy_id
   AND tcp.master_id IS NULL
   LEFT JOIN mv_contract_extend tce ON tce.item_id = tcp.item_id
   AND tce.policy_id = tcp.policy_id
   LEFT JOIN mv_contract_proposal pros ON pros.policy_id = tcm.policy_id
   LEFT JOIN t_proposal_status pro_sts ON pro_sts.proposal_status = pros.proposal_status
   LEFT JOIN t_prem_status prem_sts ON prem_sts.status_id = tce.prem_status
   LEFT JOIN t_end_cause end_cause ON end_cause.cause_id = tcp.end_cause
   LEFT JOIN
     (SELECT ACC.POLICY_ID,
             MAS.POLICY_CODE,
             SUM (CASE
                      WHEN ACC.ACCOUNT_TYPE IN (53) THEN ACC.CAPITAL_BALANCE + ACC.INTEREST_BALANCE
                      ELSE 0
                  END) AS SUM_APL
      FROM MV_POLICY_ACCOUNT ACC,
           MV_CONTRACT_MASTER MAS
      WHERE ACC.POLICY_ID = MAS.POLICY_ID
        AND MAS.SUSPEND <> 'Y'
      GROUP BY ACC.POLICY_ID,
               MAS.POLICY_CODE
      HAVING SUM (CASE
                      WHEN ACC.ACCOUNT_TYPE IN (53) THEN ACC.CAPITAL_BALANCE + ACC.INTEREST_BALANCE
                      ELSE 0
                  END) > 0) apl ON tcm.POLICY_ID=apl.POLICY_ID),
 addr_list AS
  (SELECT tpa.party_id,
          tpa.contact_type,
          addr.*
   FROM mv_address addr,
        mv_party_addr tpa
   WHERE addr.address_id = tpa.address_id
     AND tpa.party_addr_id IN
       (SELECT max(party_addr_id) AS party_addr_id
        FROM mv_party_addr list
        WHERE tpa.contact_type = list.contact_type
          AND tpa.party_id = list.party_id
        GROUP BY party_id,
                 contact_type) )
SELECT tcm.policy_id,
       TO_CHAR(tcm.validate_date, 'dd/MM/yyyy', 'nls_calendar=''thai buddha'' nls_date_language = thai') AS validate_date,
       TRIM(TO_CHAR(tcm.validate_date, 'dd month', 'nls_calendar=''thai buddha'' nls_date_language = thai')) || ' '|| TO_CHAR(tcm.validate_date, 'YYYY', 'nls_calendar=''thai buddha'' nls_date_language = thai') validate_date_str,
       tcm.policy_code,
       CASE WHEN  tcm.TAX_CERITIFICATE_INDI ='Y' 
             THEN 'ใช่'  ELSE 'ไม่ใช่'
       END TAX_CERITIFICATE_INDI,
       tcm.suspend AS frozen,
       w_policy_status.status_name AS policy_status,

  (SELECT cause_name
   FROM t_end_cause
   WHERE cause_id = tcm.end_cause) AS end_cause,
       title_ins.title_desc || ' ' || ins.first_name||' '|| ins.last_name AS insured_name,
       replace(ins.certi_code, substr(ins.certi_code, length(ins.certi_code)-3, 4), 'XXXX') AS insured_id,
       tbi.entry_age AS insured_entry_age,
       ins.gender AS insured_gender,
       ins.birthday AS insured_birthday,
       joiner.joiner_name,
       joiner.joiner_id,
       joiner.joiner_entry_age,
       joiner.joiner_birthday,
       addr_ins.POST_CODE AS ins_addrcontact_NO,
       addr_ins.ADDRESS_7 AS ins_addrcontact_MOOBAN_build,
       addr_ins.MOO AS ins_addrcontact_MOO,
       addr_ins.ADDRESS_6 AS ins_addrcontact_SOI,
       addr_ins.ADDRESS_1 AS ins_addrcontact_ROAD,
       addr_ins.ADDRESS_2 AS ins_addrcontact_SUB,
       addr_ins.ADDRESS_3 AS ins_addrcontact_DISTRICT,
       addr_ins.ADDRESS_4 AS ins_addrcontact_PROVINCE,
       addr_ins.ZIP_CODE AS ins_addrcontact_ZIPCODE,
       ins.email AS email,
      case when tcp.origin_product_id is not null then (select internal_id from t_product_life life where LIFE.PRODUCT_ID = tcp.origin_product_id) else life.internal_id end product_code  ,
      case when tcp.origin_product_id is not null then (select product_name from t_product_life life where LIFE.PRODUCT_ID = tcp.origin_product_id) else life.product_name end product_name  ,
       tcp.amount AS sum_insured,
       tcm.liability_state_date AS policy_status_date, --		tcm.validate_date as validate_date,
 TO_CHAR(tcm.expiry_date, 'dd/MM/yyyy', 'nls_calendar=''thai buddha'' nls_date_language = thai') AS expiry_date,
 ins.home_tel AS insured_home_tel_1,
 ins.office_tel AS ins_insured_office_tel,
 ins.mobile AS ins_insured_mobile,
 CASE
     WHEN cpay.customer_id IS NOT NULL THEN title_pay.title_desc || ' ' || cpay.first_name||' '|| cpay.last_name
     ELSE com_pay.company_name
 END payer_name,
 GET_MV_ADDR(addr_pay.address_id) AS payer_addr,
 addr_pay.POST_CODE AS billing_addr_NO,
 addr_pay.ADDRESS_7 AS billing_addr_MOOBAN_build,
 addr_pay.MOO AS billing_addr_MOO,
 addr_pay.ADDRESS_6 AS billing_addr_SOI,
 addr_pay.ADDRESS_1 AS billing_addr_ROAD,
 addr_pay.ADDRESS_2 AS billing_addr_SUB,
 addr_pay.ADDRESS_3 AS billing_addr_DISTRICT,
 addr_pay.ADDRESS_4 AS billing_addr_PROVINCE,
 addr_pay.ZIP_CODE AS billing_addr_ZIPCODE,
 cpay.home_tel AS insured_home_tel_2,
 cpay.office_tel AS cpay_insured_office_tel,
 cpay.mobile AS cpay_insured_mobile,
 cpay.email AS payer_email,
 agent_level.zone_code SERVICE_zone_code,
 agent_level.AGENT_CODE service_agent_code,
 agent_level.thai_full_name service_agent_name,
 PM.service_avp_code ,
 PM.service_AVP_name,
 PM.service_GM_code ,
 PM.service_GM_NAME,
 PM.service_AL_code ,
 PM.service_AL_NAME,
 PM.issue_agent_code,
 pm.zone_code_sales issue_zone_code,
 PM.issue_agent_name,
 PM.issue_AVP_code,
 PM.issue_AVP_name,
 PM.issue_GM_code,
 PM.issue_GM_name,
 PM.issue_AL_code,
 PM.issue_AL_name,

  (SELECT mode_name
   FROM w_pay_mode
   WHERE tcp.pay_next = mode_id) AS payment_method,

  m.str_data AS frequency_payment,
 CASE
     WHEN
            (SELECT sum(prem.next_total_prem_af)
             FROM mv_contract_product prem,
                  mv_contract_extend prem_sts
             WHERE prem.policy_id = tcm.policy_id
               AND prem.liability_state = 1
               AND prem.item_id = prem_sts.item_id
               AND prem.policy_id = prem_sts.policy_id
               AND prem_sts.prem_status = 1) IS NULL THEN
            (SELECT sum(prem.next_total_prem_af)
             FROM mv_contract_product prem
             WHERE prem.policy_id = tcm.policy_id)
     ELSE
            (SELECT sum(prem.next_total_prem_af)
             FROM mv_contract_product prem,
                  mv_contract_extend prem_sts
             WHERE prem.policy_id = tcm.policy_id
               AND prem.liability_state = 1
               AND prem.item_id = prem_sts.item_id
               AND prem.policy_id = prem_sts.policy_id
               AND prem_sts.prem_status IN (1, 2))
 END total_premium,
 pac.DISPATCH_DATE,
 TRIM(TO_CHAR(pac.DISPATCH_DATE, 'dd month', 'nls_calendar=''thai buddha'' nls_date_language = thai')) || ' '|| TO_CHAR(pac.DISPATCH_DATE, 'YYYY', 'nls_calendar=''thai buddha'' nls_date_language = thai') DISPATCH_DATE_str,
 pac.DISPATCH_REF_NO,
 tcm.issue_date,
 pac.acknowledge_date,
 TRIM(TO_CHAR(pac.UPDATE_TIME, 'dd month', 'nls_calendar=''thai buddha'' nls_date_language = thai')) || ' '|| TO_CHAR(pac.UPDATE_TIME, 'YYYY', 'nls_calendar=''thai buddha'' nls_date_language = thai') ack_date_str,
 TRIM(TO_CHAR(tcm.issue_date, 'dd month', 'nls_calendar=''thai buddha'' nls_date_language = thai')) || ' '|| TO_CHAR(tcm.issue_date, 'YYYY', 'nls_calendar=''thai buddha'' nls_date_language = thai') issue_date_str,
 TRIM(TO_CHAR(w_policy_status.due_date, 'dd month', 'nls_calendar=''thai buddha'' nls_date_language = thai')) || ' '|| ( EXTRACT( YEAR FROM w_policy_status.due_date) +543) due_date_str,
  w_policy_status.due_date,
  PAID.BANK_CODE,
  PAID.bank_abbr,
  PAID.PREFERED_DEDUCTION_DATE,
  PAID.BANK_ACCOUNT,
  PAID.CARD_EXPIRE_DATE,
  nvl(PAID.PAID_STATUS, ' ' ) PAID_STATUS,
  CASE
    WHEN PAID.TRANSFER_DATE IS NOT NULL
      THEN TRIM(TO_CHAR(PAID.TRANSFER_DATE, 'dd month', 'nls_calendar=''thai buddha'' nls_date_language = thai')) || ' '|| ( EXTRACT( YEAR FROM PAID.TRANSFER_DATE) +543)
    ELSE ' '
  END TRANSFER_DATE,
  NVL(APL.APL_AMOUNT, 0) APL_AMOUNT,
  NVL(LOAN.LOAN_AMOUNT,0) LOAN_AMOUNT,
  NVL(PM.general_suspense, 0)+ NVL(PM.renewal_suspense,0)+ NVL(PM.cs_suspense, 0) suspense_Amount
  
FROM mv_contract_master tcm
LEFT JOIN MV_POLICY_ACKNOWLEDGEMENT pac ON tcm.policy_id=pac.policy_id
INNER JOIN mv_contract_product tcp ON tcp.policy_id = tcm.policy_id
AND tcp.master_id IS NULL
INNER JOIN mv_contract_proposal pros ON pros.policy_id = tcm.policy_id
INNER JOIN mv_insured_list til ON tcm.policy_id = til.policy_id
INNER JOIN mv_benefit_insured tbi ON tcm.policy_id = tbi.policy_id
AND tbi.insured_id = til.list_id
AND tbi.item_id = tcp.item_id
INNER JOIN mv_customer ins ON til.party_id = ins.customer_id
LEFT JOIN t_title title_ins ON title_ins.title_code = ins.honor_title
INNER JOIN t_product_life life ON tcp.product_id = life.product_id
AND life.ins_type = 1
INNER JOIN mv_address addr_ins ON addr_ins.address_id = til.address_id -- LEFT JOIN addr_list addr_home ON til.party_id = addr_home.party_id AND addr_home.contact_type = 1
-- LEFT JOIN addr_list addr_office ON til.party_id = addr_office.party_id AND addr_office.contact_type = 2
-- LEFT JOIN addr_list addr_current ON til.party_id = addr_current.party_id AND addr_current.contact_type = 3
INNER JOIN mv_payer payer ON payer.policy_id = tcm.policy_id
left join T_STRING_RESOURCE  m ON tcp.renewal_type = to_number(substr(m.str_id, 19, 1)) AND m.ORG_TABLE='T_CHARGE_MODE' AND m.LANG_ID=218
LEFT JOIN mv_customer cpay ON payer.party_id = cpay.customer_id
LEFT JOIN t_title title_pay ON title_pay.title_code = cpay.honor_title
LEFT JOIN mv_company_customer com_pay ON payer.party_id = com_pay.company_id
LEFT JOIN mv_address addr_pay ON addr_pay.address_id = payer.address_id
LEFT JOIN dh_v_agent agent_level ON agent_level.AGENT_ID = tcm.service_agent
LEFT JOIN w_policy_status ON tcm.policy_id=w_policy_status.policy_id
LEFT JOIN dh_v_policy_master PM ON PM.policy_id = tcm.policy_id
LEFT JOIN (
        SELECT policy_id, sum(capital_balance)+sum(interest_balance) APL_AMOUNT, max(create_date) create_date
        FROM MV_POLICY_ACCOUNT 
        WHERE account_type =53 --APL
         GROUP BY policy_id
        )APL ON APL.POLICY_ID = tcm.policy_id
LEFT JOIN (
        SELECT policy_id, sum(capital_balance)+sum(interest_balance) LOAN_AMOUNT
         FROM MV_POLICY_ACCOUNT 
        WHERE account_type =51 --PL
         GROUP BY policy_id)LOAN ON LOAN.POLICY_ID = tcm.policy_id
LEFT JOIN
  (SELECT * FROM(
	    SELECT 
	      aa.POLICY_ID,
	      aa.PAY_MODE,
	      bb.BANK_CODE,
	      BK.bank_abbr,
	      aa.PREFERED_DEDUCTION_DATE,
	      bb.BANK_ACCOUNT,
	      CASE aa.PAY_MODE WHEN 30 THEN to_char(bb.EXPIRE_DATE, 'MM')||'/'||to_char(bb.EXPIRE_DATE, 'YYYY')
	      ELSE NULL END CARD_EXPIRE_DATE,
	      D.TRANSFER_DATE, 
	      CASE WHEN d.status='Y' then 'ผ่าน' ELSE 'ไม่ผ่าน' end PAID_STATUS
	    FROM MV_PAYER_ACCOUNT aa
	    inner join   mv_contract_master mcm ON mcm.policy_id = aa.policy_id
	    inner join T_BANK_TEXT_DETAIL d ON  aa.policy_id = d.policy_id  AND aa.pay_mode = d.pay_mode
	    inner join t_BANK_ACCOUNT bb ON aa.ACCOUNT_ID=bb.ACCOUNT_ID
	    left join   
	     (SELECT bank_code,
	             substr(abbr_name, 1, 30) AS "BANK_ABBR"
	      FROM T_BANK
	      WHERE BANK_CLASS=1  AND length(bank_code)=3 ) bk ON substr(bb.bank_code, 1, 3) = BK.bank_code
	    WHERE 
	      aa.PAY_MODE IN (3, 30) AND mcm.POLICY_CODE = :policy_code
	    ORDER BY D.transfer_date desc
	) WHERE ROWNUM = 1
) PAID ON PAID.POLICY_ID = TCM.POLICY_ID
--LEFT JOIN
--  (SELECT a.agent_code avp_code,
--          NVL(c.TITLE, 'คุณ' )|| first_name ||' '|| last_name service_AVP_name,
--          a.zone_code
--   FROM mv_customer c,
--        MV_AGENT a
--   WHERE customer_id = a.agent_id) SERVICE_AVP ON SERVICE_AVP.avp_code = agent_level.avp_code
--LEFT JOIN
--  (SELECT a.agent_code AL_code,
--          NVL(c.TITLE, 'คุณ' )||first_name ||' '|| last_name service_AL_name,
--          a.zone_code
--   FROM mv_customer c,
--        MV_AGENT a
--   WHERE customer_id = a.agent_id) SERVICE_AL ON SERVICE_AL.AL_code = agent_level.AL_code
--LEFT JOIN
--  (SELECT a.agent_code GM_code,
--           NVL(c.TITLE, 'คุณ' )|| first_name ||' '|| last_name service_GM_name,
--          a.zone_code
--   FROM mv_customer c,
--        MV_AGENT a
--   WHERE customer_id = a.agent_id) SERVICE_GM ON SERVICE_GM.GM_code = agent_level.GM_code
LEFT JOIN
  (SELECT tcm.policy_id,
          title_ins.title_desc || ' ' || ins.first_name||' '|| ins.last_name AS joiner_name,
          replace(ins.certi_code, substr(ins.certi_code, length(ins.certi_code)-3, 4), 'XXXX') AS joiner_id,
          entry_age AS joiner_entry_age,
          birthday AS joiner_birthday
   FROM mv_contract_master tcm
   INNER JOIN mv_contract_product tcp ON tcp.policy_id = tcm.policy_id
   AND tcp.master_id IS NULL
   INNER JOIN mv_insured_list til ON tcm.policy_id = til.policy_id
   INNER JOIN mv_benefit_insured tbi ON tcm.policy_id = tbi.policy_id
   AND tbi.insured_id = til.list_id
   AND tbi.item_id = tcp.item_id
   INNER JOIN mv_customer ins ON til.party_id = ins.customer_id
   LEFT JOIN t_title title_ins ON title_ins.title_code = ins.honor_title
   INNER JOIN t_product_life life ON tcp.product_id = life.product_id
   AND life.ins_type = 1
   WHERE ally = 1
     AND (til.relation_to_ph IN ('6',  '7')
          OR tbi.entry_age < 15)) joiner ON joiner.policy_id = tcm.policy_id
WHERE NOT (ally = 1
           AND (til.relation_to_ph IN ('6', '7')
                AND tbi.entry_age < 15))
  AND tcm.policy_code = :policy_code
  AND :login_agent_code IN (agent_level.gm_code,
                            agent_level.AVP_CODE,
                            agent_level.AL_CODE,
                            agent_level.AGENT_CODE)