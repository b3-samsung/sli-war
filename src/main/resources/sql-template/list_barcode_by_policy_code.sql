
SELECT  '|10755600012403'||SUBSTR(MOD(LENGTH(t1.policy_code),10)||t1.policy_code||'0000000000000000', 1,16) || '00' barcode
FROM (
-- SELECT t1.liability_state || t1.ref1 || t1.policy_code || t1.agent_code FROM (
    SELECT DISTINCT tcm.liability_state, chq.cheque_no as ref1,tcm.policy_code,agent.agent_code
    FROM (mv_contract_master tcm
        INNER JOIN mv_contract_product tcp on tcm.policy_id=tcp.policy_id
        LEFT JOIN mv_cash cash ON tcm.policy_id=cash.policy_id)
        LEFT JOIN mv_cheque chq ON cash.cheque_id=chq.cheque_id
        LEFT JOIN mv_agent agent ON tcm.service_agent=agent.agent_id
    WHERE 1=1
        AND tcm.liability_state=0
        AND tcm.policy_code=:policy_code
        AND LENGTH(chq.cheque_no)=11
    UNION
    SELECT DISTINCT tcm.liability_state, tcm.policy_code,tcm.policy_code,agent.agent_code
    FROM (mv_contract_master tcm
        LEFT JOIN mv_agent agent ON tcm.service_agent=agent.agent_id)
    WHERE 1=1
        AND tcm.liability_state>=1
        AND tcm.policy_code=:policy_code
        AND tcm.policy_code LIKE '01%'
        AND tcm.issue_date IS NOT NULL
) T1
