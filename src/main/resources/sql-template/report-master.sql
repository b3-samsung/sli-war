SELECT
    rpt_code code, 
    CASE rpt_code 
    WHEN '*' THEN 'ทั้งหมด'
    ELSE  rpt_code || '-' || rpt_name
    END  REPORT_NAME
FROM
   DATAHUB_PROD.dh_t_web_agent_rpt 
order by code