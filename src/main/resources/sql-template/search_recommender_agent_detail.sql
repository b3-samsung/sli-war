with AGENT_STRUCTURE AS (
select agent_id
 , (select agent_code from mv_agent where mv_agent.agent_id = p_level.agent_id) as AGENT_code
 , (select first_name ||' '|| last_name from mv_customer where customer_id = p_level.agent_id )as AGENT_name 
 , (select agent_code from mv_agent where agent_id = p_level.GM) as GM_code
 , (select first_name ||' '|| last_name from mv_customer where customer_id = p_level.GM )as GM_name 
 , (select agent_code from t_agent where agent_id = p_level.VP) as VP_code
 , (select first_name ||' '|| last_name from mv_customer where customer_id = p_level.VP )as VP_name 
 , (select agent_code from t_agent where agent_id = p_level.AL) as AL_code
 , (select first_name ||' '|| last_name from mv_customer where customer_id = p_level.AL )as AL_name 

from 
 (select agent_id
  , GET_AGENT_HIERARCHY (AGENT_ID, 1) as GM -- GM
  , GET_AGENT_HIERARCHY (AGENT_ID, 2) as VP -- VP
  , GET_AGENT_HIERARCHY (AGENT_ID, 3) as AL -- AL
 FROM dh_v_agent
 WHERE AGENT_CATE IN ('L','F','D','E') AND agent_code = :agent_code) p_level
)
SELECT AGENT_CODE, THAI_FULL_NAME, AGENT_CATE_CODE_SALES FROM DH_V_AGENT WHERE AGENT_CODE IN (
   SELECT 
      CASE
           WHEN AGENT_CODE = GM_code   THEN  GM_code 
           WHEN AGENT_CODE = VP_code   THEN  GM_code
           WHEN AGENT_CODE = AL_code   THEN  VP_code
           ELSE AL_code 
      END AGENT_CODE
    FROM AGENT_STRUCTURE
)