WITH w_producer AS
  (SELECT PRODUCER_ID,
          PRODUCER_POSITION,
          UPLINE_ID,
          UPLINE_POSITION
   FROM
     (SELECT DISTINCT PRODUCER_ID,
                      PRODUCER_POSITION,
                      UPLINE_ID,
                      UPLINE_POSITION,
                      ROW_NUMBER () OVER (PARTITION BY PRODUCER_ID
                                          ORDER BY UPDATE_TIME DESC) "RN"
      FROM MV_PRODUCER_HIERARCHY
      WHERE 1=1
        AND HIERARCHY_TYPE = '1'
        AND ACTIVE_INDI = 'Y'
        AND CHANGE_TYPE NOT IN ('10')
        AND TO_CHAR (END_DATE,
                     'YYYYMMDD') ='99991231') A
   WHERE A.RN=1 ),
     w_agent AS
  (SELECT agent_id,
          agent_code,
          LEVEL AS agent_level,
                   connect_by_root upline_id AS top,
                   upline_id AS parent
   FROM w_producer tph
   INNER JOIN mv_agent agent ON agent.agent_id=tph.producer_id CONNECT BY upline_id=
   PRIOR producer_id START WITH upline_id IN
     (SELECT agent_id
      FROM mv_agent
      WHERE agent_code=:agent_code)
   ORDER BY LEVEL),
     w_policy_count AS
  (SELECT count(policy_id) AS policy_count,
          service_agent AS agent_id
   FROM mv_contract_master
   GROUP BY service_agent)
SELECT '' CODE,  'ไม่ระบุ' "DESC"
FROM DUAL
UNION ALL
SELECT *
FROM
  (SELECT agent.AGENT_CODE CODE,
          agent.AGENT_CODE || ' : ' ||cus.first_name || ' ' || cus.last_name || '(' ||COALESCE(policy_count, 0)|| ' กรมธรรม์)' "DESC" -- , COALESCE(policy_count,0) as policy_count

   FROM w_agent agent
   INNER JOIN mv_customer cus ON cus.customer_id=agent.agent_id
   LEFT JOIN w_policy_count cnt ON cnt.agent_id=agent.agent_id
   ORDER BY agent.agent_level,
            agent.agent_code)