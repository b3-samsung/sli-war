SELECT 
    DISTINCT
    EDOC.DOCUMENT_NO as KEY
  , EDOC.DOCUMENT_DATE as DOC_DATE
  , EDOC.DOCUMENT_NO as DOC_NO
  , TPL.TEMPLATE_NAME_TH as DOC_TYPE
  , TPL.CATEGORY_NAME_TH as CAT_NAME
  , tpl.category_id
  , EDOC.POLICY_CODE as POLICY_CODE
  , EDOC.POLICY_ID as POLICY_ID
  , ins.first_name as PH_FNAME
  , ins.last_name as PH_LNAME
  , ag.first_name ||' '|| ag.last_name as AGENT_NAME
  , mv_agent.agent_code
  , det.template_id ||'/'|| replace(det.export_file_path, '\','/') as export_file_path

FROM DH_T_POLICY_EDOC EDOC
  INNER JOIN DH_T_TEMPLATE TPL 
    ON TPL.TEMPLATE_ID=EDOC.TEMPLATE_ID AND TPL.edoc_for_agt='Y'
  
  INNER JOIN MV_CONTRACT_MASTER TCM 
    ON TCM.policy_id=EDOC.policy_id  AND TCM.master_policy_id IS NULL
  
  INNER JOIN MV_CONTRACT_PRODUCT tcp
   ON tcm.POLICY_ID = tcp.POLICY_ID
   
   inner JOIN MV_BENEFIT_INSURED tben
    ON tcp.ITEM_ID = tben.ITEM_ID
    
  INNER JOIN mv_insured_list til 
    ON tcm.policy_id = til.policy_id AND til.LIST_ID = tben.INSURED_ID
  
  INNER JOIN mv_agent 
    ON mv_agent.agent_id = tcm.service_agent
  
  INNER JOIN mv_customer ins 
    ON til.party_id = ins.customer_id
  
  LEFT JOIN (SELECT customer_id, first_name, last_name FROM MV_CUSTOMER) AG 
    ON TCM.service_agent = AG.customer_id
  
  LEFT JOIN dh_t_policy_edoc_det det 
    ON EDOC.document_no = det.document_no   
WHERE 1=1
  AND EDOC.DEL_INDI IS NULL
  AND EDOC.ARCHIVE_INDI IS NULL
  AND tcp.MASTER_ID IS NULL  -- only insured on main plan
  AND mv_agent.AGENT_CODE = :agentCode  
  AND (
    (EDOC.POLICY_CODE LIKE '%'||:policyCode||'%' OR :policyCode is null)
    AND (ins.FIRST_NAME LIKE '%'||:fName||'%' OR :fName is null)
    AND (ins.LAST_NAME LIKE '%'||:lName||'%' OR :lName is null)
    AND (tpl.category_id = :docCategory OR :docCategory < 0 OR :docCategory IS NULL )
  )