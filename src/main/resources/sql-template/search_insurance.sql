--012210026385

WITH 
-- w_producer AS
--  (SELECT PRODUCER_ID,
--          PRODUCER_POSITION,
--          UPLINE_ID,
--          UPLINE_POSITION
--   FROM
--     (SELECT DISTINCT PRODUCER_ID,
--                      PRODUCER_POSITION,
--                      UPLINE_ID,
--                      UPLINE_POSITION,
--                      ROW_NUMBER () OVER (PARTITION BY PRODUCER_ID
--                                          ORDER BY UPDATE_TIME DESC) "RN"
--      FROM MV_PRODUCER_HIERARCHY
--      WHERE HIERARCHY_TYPE = '1'
--        AND ACTIVE_INDI = 'Y'
--        AND CHANGE_TYPE NOT IN ('10')
--        AND TO_CHAR (END_DATE,
--                     'YYYYMMDD') ='99991231') A
--   WHERE A.RN=1 ),
--     w_agent AS
--  (SELECT agent.* -- agent_id
-- -- , agent_code
-- -- , agent_status || '-' || agent_substatus as status
----   , cus.first_name || ' ' || cus.last_name
-- -- , upline_id, producer_id, producer_position
-- -- , level as agent_level
-- -- , connect_by_root upline_id as top
-- -- , connect_by_root upline_id || sys_connect_by_path(producer_id,',') as path
----   , tph.*
--FROM w_producer tph
--   INNER JOIN mv_agent agent ON agent.agent_id=tph.producer_id --and agent.agent_status=1 --and agent.agent_substatus in (1,2)
----    inner join mv_customer cus on cus.customer_id=agent.agent_id
--CONNECT BY upline_id=
--   PRIOR producer_id START WITH upline_id in
--     (SELECT agent_id
--      FROM mv_agent
--      WHERE agent_code = :agent_code))
--,  
 w_policy_status AS
  (SELECT tcm.policy_id AS policy_id ,
          (CASE
               WHEN tcm.liability_state=3 THEN CASE
                                                   WHEN tcp.end_cause = 51 THEN 'สิ้นผลบังคับ/ขอยกเลิกกรมธรรม์ (Freelook)'
                                                   WHEN tcp.end_cause = 31 THEN 'สิ้นผลบังคับ/เวนคืนกรมธรรม์'
                                                   WHEN tcp.end_cause = 12 THEN 'สิ้นสุดความคุ้มครอง'
                                                   WHEN tcp.end_cause = 34 THEN 'ยกเลิกกรมธรรม์'
                                                   WHEN tcp.end_cause = 21 THEN 'สิ้นผลบังคับ'
                                                   WHEN tcp.end_cause = 35 THEN 'ยกเลิกกรมธรรม์'
                                                   WHEN tcp.end_cause = 22
                                                        OR tcp.end_cause = 23 THEN 'สิ้นผลบังคับ'
                                                   ELSE 'สิ้นผลบังคับ (' || tcp.end_cause || ')'
                                               END
               WHEN tcm.liability_state=2 THEN 'ขาดอายุ'
               WHEN tcm.liability_state=1 THEN CASE
                                                   WHEN tce.prem_status IS NULL THEN 'มีผลบังคับ'
                                                   WHEN tce.prem_status = 11 THEN 'มีผลบังคับ/ขยายระยะเวลาอัตโนมัติ'
                                                   WHEN tce.prem_status = 8 THEN 'มีผลบังคับ/ขยายระยะเวลา'
                                                   WHEN tce.prem_status = 6 THEN 'มีผลบังคับ/มูลค่าใช้เงินสำเร็จอัตโนมัติ'
                                                   WHEN tce.prem_status = 3 THEN 'มีผลบังคับ/มูลค่าใช้เงินสำเร็จ'
                                                   WHEN apl.sum_apl > 0 THEN 'มีผลบังคับ/กู้อัตโนมัติชำระเบี้ย'
                                                   WHEN tce.prem_status = 1
                                                        OR tce.prem_status = 2
                                                        OR tce.prem_status = 4 THEN 'มีผลบังคับ'
                                                   ELSE 'มีผลบังคับ (' || tce.prem_status || ')'
                                               END
               WHEN tcm.liability_state=0 THEN CASE
                                                   WHEN pros.proposal_status=32 THEN 'อยู่ระหว่างการพิจารณา'
                                                   ELSE psts.status_desc || '[' || pros.proposal_status || ']'
                                               END
               ELSE 'ไม่ระบุ'
           END) AS status_name
   FROM mv_contract_master tcm
   INNER JOIN mv_contract_product tcp ON tcm.policy_id=tcp.policy_id
   AND tcp.master_id IS NULL
   LEFT JOIN mv_agent agent ON tcm.service_agent = agent.agent_id
   AND agent.agent_code = :agent_code
   LEFT JOIN mv_contract_extend tce ON tce.item_id = tcp.item_id
   AND tce.policy_id = tcp.policy_id
   LEFT JOIN mv_contract_proposal pros ON pros.policy_id = tcm.policy_id -- LEFT JOIN t_prem_status sts ON sts.status_id = tce.prem_status

   LEFT JOIN t_proposal_status psts ON psts.proposal_status = pros.proposal_status -- LEFT JOIN t_liability_status lsts ON lsts.status_id=tcm.liability_state

   LEFT JOIN
     (SELECT ACC.POLICY_ID,
             MAS.POLICY_CODE,
             SUM (CASE
                      WHEN ACC.ACCOUNT_TYPE IN (53) THEN ACC.CAPITAL_BALANCE + ACC.INTEREST_BALANCE
                      ELSE 0
                  END) AS SUM_APL
      FROM MV_POLICY_ACCOUNT ACC,
           MV_CONTRACT_MASTER MAS
      WHERE ACC.POLICY_ID = MAS.POLICY_ID
        AND MAS.SUSPEND <> 'Y'
      GROUP BY ACC.POLICY_ID,
               MAS.POLICY_CODE
      HAVING SUM (CASE
                      WHEN ACC.ACCOUNT_TYPE IN (53) THEN ACC.CAPITAL_BALANCE + ACC.INTEREST_BALANCE
                      ELSE 0
                  END) > 0) apl ON tcm.POLICY_ID=apl.POLICY_ID),
     w_policy_list AS
  (SELECT tcm.policy_id,
          tcm.policy_code,
          tcm.validate_date,
          life.product_name AS product_name,
          ins.certi_code,
          title_ins.title_desc || ' ' || ins.first_name||' '|| ins.last_name AS insured_name,
          ins.first_name AS insured_fname,
          ins.last_name AS insured_lname,
          w_policy_status.status_name AS policy_status,
          TO_CHAR(tcm.liability_state_date, 'DD/MM/YYYY', 'NLS_CALENDAR=''THAI BUDDHA') AS policy_status_date,
          agent.agent_code AS service_agent_code,
          cagent.first_name || ' ' || cagent.last_name AS service_agent_name,
          EDOC.POLICY_CODE export_file
          
FROM mv_contract_master tcm
    INNER JOIN mv_agent agent ON tcm.service_agent=agent.agent_id AND agent.AGENT_CODE = :agent_code
    INNER JOIN mv_contract_product tcp ON tcp.policy_id = tcm.policy_id AND tcp.master_id IS NULL
    INNER JOIN t_product_life life ON tcp.product_id = life.product_id AND life.ins_type = 1
    INNER JOIN w_policy_status ON tcm.policy_id = w_policy_status.policy_id
    INNER JOIN mv_customer cagent ON cagent.customer_id = agent.party_id
    INNER JOIN mv_contract_proposal pros ON pros.policy_id = tcm.policy_id
    INNER JOIN mv_insured_list til ON tcm.policy_id = til.policy_id
    INNER JOIN mv_benefit_insured tbi ON tcm.policy_id = tbi.policy_id AND tbi.insured_id = til.list_id AND tbi.item_id = tcp.item_id
    INNER JOIN mv_customer ins ON til.party_id = ins.customer_id
    LEFT JOIN t_title title_ins ON title_ins.title_code = ins.honor_title
    LEFT JOIN mv_contract_extend tce ON tce.item_id = tcp.item_id AND tce.policy_id = tcp.policy_id
    LEFT JOIN t_end_cause ON tcp.end_cause = t_end_cause.cause_id
    LEFT JOIN t_prem_status ON tce.prem_status = t_prem_status.status_id
    LEFT JOIN ( 
        select MAX(insert_date) insert_date, POLICY_CODE
        from dh_t_policy_edoc_det
        WHERE template_id = '10219'  GROUP BY template_id, POLICY_CODE
    )EDOC ON  EDOC.POLICY_CODE = tcm.POLICY_CODE
WHERE NOT (ally = 1  AND (til.relation_to_ph IN ('6','7')   OR tbi.entry_age < 15)) )
SELECT * FROM w_policy_list t1
