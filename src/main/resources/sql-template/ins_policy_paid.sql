SELECT B2.offset_id ,
        acc.bank_code,
        bk.BANK_ABBR,
       a.policy_id ,
       ctm.policy_code ,
       TO_CHAR(a.DUE_TIME, 'dd/MM/yyyy', 'nls_calendar=''thai buddha'' nls_date_language = thai') AS BILL_DATE ,
       TRIM(TO_CHAR(a.DUE_TIME, 'dd month', 'nls_calendar=''thai buddha'' nls_date_language = thai')) || ' '|| (EXTRACT(Year FROM a.DUE_TIME)+543) BILL_DATE_STR,
       TO_CHAR(a.CHECK_ENTER_TIME, 'dd/MM/yyyy', 'nls_calendar=''thai buddha'' nls_date_language = thai') AS INSERT_TIME ,
       TRIM(TO_CHAR(a.CHECK_ENTER_TIME, 'dd month', 'nls_calendar=''thai buddha'' nls_date_language = thai'))|| ' '|| (EXTRACT(Year FROM a.CHECK_ENTER_TIME)+543) AS INSERT_TIME_TH ,
       a.POLICY_YEAR, --,a.policy_period,
       CASE
           WHEN a.charge_type= 2 THEN -- Half Yearly
 decode(mod(a.policy_period, 2), 0, 2, mod(a.policy_period, 2))
           WHEN a.charge_type = 3 THEN --Quarterly
 decode(mod(a.policy_period, 4), 0, 4, mod(a.policy_period, 4))
           WHEN a.charge_type = 4 THEN --Monthly
 decode(mod(a.policy_period, 12), 0, 12, mod(a.policy_period, 12))
           ELSE ---Yearly
 1
       END policy_period,
       t1.str_data AS CHARGE_MODE,
       t2.str_data AS PAY_MODE_DESC,
       a.charge_type,
       c.pay_mode,
       sum(a.fee_amount) PAY_BALANCE
FROM t_cash c,
     t_arap_offset b2,
     t_arap_offset b1,
     t_prem_arap a,
     T_BANK_ACCOUNT acc,
     mv_contract_master ctm, 
  (SELECT to_number(substr(str_id, 19, 1)) AS "CODE1",
          str_data
   FROM T_STRING_RESOURCE
   WHERE ORG_TABLE='T_CHARGE_MODE'
     AND LANG_ID=218) t1,
  (SELECT to_number(substr(str_id, 16, 2)) AS "CODE2",
          str_data
   FROM T_STRING_RESOURCE
   WHERE ORG_TABLE='T_PAY_MODE'
     AND LANG_ID='011' ) t2,
  (SELECT bank_code,substr(abbr_name,1,30) AS "BANK_ABBR" 
    FROM T_BANK WHERE  BANK_CLASS=1 AND length(bank_code)=3 ) bk
WHERE a.CHARGE_TYPE = t1.CODE1(+)
  AND c.pay_mode = t2.CODE2(+)
  AND ctm.policy_id = a.policy_id
  AND c.fee_id = b2.fee_id
  AND b2.arap_table = '1'
  AND b2.offset_id = b1.offset_id
  AND b1.arap_table = '4'
  AND b1.fee_id = a.list_id
  AND a.fee_type IN (41) ----premium apply
AND c.fee_status = 1 --and c.fee_type = 11
AND c.ACCOUNT_ID=acc.ACCOUNT_ID(+) --AND substr(acc.bank_code,1,3) = BK.bank_code(+)
AND ctm.POLICY_code = :POLICY_CODE --210010  --335138 --210010
AND C.check_enter_time = a.check_enter_time 
AND substr(acc.bank_code,1,3) = BK.bank_code(+)
AND ABS(EXTRACT(YEAR FROM SYSDATE) - EXTRACT(YEAR FROM a.due_time)) <=2
GROUP BY b2.offset_id,
         ctm.policy_code,
         a.due_time,
         a.check_enter_time,
         a.POLICY_YEAR,
         a.policy_period,
         a.charge_type,
         c.pay_mode,
         a.POLICY_ID,
         t1.str_data,
         t2.str_data,
         acc.bank_code,
         bk.BANK_ABBR
ORDER BY a.due_time  DESC