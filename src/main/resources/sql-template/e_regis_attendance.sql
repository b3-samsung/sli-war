UPDATE DH_T_ACT_REGIS
SET title = :title
--	, id = :id
    , name = :name
    , surname = :surname
    , gender = :gender
    , age = :age
    , dob =  CASE WHEN :dob IS NULL OR '' = :dob THEN NULL ELSE TO_DATE(:dob, 'dd-mon-yyyy') END
    , address1 = :address1
    , address2 = :address2
    , address3 = :address3
    , address4 = :address4
    , address5 = :address5
    , zip = :zip
    , mobile = :mobile
    , email = :email
    , line = :line
    , recommender_code = :recommender_code
    , attendance_result = :attendance_result
    , payin_local = :payin_local
    , picture_local = :picture_local
    , idcard_local = :idcard_local
    , update_time = :update_time
    , update_by = :update_by
    , bank_code = :bank_code
    , payin_num = :payin_num
    , payin_amount = :payin_amount
    , payin_date = CASE WHEN :payin_date IS NULL OR '' = :dob THEN NULL ELSE TO_DATE(:payin_date, 'dd-mon-yyyy') END
    , payin_time = :payin_time
    , payin_ref = :payin_ref
    , education_level = :education_level
    , register_type = :register_type
WHERE ACTIVITY_ID = :activity_id AND id = :id--AND Register_id = :register_id