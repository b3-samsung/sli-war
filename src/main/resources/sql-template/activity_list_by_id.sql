SELECT 
    ACTIVITY_ID
    , ACTIVITY_NAME
    , LOCATION
    , TO_CHAR(ACTIVITY_DATE, 'dd/MM/yyyy', 'nls_calendar=''thai buddha'' nls_date_language = thai') AS ACTIVITY_DATE
    , CONCAT(CONCAT(START_TIME, ' - '), END_TIME) AS TIME
    , MAX_CAPA
    , ROOM_CAPA
    , ACTIVITY_TYPE
    ,ACTIVITY_SUB
    , (SELECT PROVINCE_NAME FROM dh_t_ag_province WHERE PROVINCE_CD = PROVINCE) as PROVINCE
    , ACTIVITY_REGIS_TO
    , CASE
        WHEN TRUNC(SYSDATE) <= TRUNC(ACTIVITY_DATE) THEN 'true'
        ELSE 'false'
    END AS IS_REGISTER
FROM dh_t_ag_activity 
WHERE ACTIVITY_ID= :activityId