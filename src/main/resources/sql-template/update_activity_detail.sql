UPDATE DH_T_AG_ACTIVITY
    SET ACTIVITY_TYPE = :activityType
    , ACTIVITY_SUB =  :activitySubType 
    , PROVINCE = :province
    , ACTIVITY_NAME = :activityName
    , ACTIVITY_DATE = TO_DATE(:activityDate, 'dd-mon-yyyy')
    , LOCATION = :location
    , NUMOFDAYS = :numOfDays
    , START_TIME = :startTime
    , END_TIME = :endTime
    , ACTIVITY_REGIS_FROM = TO_DATE(:activityRegisFrom, 'dd-mon-yyyy')
    , ACTIVITY_REGIS_TO = TO_DATE(:activityRegisTo, 'dd-mon-yyyy HH24:MI')
    , MAX_CAPA = :maxCapa
    , ROOM_CAPA = :roomCapa
    , ACTIVITY_DATE_TO = CASE WHEN :activityDateTo IS NULL OR  :activityDateTo = 'null' OR :activityDateTo = '' THEN NULL ELSE TO_DATE(:activityDateTo, 'dd-mon-yyyy') END
    , REMARK1 = :remark || :activitySubType
    , DEL_INDI = NULL
WHERE ACTIVITY_ID = :activityId