SELECT 
    ACTIVITY_ID
    , ACTIVITY_NAME
    , LOCATION
    , TO_CHAR(ACTIVITY_DATE, 'dd/MM/yyyy', 'nls_calendar=''thai buddha'' nls_date_language = thai') AS ACTIVITY_DATE
    , CONCAT(CONCAT(START_TIME, ' - '), END_TIME) AS TIME
    , MAX_CAPA
    , ROOM_CAPA
    , DEL_INDI
    , ACTIVITY_TYPE
    , (SELECT COUNT(regis.register_id) FROM DH_T_ACT_REGIS regis WHERE regis.activity_id = dh_t_ag_activity.activity_id AND NVL(regis.del_indi,'N' ) <> 'Y') AS CURRENT_CAPA
    , CASE
        WHEN TRUNC(SYSDATE) <= TRUNC(ACTIVITY_DATE) AND DEL_INDI IS NULL THEN 'true'
        ELSE 'false' 
    END AS IS_REGISTER
    , CASE
        WHEN SYSDATE BETWEEN ACTIVITY_REGIS_FROM AND ACTIVITY_REGIS_TO AND DEL_INDI IS NULL THEN 'true'
        ELSE 'false' 
    END AS IS_SIGNUP
    ,ACTIVITY_DATE Act_date
    , SYSDATE todaty
FROM dh_t_ag_activity 
WHERE 
 	(NVL(DEL_INDI,'N' ) = 'N' OR ( DEL_INDI = 'Y' AND :webRole LIKE '%REGISTER_ADMIN%' ) )
	AND activity_type = :activityType 
    AND province LIKE
        CASE
            WHEN :province <> '*' THEN :province
            ELSE '%%'
        END
    AND TRUNC(SYSDATE) - TRUNC(ACTIVITY_DATE) <= 30
ORDER BY 
    CASE WHEN :sortType = 'ASC' THEN dh_t_ag_activity.ACTIVITY_ID END ASC
    , CASE WHEN :sortType = 'DESC' THEN dh_t_ag_activity.ACTIVITY_ID END DESC
