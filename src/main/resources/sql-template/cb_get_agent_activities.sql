SELECT 
    T2.ACTIVITY_NAME,
    T2.LOCATION,
    T1.REGISTER_ID,
    TRIM(TO_CHAR(t2.activity_date, 'dd month', 'nls_calendar=''thai buddha'' nls_date_language = thai')) || ' '|| TO_CHAR(t2.activity_date, 'YYYY', 'nls_calendar=''thai buddha'' nls_date_language = thai') INSERT_DATE,
    TO_CHAR(to_date(t2.start_time, 'hh24:mi:ss'), 'HH24:mi') ||'-' || TO_CHAR(to_date(t2.end_time, 'hh24:mi:ss'), 'HH24:mi') INSERT_TIME,
    CASE 
      CASE 
        WHEN t2.activity_type = 3 THEN T1.EXAM_RESULT
        ELSE T1.attendance_result
      END 
        WHEN 'Y' THEN 'ผ่าน'
        ELSE 'ไม่ผ่าน'
    END EXAM_RESULT,
    T2.ACTIVITY_ID,
    T2.ACTIVITY_TYPE,
    T1.NAME || ' '|| T1.SURNAME FULLNAME,
    T1.ID IDCARD,
    T1.AGENT_CODE
from dh_t_act_regis t1, dh_t_ag_activity t2
WHERE 
    t2.activity_id = t1.activity_id 
    AND t1.recommender_code IN ( select AGENT_CODE from dh_v_agent  where :agentCode IN (gm_code, avp_code, AL_CODE, AGENT_CODE) )
    AND (T1.AGENT_CODE = :filter OR T1.ID = :filter OR REPLACE(:filter, ' ','')  = T1.NAME || T1.SURNAME)
    AND CASE  WHEN T2.ACTIVITY_TYPE IN (6,4) THEN 4 ELSE T2.ACTIVITY_TYPE END = :activityType
    AND  CASE 
        	WHEN activity_date >= sysdate THEN 'FUTURE'
        	ELSE 'PAST'
    	END = :period 
    AND NVL(t1.del_indi,'N') <> 'Y' 
    AND NVL(t2.del_indi,'N') <> 'Y'
order by t2.activity_date asc