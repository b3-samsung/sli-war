SELECT
    det.insert_date,
    det.document_no,
    det.export_file_path,
    temp.template_name_th,
    temp.category_name_th,
    det.policy_code,
    ins.first_name,
    ins.last_name,
    cagent.first_name || ' ' || cagent.last_name AS service_agent_name
FROM dh_t_policy_edoc_det det
LEFT JOIN dh_t_template temp ON temp.template_id = det.template_id
LEFT JOIN mv_contract_master tcm ON tcm.policy_code = det.policy_code
INNER JOIN mv_insured_list til ON tcm.POLICY_ID = til.POLICY_ID
INNER JOIN mv_customer ins ON ins.CUSTOMER_ID = til.PARTY_ID
INNER JOIN mv_agent agent ON tcm.service_agent = agent.agent_id
INNER JOIN mv_customer cagent ON cagent.customer_id = agent.party_id
WHERE temp.category_name_th = :categoryTypeName