select tcm.POLICY_CODE
  , ins.CERTI_TYPE
  , replace(ins.CERTI_CODE,substr(ins.CERTI_CODE,length(ins.CERTI_CODE)-3,4),'XXXX') certi_code
  , ins.first_name || ' ' || ins.last_name as CUSTOMER_NAME
  , case when ins.GENDER='M' then 'ชาย' else 'หญิง' end gender
  , TO_CHAR(ins.BIRTHDAY, 'dd/MM/yyyy', 'nls_calendar=''thai buddha'' nls_date_language = thai') AS BIRTHDAY
  , tbi.entry_age as INSURED_ENTRY_AGE
from mv_contract_master tcm
  inner join mv_insured_list til on tcm.POLICY_ID=til.POLICY_ID
  inner join mv_customer ins on ins.CUSTOMER_ID=til.PARTY_ID
  inner join mv_contract_product tcp on tcm.policy_id = tcp.policy_id AND tcp.master_id IS NULL
  INNER JOIN mv_benefit_insured tbi ON tcm.policy_id = tbi.policy_id AND tbi.insured_id = til.list_id AND tbi.item_id = tcp.item_id
where tcm.POLICY_CODE = :policy_code 