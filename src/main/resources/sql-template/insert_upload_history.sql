INSERT INTO
	DD_T_UPLOAD_HISTORY (
		FILE_NAME,
		HEADER,
		CREATE_DATE,
		CREATE_BY,
		UPDATE_DATE,
		UPDATE_BY,
		TYPE
	)
VALUES
	(
		:fileName,
		:header,
		:createDate,
		:createBy,
		:updateDate,
		:updateBy,
		:activityType
	)