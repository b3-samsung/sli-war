SELECT -1 category_id, 'ไม่ระบุ' category_name_th from dual
union all
SELECT 
    category_id,
    category_name_th
FROM dh_t_template
WHERE category_id IS NOT NULL AND edoc_for_agt='Y'
GROUP BY category_id, category_name_th
