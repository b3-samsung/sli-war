UPDATE DH_T_ACT_REGIS
SET 
  UPDATE_TIME = SYSDATE ,
  UPDATE_BY = :editor,
  DEL_INDI =
  (SELECT CASE DEL_INDI
              WHEN 'Y' THEN NULL
              ELSE 'Y'
          END DEL_INDI
   FROM DH_T_ACT_REGIS
   WHERE REGISTER_ID = :registerId AND ACTIVITY_ID = :activityId)
WHERE REGISTER_ID = :registerId AND ACTIVITY_ID = :activityId