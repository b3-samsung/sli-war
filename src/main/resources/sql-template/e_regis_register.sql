INSERT INTO DH_T_ACT_REGIS (
	register_id, register_type,
	activity_id,
	id, title, name, surname, gender, age, dob,
	address1, address2, address3, address4, address5,
	zip, mobile, email, line,
	recommender_code,
	attendance_result, exam_result,
	payin_local,
	picture_local,
	idcard_local,
	insert_time, update_time,
	insert_by, update_by,
	agent_code,
	bank_code,
	payin_num, payin_amount, payin_date, payin_time, payin_ref,
	education_level
)
VALUES (
    :register_id, :register_type
    , :activity_id
    , :id, :title, :name, :surname, :gender, :age, CASE WHEN :dob IS NULL OR '' = :dob THEN NULL ELSE TO_DATE(:dob, 'dd-mon-yyyy') END
    , :address1, :address2, :address3, :address4, :address5
    , :zip, :mobile, :email, :line
    , :recommender_code
    , :attendance_result, :exam_result
    , :payin_local
    , :picture_local
    , :idcard_local
    , :insert_time, :update_time
    , :insert_by, :update_by
    , :agent_code
    , :bank_code
    , :payin_num, :payin_amount, CASE WHEN :payin_date IS NULL OR '' = :dob THEN NULL ELSE TO_DATE(:payin_date, 'dd-mon-yyyy') END, :payin_time, :payin_ref
	, :education_level
)