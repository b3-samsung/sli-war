SELECT *
FROM
  (SELECT T2.ACTIVITY_NAME,
          T2.LOCATION,
          T1.REGISTER_ID,
          TRIM(TO_CHAR(T1.INSERT_TIME, 'dd month', 'nls_calendar=''thai buddha'' nls_date_language = thai')) || ' '|| TO_CHAR(T1.INSERT_TIME, 'YYYY', 'nls_calendar=''thai buddha'' nls_date_language = thai') INSERT_DATE,
          TO_CHAR(T1.INSERT_TIME, 'HH24:MM') INSERT_TIME,
          T1.EXAM_RESULT,
          T2.ACTIVITY_ID,
          T2.ACTIVITY_TYPE,
          T1.NAME || ' '|| T1.SURNAME FULLNAME,
          T1.ID IDCARD,
          T3.GM_CODE,
          T3.AVP_CODE,
          T3.AL_CODE,
          T1.AGENT_CODE
   FROM DH_T_ACT_REGIS T1
   INNER JOIN DH_T_AG_ACTIVITY T2 ON T1.ACTIVITY_ID = T2.ACTIVITY_ID
   INNER JOIN DH_V_AGENT T3 ON T3.AGENT_CODE = T1.AGENT_CODE
   WHERE :agentCode IN (T3.GM_CODE,s
                         T3.AVP_CODE,
                         T3.AL_CODE,
                         T1.AGENT_CODE)
     AND (T1.AGENT_CODE = :filter OR T1.ID = :filter OR REPLACE(:filter, ' ','')  = T1.NAME || T1.SURNAME)
     AND CASE 
            WHEN T2.ACTIVITY_TYPE IN (1,2,5) THEN 1 -- อบรม
            WHEN T2.ACTIVITY_TYPE IN (3,4) THEN 2   -- สอบ
         END = :activityType
   ORDER BY T1.INSERT_TIME DESC) T
WHERE ROWNUM <=5

-- :agentCode is upline 
-- :filter is downline AG 221102010 121200049 121200092
-- :activityType : 1 อบรม 2 สอบ