WITH w_case_status as (
	SELECT status_id, 
	    (CASE 
	        WHEN status_id in (1,2,3,4) THEN 'บันทึกการเรียกร้องสินไหมแล้ว'
	        WHEN status_id in (5,6,14,15,16,18) THEN 'อยู่ระหว่างการพิจารณา'
	        WHEN status_id in (7,8,13) THEN 'อยู่ระหว่างการพิจารณา'
	        WHEN status_id in (10) THEN 'การพิจาณาสินไหมสิ้นสุดแล้ว'
	        WHEN status_id in (11,12,17) THEN 'ยกเลิกการบันทึกสินไหม'
	        ELSE status_name || '[' || status_id || ']'
	     END) as status_name
	FROM t_case_status)
select
     policy_id 
     , ccase.insured_id
     ,tcm.policy_code
    , ccase.case_id 
    ,case_no
    , w_case_status.status_name as status
    , TO_CHAR(notification_date, 'dd/MM/yyyy', 'nls_calendar=''thai buddha'' nls_date_language = thai') AS notification_date
    , TO_CHAR(trunc(approve_time), 'dd/MM/yyyy', 'nls_calendar=''thai buddha'' nls_date_language = thai') AS approve_date
    , t1.TITLE_DESC ||  cust.first_name || ' ' || cust.last_name as  insured_Name
from 
    t_claim_case ccase
     inner join mv_customer cust on cust.customer_id = ccase.insured_id
     INNER JOIN T_TITLE_THAI t1 ON t1.TITLE_CODE = cust.HONOR_TITLE
    inner join t_claim_product cprod on cprod.case_id = ccase.case_id and is_accepted = 'Y'
    inner join (select policy_code,policy_id as tcm_policy_id from mv_contract_master ) tcm on tcm.tcm_policy_id = policy_id
    left join w_case_status on ccase.case_status = w_case_status.status_id
where 
    tcm.policy_code = :policy_code    
order by approve_time DESC


