WITH next_Register_Id AS (
    SELECT activity_id, COUNT(REGISTER_ID)+ 1 AS next_Register_Id FROM DH_T_ACT_REGIS WHERE ACTIVITY_ID = :activityId GROUP BY ACTIVITY_ID
), ACTIVE_REGIS AS (
  SELECT activity_id, COUNT(REGISTER_ID)  AS ACTIVE_REGIS FROM DH_T_ACT_REGIS WHERE ACTIVITY_ID = :activityId AND NVL(del_indi,'N' ) <> 'Y' GROUP BY ACTIVITY_ID
)
SELECT 
--    T1.max_capa,
--    T3.ACTIVE_REGIS ,
    CASE 
        WHEN T1.max_capa <= T3.ACTIVE_REGIS THEN -1
        ELSE T2.next_Register_Id
    END next_Register_Id
FROM dh_t_ag_activity T1, next_Register_Id T2, ACTIVE_REGIS T3  WHERE T1.ACTIVITY_ID  = T2.ACTIVITY_ID AND t3.activity_id=T2.activity_id AND T1.ACTIVITY_ID = :activityId
