select 
   a.POLICY_ID,
   cm.POLICY_CODE,
   a.POLICY_YEAR || '/' || a.POLICY_PERIOD YEAR_PERIOD,
   TRIM(TO_CHAR(py.cheque_date, 'dd month', 'nls_calendar=''thai buddha'' nls_date_language = thai')) || ' '|| (EXTRACT(Year FROM py.cheque_date)+543) PAID_DATE_STR,
  TO_CHAR(py.cheque_date, 'dd/MM/yyyy', 'nls_calendar=''thai buddha'' nls_date_language = thai') AS PAID_DATE, 
   TO_CHAR( a.DUE_TIME, 'dd/MM/yyyy', 'nls_calendar=''thai buddha'' nls_date_language = thai') DUE_TIME,
   TRIM(TO_CHAR(a.DUE_TIME, 'dd month', 'nls_calendar=''thai buddha'' nls_date_language = thai')) || ' '|| (EXTRACT(Year FROM a.DUE_TIME)+543) DUE_TIME_STR,
 
CASE WHEN a.FEE_type =8 THEN 'เงินคืนตามสัญญา'
   WHEN a.FEE_TYPE = 83 THEN 'เงินบำนาญ'
   WHEN a.FEE_TYPE = 85 THEN 'เงินครบกำหนดสัญญา' 
   ELSE 'Other' END AS "FEE_NAME",
 c.FEE_AMOUNT, 
  CASE WHEN c.pay_mode ='3' THEN 'โอนเงินผ่านธนาคาร'
   WHEN c.pay_mode ='2' THEN 'เช็ค'
   WHEN c.pay_mode ='5' THEN 'โอนเงินภายในบริษัท' 
   ELSE 'อื่นๆ' END AS "PAY_MODE_NAME",
  c.pay_mode, 
   CASE WHEN c.pay_mode ='3' THEN substr(acc.BANK_code,1,3)
    ELSE '' END AS "BANK_CODE",
     CASE WHEN c.pay_mode ='3' THEN BK.bank_abbr
    ELSE '' END AS "BANK_ABBR",
  CASE WHEN c.pay_mode ='3' THEN acc.BANK_ACCOUNT 
    ELSE '' END AS "ACCOUNT_NAME" 

from t_cash c, t_arap_offset b2, t_arap_offset b1, t_prem_arap a, 
t_bank_text_busi_data_ext py,
T_BANK_ACCOUNT acc , 
mv_contract_master cm,
(SELECT bank_code,substr(abbr_name,1,30) AS "BANK_ABBR" 
FROM T_BANK WHERE  BANK_CLASS=1 AND length(bank_code)=3 ) bk
where 
   cm.POLICY_ID = a.POLICY_ID
and    c.fee_id = b2.fee_id
and b2.arap_table = '1'
and b2.offset_id = b1.offset_id
and b1.arap_table = '4'
and b1.fee_id = a.list_id
and a.fee_type IN (8,83,85)   ----withdraw from deposit account
AND C.fee_id =py.fee_id(+)
--and a.account_type = 23 ---SB
and c.fee_status = 1 
--AND c.fee_type = 32
AND c.ACCOUNT_ID=acc.ACCOUNT_ID(+)
AND substr(acc.bank_code,1,3) = BK.bank_code(+)
AND cm.POLICY_CODE = :POLICY_CODE  -- 401495
 ORDER BY a.due_time DESC
 